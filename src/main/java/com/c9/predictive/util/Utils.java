package com.c9.predictive.util;

import org.apache.commons.lang3.StringUtils;
import org.vertx.java.core.json.JsonObject;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by yngbldjr on 6/10/14.
 */
public class Utils {
  public static String getHostName(){
    try{
      return InetAddress.getLocalHost().getHostName();
    }catch(Exception ex){
      return "UNABLE TO GET HOSTNAME";
    }
  }

  public static final SimpleDateFormat LAST_EXTRACT_DATE_FORMAT;

  static final public class JMSPropertyConstants {
    public static final String HOSTNAME = "hostname";
  }

  static final public class PropertyConstants {
    public static final String LAST_OPEN_EXTRACT_DATE = "last_open_extract_date";
    public static final String ANALYTIC_HOSTNAME = "analyticHostname";
    public static final String LAST_REFRESH_TIME = "lastRefreshTime";

    public static final String ID_LIST_REPLACEMENT = "id_list";

    public static final String PARTITION_ID = "partitionId";
    public static final String CUSTOMER = "customer";

    public static final String DATA_EXTRACTION_LOCATION = "dataExtractionLocation";
    public static final String CUSTOM_CONFIG_OUT_KEY = "customconfig";
    public static final String ENABLE_OPP_SCORE_PROPERTY = "enableOppScores";
    public static final String ENABLE_DURATION_SCORE_PROPERTY = "enableDurationModelScores";
    public static final String CORRELATION_ID = "correlationId";
    public static final String SCORE_FILE_LOCATION = "scoreFileLocation";
    public static final String DURATION_SCORE_FILE_LOCATION = "durationScoreFileLocation";
    public static final String EMAIL_SUBJECT = "emailSubject";
    public static final String UPLOAD_SCORE_SFDC = "uploadScoreSFDC";

    // All added to support retrospective scoring.  Simple overrides for existing components.
    public static final String EXTRACT_QUERIES_SECTION = "extractQueriesSection";
    public static final String LAST_OPEN_EXTRACT_DATE_OVERRIDE = "lastOpenExtractOverride";
    public static final String SCORE_MODE = "scoreMode";
//    public static final String OPP_SCORES_ADS_TABLE_DEFINITION = "OpportunityScoresV3_Base.csv";
    // These need to become inputs, and or lookups from the config based on oppscore version/other overrides
    public static final String OPP_SCORES_ADS_TABLE_DEFINITION = "OpportunityScoresV3_Base.csv";
    public static final String DURATION_ADS_TABLE_DEFINITION = "DurationModelScores_Base.csv";
    public static final String QUERY_REPLACEMENTS = "queryReplacements";
  }

  static {
    LAST_EXTRACT_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    LAST_EXTRACT_DATE_FORMAT.setLenient(false);
  }

  public static String sqlReplacements(String original, Map<String, Object> replacements){
    String finalSQL = original;
    for(Map.Entry<String, Object> e : replacements.entrySet()){
      finalSQL = finalSQL.replace("{"+e.getKey().trim()+"}", (String)e.getValue());
    }

    return finalSQL;
  }

}
