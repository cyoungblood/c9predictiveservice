/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.c9.predictive.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.handlers.MapListHandler;

public class LimitedMapListHandler extends MapListHandler {
  private int limit ;
  private boolean expired = false ;

  public LimitedMapListHandler() {
    this( -1 ) ;
  }

  public LimitedMapListHandler(int limit) {
    super(new JsonRowProcessor());
    this.limit = limit ;
  }

  static class JsonRowProcessor extends BasicRowProcessor{

    @Override
    public Map<String, Object> toMap(ResultSet rs) throws SQLException {
      Map<String, Object> result = new CaseInsensitiveHashMap();
      ResultSetMetaData rsmd = rs.getMetaData();
      int cols = rsmd.getColumnCount();

      for (int i = 1; i <= cols; i++) {
        Object o = rs.getObject(i);
        if(o instanceof Timestamp){
          result.put(rsmd.getColumnLabel(i), ((Timestamp)rs.getObject(i)).getTime());
        } else {
          result.put(rsmd.getColumnLabel(i), rs.getObject(i));
        }
      }

      return result;
    }
  }

  private static class CaseInsensitiveHashMap extends HashMap<String, Object> {
    /**
     * The internal mapping from lowercase keys to the real keys.
     *
     * <p>
     * Any query operation using the key
     * ({@link #get(Object)}, {@link #containsKey(Object)})
     * is done in three steps:
     * <ul>
     * <li>convert the parameter key to lower case</li>
     * <li>get the actual key that corresponds to the lower case key</li>
     * <li>query the map with the actual key</li>
     * </ul>
     * </p>
     */
    private final Map<String, String> lowerCaseMap = new HashMap<String, String>();

    /**
     * Required for serialization support.
     *
     * @see java.io.Serializable
     */
    private static final long serialVersionUID = -2848100435296897392L;

    /** {@inheritDoc} */
    @Override
    public boolean containsKey(Object key) {
      Object realKey = lowerCaseMap.get(key.toString().toLowerCase(Locale.ENGLISH));
      return super.containsKey(realKey);
      // Possible optimisation here:
      // Since the lowerCaseMap contains a mapping for all the keys,
      // we could just do this:
      // return lowerCaseMap.containsKey(key.toString().toLowerCase());
    }

    /** {@inheritDoc} */
    @Override
    public Object get(Object key) {
      Object realKey = lowerCaseMap.get(key.toString().toLowerCase(Locale.ENGLISH));
      return super.get(realKey);
    }

    /** {@inheritDoc} */
    @Override
    public Object put(String key, Object value) {
            /*
             * In order to keep the map and lowerCaseMap synchronized,
             * we have to remove the old mapping before putting the
             * new one. Indeed, oldKey and key are not necessaliry equals.
             * (That's why we call super.remove(oldKey) and not just
             * super.put(key, value))
             */
      Object oldKey = lowerCaseMap.put(key.toLowerCase(Locale.ENGLISH), key);
      Object oldValue = super.remove(oldKey);
      super.put(key, value);
      return oldValue;
    }

    /** {@inheritDoc} */
    @Override
    public void putAll(Map<? extends String, ?> m) {
      for (Map.Entry<? extends String, ?> entry : m.entrySet()) {
        String key = entry.getKey();
        Object value = entry.getValue();
        this.put(key, value);
      }
    }

    /** {@inheritDoc} */
    @Override
    public Object remove(Object key) {
      Object realKey = lowerCaseMap.remove(key.toString().toLowerCase(Locale.ENGLISH));
      return super.remove(realKey);
    }
  }

  @Override
  public List<Map<String,Object>> handle( ResultSet rs ) throws SQLException {
    List<Map<String,Object>> rows = new ArrayList<Map<String,Object>>();
    while( limit == -1 || rows.size() < limit ) {
      if( rs.next() ) {
        rows.add( this.handleRow( rs ) ) ;
      }
      else {
        expired = true ;
        break ;
      }
    }
    return rows ;
  }

  public boolean isExpired() {
    return expired ;
  }
}