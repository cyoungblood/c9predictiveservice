package com.c9.predictive.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.*;
import java.util.Properties;

/**
 * 
 * @author ssubramanian 
 *  TODO To be removed once Platform provides an API to get
 *  customer creds. Use the same mechanism to decrypt as
 *  com.certive.cs.salesforce.loader.LoaderEncrypter
 */
public class Credentials {

  private static final String ALGORITHM = "DESede";
  private static final int FILEKEYS = 639;
  private static final int KEYID = 222;

  private static final String C9_BASE_ENCRYPTION = "c9_encryption.key";
  private static final String C9_PROPERTIES = "c9.properties";
  
  private static final String USER_NAME = "UserName";
  private static final String PASSWORD = "Password";

  /**
   * 
   * @param dir - loader dir' that has c9.properties and c9_encryption.key
   * @return creds[username, password]
   * @throws Exception
   */
  public String[] getCreds(String dir) throws Exception {
    String[] creds = new String[2];
    BufferedReader br = null;
    try {
      File file = new File(dir + File.separator + C9_PROPERTIES);
      if(!file.exists()) {
    	  throw new IllegalArgumentException("Cannot fetch credentials, path does not exist: " + file.getAbsolutePath() );
      }
      br = new BufferedReader(new FileReader(file));
      Properties props = new Properties();
      props.load(br);
      creds[0] = props.getProperty(USER_NAME);
      creds[1] = decryptString(dir, props.getProperty(PASSWORD));
    } finally {
      br.close();
    }
    return creds;
  }

  
  public String decryptString(String sDirectory, String sInput) throws Exception {
    if (sInput == null) {
      return null;
    }

    String sFile = sDirectory + "/" + C9_BASE_ENCRYPTION;
    Cipher dcipher = loadDecryptor(sFile);

    byte[] aEncryptedBytes = Base64.decode(sInput.toCharArray());
    byte[] aDecryptedBytes = dcipher.doFinal(aEncryptedBytes);

    return new String(aDecryptedBytes, "UTF8");
  }

  public Cipher loadDecryptor(String sFile) throws Exception {
    Cipher dcipher = null;

    File theFile = new File(sFile);
    if(!theFile.exists()) {
  	  throw new IllegalArgumentException("Cannot fetch credentials, path does not exist: " + theFile.getAbsolutePath() );
    }
    
    SecretKey secretKey = null;

    if (theFile.exists() == false) {
      FileOutputStream fileOutput = new FileOutputStream(theFile);
      ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);

      //
      // Create a new encryption key
      //
      KeyGenerator keyGen = KeyGenerator.getInstance(ALGORITHM);
      keyGen.init(168);

      for (int i = 0; i < FILEKEYS; i++) {
        SecretKey key = keyGen.generateKey();

        if (i == KEYID) {
          secretKey = key;
        }

        objectOutput.writeObject(key);
      }

      dcipher = Cipher.getInstance(secretKey.getAlgorithm());
      dcipher.init(Cipher.DECRYPT_MODE, secretKey);

      objectOutput.close();
    } else {
      FileInputStream fileInput = new FileInputStream(theFile);
      ObjectInputStream objectInput = new ObjectInputStream(fileInput);

      for (int i = 0; i < FILEKEYS; i++) {
        SecretKey key = (SecretKey) objectInput.readObject();

        if (i == KEYID) {
          secretKey = key;
        }
      }

      dcipher = Cipher.getInstance(secretKey.getAlgorithm());
      dcipher.init(Cipher.DECRYPT_MODE, secretKey);

      objectInput.close();
    }

    return dcipher;
  }
  
  
  public static void main(String[] args) throws Exception {
    Credentials creds = new Credentials();
    String[] credentials = creds.getCreds("/Users/ssubramanian/Documents/cloud9/bulk-upload/c9dev-creds");
    System.out.println("Creds: " + credentials[0] + "..." + credentials[1]);
  }

}
