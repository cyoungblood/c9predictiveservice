package com.c9.predictive.hooks;

import com.c9.predictive.util.Utils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import net.kuujo.vertigo.component.Component;
import net.kuujo.vertigo.component.InstanceContext;
import net.kuujo.vertigo.hook.ComponentHook;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.logging.Logger;

/**
 * Created by yngbldjr on 5/14/14.
 */
public class C9DataServicesCustomHook implements ComponentHook {


  @JsonIgnore private InstanceContext context;
  @JsonIgnore private Logger logger;
  @JsonIgnore private EventBus eventBus;
  @JsonIgnore private Vertx vertx;


  private String jdbcAddress;
  private String address;
  private Long timerId;

  public C9DataServicesCustomHook(){}

  public C9DataServicesCustomHook(String jdbcAddress){
    this.jdbcAddress = jdbcAddress;
  }


  @Override
  public void handleStart(Component component) {
    this.eventBus = component.vertx().eventBus();
    this.vertx = component.vertx();
    this.logger = component.container().logger();
    this.context = component.context();

    this.address = component.context().component().address();

    eventBus.publish(address, new JsonObject().putString("event", "start").putString("uri", context.uri()));
  }

  @Override
  public void handleSend(Object message) {
//    final String partitionId = ((JsonObject) message).getString(Utils.PropertyConstants.PARTITION_ID);
//
//    final Long stop = System.currentTimeMillis();

    if(timerId != null){
      vertx.cancelTimer(timerId);
    }

    eventBus.publish(address, new JsonObject().putString("event", "send").putValue("message", message));
  }

  @Override
  public void handleStop(Component subject) {
    eventBus.publish(address, new JsonObject().putString("event", "stop").putString("uri", context.uri()));
  }

  @Override
  public void handleReceive(Object message) {
    if(message instanceof JsonObject){

      final String partitionId = ((JsonObject) message).getString(Utils.PropertyConstants.PARTITION_ID);
      final String jdbcAdd = jdbcAddress;
      if(partitionId != null){


        timerId = vertx.setPeriodic(10000, new Handler<Long>() {
          @Override
          public void handle(Long aLong) {
            eventBus.send(jdbcAdd + ".info", new JsonObject().putString(Utils.PropertyConstants.PARTITION_ID, partitionId), new Handler<Message<JsonObject>>() {
              @Override
              public void handle(Message<JsonObject> message) {
                logger.info(message.body().encodePrettily());
              }
            });
          }
        });
      }

    }
    eventBus.publish(address, new JsonObject().putString("event", "receive").putValue("message", message));

  }

}
