package com.c9.predictive.hooks;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.kuujo.vertigo.component.Component;
import net.kuujo.vertigo.component.InstanceContext;
import net.kuujo.vertigo.hook.ComponentHook;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.logging.Logger;

/**
 * Created by yngbldjr on 5/14/14.
 */
public class C9CustomHook implements ComponentHook {

  @JsonIgnore
  private InstanceContext context;
  @JsonIgnore
  private EventBus eventBus;
  @JsonIgnore
  private Logger logger;

  private String address;
  private boolean suppressMessage = true;

  public C9CustomHook() {}

  @Override
  public void handleStart(Component component) {
    this.eventBus = component.vertx().eventBus();
    this.context = component.context();
    this.logger = component.container().logger();

    this.address = "components";

    eventBus.publish(address, new JsonObject().putString("event", "start").putString("url", context.uri()));
  }

  @Override
  public void handleSend(Object message) {
    if(suppressMessage)
      logger.info(this.context.address() + " (send) " + context.uri());
    else
      logger.info(this.context.address() + " (send) : " + message);

    eventBus.publish(address, new JsonObject().putString("event", "send").putValue("message", message).putString("componentAddress", context.address()));
  }

  @Override
  public void handleReceive(Object message) {
    if(suppressMessage)
      logger.info(this.context.address() + " (receive) " + context.uri());
    else
      logger.info(this.context.address() + " (receive) : " + message);
    eventBus.publish(address, new JsonObject().putString("event", "receive").putValue("message", message).putString("componentAddress", context.address()));
  }

  @Override
  public void handleStop(Component subject) {
    eventBus.publish(address, new JsonObject().putString("event", "stop").putString("uri", context.uri()));
  }
}
