package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.DefaultInputs;
import com.c9.predictive.util.Utils;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 1/22/15.
 */
public class ErrorHandlerComponent extends DefaultBaseComponentVerticle {

  public static class ErrorHandlerComponentInputs extends DefaultInputs {
    public static final String DUPLICATE = "duplicate";
    public static final String TIMEOUT = "timeout";
    public static final String NO_DATA = "no_data";
  }

  public static ErrorHandlerComponentInputs INPUTS() {return new ErrorHandlerComponentInputs(); }

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    input.port(INPUTS().TIMEOUT).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject event) {
        String partitionId = event.getString(Utils.PropertyConstants.PARTITION_ID);
        String customer = event.getString(Utils.PropertyConstants.CUSTOMER);
        output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(event, String.format("Error : Timeout occurred for %s [%s]", customer, partitionId), "CompletionEmail", true));
        output.port(OUTPUTS().OUT).send(event);
      }
    });

    input.port(INPUTS().NO_DATA).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject event) {
        String partitionId = event.getString(Utils.PropertyConstants.PARTITION_ID);
        String customer = event.getString(Utils.PropertyConstants.CUSTOMER);
        output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(event, String.format("No data for %s [%s]", customer, partitionId), "CompletionEmail", true));
        output.port(OUTPUTS().OUT).send(event);
      }
    });

    input.port(INPUTS().DUPLICATE).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject event) {
        String partitionId = event.getString(Utils.PropertyConstants.PARTITION_ID);
        String customer = event.getString(Utils.PropertyConstants.CUSTOMER);
        output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(event, String.format("Duplicate Request for %s [%s]", customer, partitionId), "DuplicateRequest", true));
        output.port(OUTPUTS().OUT).send(event);
      }
    });
  }

  @Override
  public void processInput(JsonObject event) {
    String partitionId = event.getString(Utils.PropertyConstants.PARTITION_ID);
    String customer = event.getString(Utils.PropertyConstants.CUSTOMER);
    String subject = event.getString(Utils.PropertyConstants.EMAIL_SUBJECT, String.format("Error : Exception for %s [%s]", customer, partitionId));
    output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(event, subject, "Error", true));
    output.port(OUTPUTS().OUT).send(event);
  }
}
