package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.components.config.CustomConfig;
import com.c9.predictive.util.Utils;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.json.JsonObject;

import javax.jms.*;
import java.util.HashMap;
import java.util.Map;

public class JMSSendComponent extends DefaultBaseComponentVerticle {

  String activeMQHost;
  String queue;

  ActiveMQConnectionFactory connectionFactory;
  Connection connection;
  String definitionBaseLocation;

  Logger log = LoggerFactory.getLogger(JMSListenComponent.class);

	public void start(final Future<Void> startedResult) {
      super.start(startedResult);
      definitionBaseLocation = container.config().getString("tableDefinitionLocation");

	  // local activeMQ if not set in config
	  activeMQHost = container.config().getString("activeMQHost", "tcp://localhost:61616");

      queue = container.config().getString("jms.send.queue");

      try {
        connectionFactory = new ActiveMQConnectionFactory(activeMQHost);
        connection = connectionFactory.createConnection();
        connection.start();
      } catch (Exception e){
        log.error("Not able to Listen to JMS queue with this config : " + container.config(), e);
        startedResult.setFailure(e);
      }

        startedResult.setResult(null);
	}

  @Override
  public void stop() {
    try {

      connection.stop();
      connection.close();
      connection = null;

      connectionFactory = null;

    } catch(Exception e){
      e.printStackTrace();
    }
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();
    if(queue == null && input.getString("queue") == null){
      errors.put("queue", "Either set the queue with the config or ensure it is passed as a parameter.");
    }
    if(input.getString("correlationId") == null){
      errors.put("correlationId", "CorrelationId required to send to the correct analytic.");
    }
    if(input.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME) == null){
      errors.put(Utils.PropertyConstants.ANALYTIC_HOSTNAME, "hostname required to send to the correct analytic.");
    }
    return errors;
  }

  @Override
  public void processInput(JsonObject input) {

    String sendToQueue = input.getString("queue", queue);
    String partitionId = input.getString(Utils.PropertyConstants.PARTITION_ID);
    String correlationId = input.getString(Utils.PropertyConstants.CORRELATION_ID);
    String hostname = input.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME);
    String servicename = input.getString("servicename");

    CustomConfig customConfig = CustomConfig.getCustomConfig(input);
    String scoreFileLocation = input.getString(Utils.PropertyConstants.SCORE_FILE_LOCATION);
    String durationScoreFileLocation = input.getString(Utils.PropertyConstants.DURATION_SCORE_FILE_LOCATION);

    String oppScoreFileDefinition = customConfig.getOppScoreVersion().getHelper().getOppScoreFileDefinition(definitionBaseLocation);
    String durationScoreFileDefinition = customConfig.getOppScoreVersion().getHelper().getDurationScoreFileDefinition(definitionBaseLocation);

    log.debug("QUEUE : " + sendToQueue);
    log.debug("partitionId : " + partitionId);
    log.debug("correlationId :" + correlationId);
    log.debug("hostname :" + hostname);
    log.debug("servicename :" + servicename);
    log.debug("OpportunityScoreFilePath :" + scoreFileLocation);
    log.debug("OpportunityScoreFileDefinitionPath :" + oppScoreFileDefinition);
    log.debug("DurationModelScoreFilePath :" + durationScoreFileLocation);
    log.debug("DurationModelScoreFileDefinitionPath :" + durationScoreFileDefinition);

    String errorMessage = errorsToString(input);

    Session session = null;
    Destination destination;

    try {

      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

      destination = session.createQueue(sendToQueue);

      TextMessage msg = session.createTextMessage("");

      msg.setStringProperty("PartitionId", partitionId);
      msg.setStringProperty(servicename, "TRUE");
      if(StringUtils.isNotEmpty(errorMessage) && (scoreFileLocation == null &&  durationScoreFileDefinition == null)){
        log.debug("Errors :" + errorMessage);
        msg.setStringProperty("errors", errorMessage);
      }
      if(scoreFileLocation != null){
        msg.setStringProperty("OpportunityScoreFilePath", scoreFileLocation);
        msg.setStringProperty("OpportunityScoreFileDefinitionPath", oppScoreFileDefinition);
      }
      if(durationScoreFileLocation != null){
        msg.setStringProperty("DurationModelScoreFilePath", durationScoreFileLocation);
        msg.setStringProperty("DurationModelScoreFileDefinitionPath", durationScoreFileDefinition);
      }
      // Txt type
      msg.setIntProperty("type", 3);
      msg.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);
      msg.setStringProperty("serviceName", "Predictive");
      msg.setJMSCorrelationID(correlationId);
      msg.setText("");

      session.createProducer(destination).send(msg);

      output.port(JMSSendComponent.OUTPUTS().OUT).send(input);
      log.info("Sent jms message back to wait queue.");
    } catch (Exception e) {
      log.error("Error sending message to JMS queue", e);
      output.port(JMSSendComponent.OUTPUTS().ERROR_OUT).send(input.putString("status", "error").putString("message", "" + e.getMessage()));
    } finally {
      if(session != null) {
        try { session.close();} catch (Exception e){log.error("Can not close JMS session.", e);}
      }
      // Cleanup;
      session = null;
      destination = null;
    }
  }
}