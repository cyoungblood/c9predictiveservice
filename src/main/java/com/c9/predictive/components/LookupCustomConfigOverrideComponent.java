package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by yngbldjr on 4/25/14.
 */
public class LookupCustomConfigOverrideComponent extends DefaultBaseComponentVerticle {

  Logger log = LoggerFactory.getLogger(LookupCustomConfigOverrideComponent.class);

  private String customConfigLocationFormat;
  private String predictiveBaseDir;
  private String outKey;
  private boolean failOnError;

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    JsonObject scoreConfig = this.container.config().getObject("scoreEngine", new JsonObject());

    predictiveBaseDir = scoreConfig.getString("predictiveBaseDir", "/cloud9/predictiveserviceconfig/test/");

    customConfigLocationFormat = this.container.config().getString("customConfigLocationFormat", predictiveBaseDir + "%s_%s" + "/config/config.ini");

    outKey = container.config().getString("outKey", Utils.PropertyConstants.CUSTOM_CONFIG_OUT_KEY);

    failOnError = container.config().getBoolean("failOnError", true);

    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();
    if(input.getString(Utils.PropertyConstants.CUSTOMER) == null){
      errors.put(Utils.PropertyConstants.CUSTOMER, "customer name is required.");
    }

    return errors;
  }

  @Override
  public void processInput(final JsonObject o) {

    final String customerName = o.getString(Utils.PropertyConstants.CUSTOMER);
    final String partitionId = o.getString(Utils.PropertyConstants.PARTITION_ID);
    final String file = o.getString("customConfigFile") != null ? o.getString("customConfigFile") : String.format(customConfigLocationFormat, customerName, partitionId);

    vertx.fileSystem().exists(file, new Handler<AsyncResult<Boolean>>() {
      @Override
      public void handle(AsyncResult<Boolean> booleanAsyncResult) {
        if(booleanAsyncResult.succeeded() && booleanAsyncResult.result()){
          try {
            JsonObject config = parse(file);

            if(config.getString("predictive_service_config") != null){
              o.putString("file", config.getString("predictive_service_config"));
            }

            if(!o.containsField("runScore"))
              o.putBoolean("runScore", config.getString(Utils.PropertyConstants.ENABLE_OPP_SCORE_PROPERTY, "true").equalsIgnoreCase("true"));

            if(!o.containsField("runDuration"))
              o.putBoolean("runDuration", config.getString(Utils.PropertyConstants.ENABLE_DURATION_SCORE_PROPERTY, "false").equalsIgnoreCase("true"));

            if(!o.containsField("uploadScoreSFDC"))
              o.putBoolean("uploadScoreSFDC", config.getString(Utils.PropertyConstants.UPLOAD_SCORE_SFDC, "false").equalsIgnoreCase("true"));

            output.port(OUTPUTS().OUT).send(o.putObject(outKey, config));

          } catch (IOException e){
            // This should never happen!
            log.error("File could not be found.", e);
            output.port(OUTPUTS().ERROR_OUT).send(addErrors(o, "File does not exist : " + file));
          }
          catch (Exception e){
            // This should never happen!
            log.error("Exception while parsing file", e);
            if(failOnError)
              output.port(OUTPUTS().ERROR_OUT).send(addErrors(o, String.format("Error reading file (%s), with error : %s",file, e.getMessage()+"")));
            else
              output.port(OUTPUTS().OUT).send(addErrors(o, "Could not parse file: " + file));
          }
        } else {
          log.info(String.format("Custom config not found for customer [%s] at location %s", customerName, file));
          output.port(OUTPUTS().OUT).send(o);
        }
      }
    });
  }

  @SuppressWarnings("unchecked")
  private JsonObject parse(String iniFile) throws IOException {
    Properties props = new Properties();
    props.load(new FileInputStream(new File(iniFile)));
    return new JsonObject((Map<String, Object>) (Map)props);
  }
}
