package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.helpers.RxEBUtil;
import com.c9.predictive.util.Utils;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Action1;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 4/25/14.
 */
public class LookupCustomerComponent extends DefaultBaseComponentVerticle {

  String customerYamlConfigFormat;
  String customerBaseDir;
  String customerExtractionLocationFormat;
  String ebAddress;
  RxEventBus rxeb;

  Logger log = LoggerFactory.getLogger(LookupCustomerComponent.class);

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    customerYamlConfigFormat = this.container.config().getString("customerYamlConfigFormat");
    customerBaseDir = this.container.config().getObject("scoreEngine").getString("predictiveBaseDir");
    customerExtractionLocationFormat = this.container.config().getObject("scoreEngine").getString("dataExtractLocationFormat");

    if(customerYamlConfigFormat == null){
      startResult.setFailure(new IllegalArgumentException("customerYamlConfigFormat is required"));
    }

    ebAddress = container.config().getObject("dataServices", new JsonObject()).getString("address");

    if(ebAddress == null){
      startResult.setFailure(new IllegalArgumentException("dataServices.address is required"));
    }

    this.rxeb = new RxEventBus(vertx.eventBus());

    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {

    Map<String, String> errors = new HashMap<>();
    if(input.getString(Utils.PropertyConstants.PARTITION_ID) == null) {
      errors.put(Utils.PropertyConstants.PARTITION_ID, "partitionId is required.");
    }
    return errors;
  }

  @Override
  public void processInput(final JsonObject input) {
    final String partitionId = input.getString(Utils.PropertyConstants.PARTITION_ID);

    JsonObject hostNameCall = new JsonObject();
    hostNameCall.putString(Utils.PropertyConstants.PARTITION_ID, "mac");
    String sQuery = String.format("select b.machine, a.catalogname from ShowProvisions() a join ShowServices() b on a.service=b.service where a.partition='%s'", input.getString(Utils.PropertyConstants.PARTITION_ID));
    hostNameCall.putString("stmt", sQuery);
    hostNameCall.putString("format", "json");

    log.info(String.format("Making jdbc call %s to address %s", hostNameCall, ebAddress));

    Observable<RxMessage<JsonObject>> customerRequest = rxeb.send(ebAddress, hostNameCall);

    Long start = System.currentTimeMillis();

    input.putNumber("start", start).putString("startDate", formatTime(start));

    RxEBUtil.raise(customerRequest).subscribe(new Action1<JsonObject>() {
      @Override
      public void call(JsonObject jsonObject) {
        jsonObject.putString(Utils.PropertyConstants.PARTITION_ID, partitionId);
        if (jsonObject.getString("status").equalsIgnoreCase("ok") && jsonObject.getArray("result").size() > 0) {

          String catalogName = ((JsonObject)jsonObject.getArray("result").get(0)).getString("catalogname");

          String cleanedCompanyName = cleanCompanyName(catalogName);

          if(!input.containsField(Utils.PropertyConstants.ANALYTIC_HOSTNAME))
            input.putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, ((JsonObject)jsonObject.getArray("result").get(0)).getString("machine"));

          String yamlConfigFile = String.format(customerYamlConfigFormat, cleanedCompanyName, partitionId);
          String yamlHistoryConfigFile = yamlConfigFile.replace(".yaml", "_history.yaml");
          String lastOpenExtractDateFile = String.format(customerBaseDir + "%s_%s/last_open_extract_date.yaml", cleanedCompanyName, partitionId);

          String dataExtractionLocation = String.format(customerExtractionLocationFormat, cleanedCompanyName, partitionId) + input.getString(Utils.PropertyConstants.CORRELATION_ID) + File.separator;

          String cleanupExtractionFolder = String.format(customerExtractionLocationFormat, cleanedCompanyName, partitionId);
          String customerScoreLocation = String.format(customerBaseDir + "%s_%s/scores", cleanedCompanyName, partitionId);

          JsonArray foldersToClean = new JsonArray();
          foldersToClean.add(cleanupExtractionFolder);
          foldersToClean.add(customerScoreLocation);

          jsonObject
              .putString(Utils.PropertyConstants.CUSTOMER, cleanedCompanyName)
              .putString("file", yamlConfigFile)
              .putString("lastOpenExtractDateFile", lastOpenExtractDateFile)
              .putString("historyFile", yamlHistoryConfigFile)
              .putArray("foldersToClean", foldersToClean)
              .putString(Utils.PropertyConstants.DATA_EXTRACTION_LOCATION, dataExtractionLocation);

          output.port(LookupCustomerComponent.OUTPUTS().OUT).send(input.mergeIn(jsonObject));
        } else {
          output.port(LookupCustomerComponent.OUTPUTS().ERROR_OUT).send(addErrors(input.mergeIn(jsonObject), "No customer found for the given partitionId."));
        }
      }
    });
  }

  private String cleanCompanyName(String companyName) { return companyName.replaceAll("[^A-Za-z0-9]", ""); }
}
