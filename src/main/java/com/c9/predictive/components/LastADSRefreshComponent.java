package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.helpers.RxEBUtil;
import com.c9.predictive.util.Utils;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Action1;
import rx.util.functions.Func1;

import java.util.Map;

/**
 * Created by yngbldjr on 7/8/14.
 */
public class LastADSRefreshComponent extends DefaultBaseComponentVerticle {

  private RxEventBus rxeb;
  private String jdbcAddress;

  Logger log = LoggerFactory.getLogger(JMSListenComponent.class);

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    jdbcAddress = container.config().getString("address");

    rxeb = new RxEventBus(vertx.eventBus());

    if(jdbcAddress == null){
      Exception e = new IllegalArgumentException("Missing config parameter - address");
      log.error("Error starting LastADSRefreshComponent", e);
      startResult.setFailure(e);
      return;
    }

    startResult.setResult(null);

  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    return super.validateInput(input);
  }

  @Override
  public void processInput(final JsonObject input) {
    JsonObject refreshCall = new JsonObject()
                                      .putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, input.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME))
                                      .putString(Utils.PropertyConstants.PARTITION_ID, input.getString(Utils.PropertyConstants.PARTITION_ID))
                                      .putString("stmt", "select * from system.RefreshInformation")
                                      .putString("format", "json");

    Observable<RxMessage<JsonObject>> refreshInfo = rxeb.send(jdbcAddress, refreshCall);

    RxEBUtil.raise(refreshInfo)
       .onErrorReturn(new Func1<Throwable, JsonObject>() {
         @Override
         public JsonObject call(Throwable throwable) {
           log.error("Failed to get refresh info.", throwable);
           return new JsonObject().putString("status", "error").putString("message", throwable.getMessage());
         }
       })
       .subscribe(new Action1<JsonObject>() {
         @Override
         public void call(JsonObject jsonObject) {
           if ("ok".equalsIgnoreCase(jsonObject.getString("status"))) {
             input.putObject("refreshInfo", (JsonObject) jsonObject.getArray("result").get(0));
             log.info("Refresh Info : " + jsonObject.encodePrettily());
             output.port(OUTPUTS().OUT).send(input);
           } else {
             addErrors(input, "Failed to get refresh Information");
             output.port(OUTPUTS().ERROR_OUT).send(input);
           }
         }
       });
  }
}
