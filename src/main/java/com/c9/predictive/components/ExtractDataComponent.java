package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.TimeoutOutputs;
import com.c9.predictive.util.Utils;
import com.google.common.base.Joiner;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import net.kuujo.vertigo.io.batch.InputBatch;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Action1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by yngbldjr on 4/25/14.
 */
public class ExtractDataComponent extends DefaultBaseComponentVerticle {

  private RxEventBus rxeb;
  private String jdbcAddress = "";

  Logger log = LoggerFactory.getLogger(ExtractDataComponent.class);

  public static class ExtractDataComponentOutputs extends TimeoutOutputs {
    public static final String NO_DATA = "no_data";
  }

  public static ExtractDataComponentOutputs OUTPUTS() { return new ExtractDataComponentOutputs(); }

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    jdbcAddress = container.config().getString("address");

    rxeb = new RxEventBus(vertx.eventBus());

    input.port(INPUTS().IN).batchHandler(new Handler<InputBatch>(){

      @Override
      public void handle(final InputBatch batchEvent) {

        final ConcurrentLinkedQueue<JsonObject> queries = new ConcurrentLinkedQueue<>();
        final AtomicReference<JsonObject> originalInput = new AtomicReference<>();
        final AtomicBoolean timeoutOccured = new AtomicBoolean(false);
        final AtomicReference<String> partitionId = new AtomicReference<>("");
        final AtomicLong startTime = new AtomicLong(0);
        final AtomicLong timerId = new AtomicLong(0);

        batchEvent.startHandler(new Handler<Object>() {
          @Override
          public void handle(Object o) {
            startTime.set(System.currentTimeMillis());
          }
        });

        batchEvent.messageHandler(new Handler<JsonObject>() {
          @Override
          public void handle(final JsonObject query) {
            queries.add(query);
          }
        });
        batchEvent.endHandler(new Handler<JsonObject>() {
          @Override
          public void handle(JsonObject event) {
            log.info("Got all messages for Batch.");
            partitionId.set(event.getString(Utils.PropertyConstants.PARTITION_ID));
            originalInput.set(event);
            checkIfTimeoutTimerNeeded(event);
            performNextQuery();
          }

          private void sendCompletion(){
            vertx.cancelTimer(timerId.get());
            timerId.set(0);

            JsonObject extractionStats = new JsonObject();
            extractionStats.putObject("input", originalInput.get());
            extractionStats.putString(Utils.PropertyConstants.PARTITION_ID, partitionId.get());
            extractionStats.putString("status", "extraction compelte");
            extractionStats.putString("currentQuery", "None");

            addTimer("DataExtraction", startTime.get());

            output.port(OUTPUTS().UPDATE_STATUS).send(extractionStats);

            log.info("Total Extract Time : " + formatElapsedTime(System.currentTimeMillis() - startTime.get()));

            output.port(OUTPUTS().OUT).send(originalInput.get());
          }

          private void performNextQuery(){
            if(queries.size() == 0 && !timeoutOccured.get()){
              sendCompletion();
              return;
            }

            JsonObject query = queries.poll();

            if(query == null){
              log.info(String.format("No queries to process for %s", partitionId.get()));
              // Timeout should have been handled already by the timeout handler from earlier in the process.
              return;
            }

            updateStatus(partitionId.get(), query);

            if(query.containsField("SQL")){
              log.info("Simple SQL" + query.getString("SQL"));
              performSingleSQL(partitionId.get(), query);
            } else if(query.containsField("idSQL") && query.containsField("templateSQL")){
              log.info("id and template pair ");
              log.info("idSQL " + query.getString("idSQL"));
              log.info("templateSQL " + query.getString("templateSQL"));
              performIdTemplatePairSQL(partitionId.get(), query);
            } else {
              log.error("Unsupported Query Object! ");
              sendError("Incorrect configuration in yaml file!.");
            }
          }

          private void sendError(String errorMessage){
            log.error(errorMessage);
            originalInput.set(addErrors(originalInput.get(), errorMessage));
            cancelTimer();
            output.port(OUTPUTS().ERROR_OUT).send(originalInput.get());
          }

          private void performIdTemplatePairSQL(final String partitionId, final JsonObject query) {

            final Long start = System.currentTimeMillis();

            final String idSQL = Utils.sqlReplacements(query.getString("idSQL"), query.getObject(Utils.PropertyConstants.QUERY_REPLACEMENTS, new JsonObject()).toMap());

            JsonObject idListQuery = new JsonObject()
                .putString(Utils.PropertyConstants.PARTITION_ID, partitionId)
                .putString("stmt", idSQL)
                .putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, query.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME))
                .putString("format", "json");

            String workQueueAddress = query.getString("workQueueAddress", jdbcAddress);

            Observable<RxMessage<JsonObject>> idQueryObservable = rxeb.send(workQueueAddress, idListQuery);

            idQueryObservable.subscribe(new Action1<RxMessage<JsonObject>>(){
              @Override
              public void call(RxMessage<JsonObject> idSelectResult) {
                if (idSelectResult.body().getString("status").equalsIgnoreCase("ok") && idSelectResult.body().getArray("result", new JsonArray()).size() == 0) {
                  addTimer("Id's for " + query.getString("tableName"), start);
                  if(query.getBoolean("mandatory", true)) {
                    originalInput.set(addErrors(originalInput.get(), "No data returned for mandatory table " + query.getString("tableName") + ". Query : " + idSQL));
                    noDataFoundError();
                  } else {
                    // Continue to perform the next query
                    performNextQuery();
                  }
                }
                else if (idSelectResult.body().getString("status").equalsIgnoreCase("ok") && idSelectResult.body().getArray("result", new JsonArray()).size() > 0) {
                  log.info("Received ids for : " + partitionId + " : " + query.getString("tableName"));

                  String idReplacementString = createIDLists(idSelectResult.body().getArray("result"));

                  // Run the template and save the file.
                  JsonObject queryReplacements = query.getObject(Utils.PropertyConstants.QUERY_REPLACEMENTS, new JsonObject());
                  queryReplacements.putString(Utils.PropertyConstants.ID_LIST_REPLACEMENT, idReplacementString);

                  final String sql = Utils.sqlReplacements(query.getString("templateSQL"), query.getObject(Utils.PropertyConstants.QUERY_REPLACEMENTS).toMap());

                  // Build request for final csv results from id and template sql
                  JsonObject csvQuery = new JsonObject()
                      .putString(Utils.PropertyConstants.PARTITION_ID, partitionId)
                      .putString("stmt", sql)
                      .putString("format", "csv")
                      .putString("fileName", query.getString("path"))
                      .putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, query.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME))
                      .putString("csvSeparator", "\t");

                  String workQueueAddress = query.getString("workQueueAddress", jdbcAddress);

                  log.info(String.format("Making Call for Data to address %s with %s ", workQueueAddress, csvQuery));
                  Observable<RxMessage<JsonObject>> templateSQLObservable = rxeb.send(workQueueAddress, csvQuery);

                  templateSQLObservable.subscribe(new Action1<RxMessage<JsonObject>>() {
                    @Override
                    public void call(RxMessage<JsonObject> jsonObjectRxMessage) {
                      addTimer(query.getString("tableName"), start);

                      if("ok".equalsIgnoreCase(jsonObjectRxMessage.body().getString("status")) && jsonObjectRxMessage.body().getString("result") != null) {
                        log.info("Got data to " + query.getString("path"));
                        if(query.getBoolean("mandatory", true) && jsonObjectRxMessage.body().getLong("resultCount", 0) == 0) {
                          originalInput.set(addErrors(originalInput.get(), "No data returned for  mandatory table " + query.getString("tableName") + ". Query : " + sql + " Response : " + jsonObjectRxMessage.body()));
                          log.error("Error getting data for " + query + " Response : " + jsonObjectRxMessage.body());
                          noDataFoundError();
                        } else {
                          performNextQuery();
                        }
                      } else {
                        sendError(String.format("Error getting data for %s Response : %s",query, jsonObjectRxMessage.body()));
                      }
                    }
                  });
                } else {
                  addTimer(query.getString("tableName"), start);

                  sendError(String.format("Could not get id's for %s", query.getString("tableName")));
                }
              }
            });
          }

          private void cancelTimer(){
            if(timerId.get() > 0) {
              log.info("Cancel timeout timer.");
              vertx.cancelTimer(timerId.get());
            }
          }

          private void noDataFoundError() {
            log.error("No Data Found. Sending Message on.");

            cancelTimer();

            if(!timeoutOccured.get()) {

              output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(originalInput.get(),
                                                                       String.format("No Data for %s [%s]", originalInput.get().getString(Utils.PropertyConstants.CUSTOMER), originalInput.get().getString(Utils.PropertyConstants.PARTITION_ID)),
                                                                       "CompletionEmail",
                                                                       false));
              output.port(OUTPUTS().NO_DATA).send(originalInput.get());
            }
          }

          private void updateStatus(String partitionId, JsonObject currentQueryToProcess) {

            JsonObject extractionStats = new JsonObject();
            extractionStats.putObject("input", originalInput.get());
            extractionStats.putString(Utils.PropertyConstants.PARTITION_ID, partitionId);
            extractionStats.putString("status", "extraction in progress");
            extractionStats.putString("currentQuery", currentQueryToProcess.getString("tableName"));

            output.port(OUTPUTS().UPDATE_STATUS).send(extractionStats);

          }

          private void addTimer(String timerName, Long start){
            originalInput.set(ExtractDataComponent.this.addTimer(originalInput.get(), timerName, start));
          }

          private void performSingleSQL(final String partitionId, final JsonObject query) {
            final Long start = System.currentTimeMillis();

            final String sql = Utils.sqlReplacements(query.getString("SQL"), query.getObject(Utils.PropertyConstants.QUERY_REPLACEMENTS).toMap());

            JsonObject csvQuery = new JsonObject()
                .putString(Utils.PropertyConstants.PARTITION_ID, partitionId)
                .putString("stmt", sql)
                .putString("format", "csv")
                .putString("fileName", query.getString("path"))
                .putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, query.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME))
                .putString("csvSeparator", "\t");

            String workQueueAddress = query.getString("workQueueAddress", jdbcAddress);

            Observable<RxMessage<JsonObject>> templateSQLObservable = rxeb.send(workQueueAddress, csvQuery);

            templateSQLObservable.subscribe(new Action1<RxMessage<JsonObject>>() {
              @Override
              public void call(RxMessage<JsonObject> jsonObjectRxMessage) {
                addTimer(query.getString("tableName"), start);
                if("ok".equalsIgnoreCase(jsonObjectRxMessage.body().getString("status")) && jsonObjectRxMessage.body().getString("result") != null) {
                  log.info("Got data to " + query.getString("path"));
                  if(query.getBoolean("mandatory", true) && jsonObjectRxMessage.body().getLong("resultCount") == 0) {
                    originalInput.set(addErrors(originalInput.get(), "No data returned for  mandatory table " + query.getString("tableName") + ". Query : " + sql + " Response : " + jsonObjectRxMessage.body()));
                    log.error("Error getting data for " + query + " Response : " + jsonObjectRxMessage.body());
                    noDataFoundError();
                  } else {
                    performNextQuery();
                  }
                } else {
                  sendError(String.format("Error getting data for %s Response : %s",query, jsonObjectRxMessage.body()));
                }
              }
            });
          }

          private void checkIfTimeoutTimerNeeded(final JsonObject event) {
            if(event.containsField("timeoutTimestamp") && timerId.get() == 0L){

              DateTime now = DateTime.now();
              DateTime timeout = new DateTime(event.getLong("timeoutTimestamp"));

              if(now.isAfter(timeout)){
                timedOut();
                return;
              }

              Interval interval = new Interval(now, timeout);
              // Set the timeout timer
              timerId.set(vertx.setTimer(interval.toDurationMillis(), new Handler<Long>() {
                @Override
                public void handle(Long timerEvent) {
                  timedOut();
                }
              }));
            }
          }

          private void timedOut() {
            timerId.set(0);
            queries.clear();
            timeoutOccured.set(true);
            originalInput.set(addErrors(originalInput.get(), "Timeout occurred while extracting data.  The extraction and predictive processes has been terminated."));
            output.port(OUTPUTS().TIMEOUT).send(originalInput.get());
          }
        });
      }


    });

    startResult.setResult(null);
  }

  private String createIDLists(JsonArray result) {
    if(result == null || result.size() == 0){
      return null;
    }

    List<String> ids = new ArrayList<>();

    for(Object o : result){
      JsonObject idResult = (JsonObject)o;
      String id = idResult.getString("Id");
      ids.add(id);
    }
    return Joiner.on("','").skipNulls().join(ids);
  }
}