package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.json.JsonObject;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 4/25/14.
 */
public class UpdateYAMLConfig extends DefaultBaseComponentVerticle {

  DumperOptions options;

  private String fileKey;
  private String inKey;

  Logger log = LoggerFactory.getLogger(UpdateYAMLConfig.class);

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    options = new DumperOptions();
    options.setPrettyFlow(true);
    options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
    options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
    options.setExplicitStart(false);
    options.setExplicitEnd(false);
    options.setWidth(1000);

    fileKey = container.config().getString("fileKey", "file");
    inKey = container.config().getString("inKey", "config");

    startResult.setResult(null);

  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();

    if(input.getString(fileKey) == null){
      errors.put(fileKey, fileKey + "parameter is required.");
    }

    if(input.getObject(inKey) == null){
      errors.put(inKey, inKey + "object is required.");
    }

    return errors;
  }

  @Override
  public void processInput(final JsonObject input) {
    final String file = input.getString(fileKey);

    try {

      createEmptyIfNotExist(file);

      Map<String, Object> configMap = input.getObject(inKey).toMap();

      Yaml yaml = new Yaml(options);

      String configString = yaml.dump(configMap);

      vertx.fileSystem().writeFile(file, new Buffer(configString), new Handler<AsyncResult<Void>>() {
        @Override
        public void handle(AsyncResult<Void> voidAsyncResult) {
          if(voidAsyncResult.succeeded()){
            output.port(OUTPUTS().OUT).send(input);
          } else {
            String message = String.format("Not able to save config to file (%s).  Reason : %s", file, voidAsyncResult.cause().getMessage() + "");
            output.port(OUTPUTS().ERROR_OUT).send(addErrors(input, message));
          }
        }
      });
    } catch(Exception e){
      log.error("Unable to get yaml file ", e);
      output.port(OUTPUTS().ERROR_OUT).send(addErrors(input, "Not able to load yaml config file."));
      return;
    }
  }

  private File createEmptyIfNotExist(String file) throws IOException {
    File f = new File(file);
    if(!f.exists()){
      f.getParentFile().mkdirs();
      f.createNewFile();
    }
    return f;
  }
}
