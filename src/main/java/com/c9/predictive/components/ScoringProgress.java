package com.c9.predictive.components;

import com.c9.generic.components.data.HasKeyInClusterMap;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 5/19/15.
 */
public class ScoringProgress extends HasKeyInClusterMap {

  @Override
  public void processInput(JsonObject input) {
    System.out.println("\n\n\n PROCESS INPUT FROM ScoringProgress - consider handling score type \"retrospective\" " + input);
    super.processInput(input);
  }
}
