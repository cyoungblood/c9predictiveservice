package com.c9.predictive.components.sfdcupload;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.DefaultOutputs;
import com.c9.predictive.components.config.CustomConfig;
import com.c9.predictive.components.config.OppScoreDefinitionHelper;
import com.c9.predictive.components.config.OppScoreVersion;
import com.c9.predictive.util.Credentials;
import com.c9.predictive.util.Utils;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.sforce.async.*;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Action1;
import rx.util.functions.Func1;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UploadScoresToSF extends DefaultBaseComponentVerticle {

  Logger log = LoggerFactory.getLogger(UploadScoresToSF.class);

  //input params
  private static final String USERNAME = "userName";
  private static final String PASSWORD = "password";
  private static final String SCORE_CSV = "scoreFileLocationSFDC";
  private static final String PARTITION_ID = Utils.PropertyConstants.PARTITION_ID;

  //SF table
  private static final String CUSTOM_SCORE_TABLE = "c9oppscoring__C9Score__c";

  private static final String SEL_FEATURE_SQL = "select feature_name, feature_desc, Id from ads.PredictiveFeatureNames";

  private static final int BATCH_SIZE = 8000; //should be less than 10,000

  private RxEventBus rxeb;
  private String jdbcAddress;
  private String baseLoaderDirFormat;

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    jdbcAddress = container.config().getString("address");

    baseLoaderDirFormat = container.config().getString("base.loader.dir.format");

    rxeb = new RxEventBus(vertx.eventBus());
    // init others
    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();

    if (!input.containsField(SCORE_CSV)) {
      errors.put(SCORE_CSV, "Missing scores csv location");
    }

    if (!input.containsField(PARTITION_ID)) {
      errors.put(PARTITION_ID, "Missing partitionId");
    }

    //Either userName/password OR loaderDir is needed.
    if(baseLoaderDirFormat == null && (!input.containsField(USERNAME) || !input.containsField(PASSWORD))) {
      errors.put("LOADER_DIR", "Missing Loader Dir OR userName/password");
    }

    return errors;
  }

  @Override
  public void processInput(final JsonObject input) {
    final String partitionId = input.getString(PARTITION_ID);

    final CustomConfig customConfig = CustomConfig.getCustomConfig(input);

    try {
      if(input.getString(USERNAME) == null || input.getString(PASSWORD) == null) {
        String customerLoaderDir = String.format(baseLoaderDirFormat, input.getString(Utils.PropertyConstants.CUSTOMER), input.getString(PARTITION_ID), input.getString(PARTITION_ID));
        String[] creds = new Credentials().getCreds(customerLoaderDir);
        input.putString(USERNAME, creds[0]);
        input.putString(PASSWORD, creds[1]);
      }

      final JsonObject featureListQuery = new JsonObject()
          .putString(Utils.PropertyConstants.PARTITION_ID, partitionId)
          .putString("stmt", SEL_FEATURE_SQL)
          .putString("format", "json")
          .putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, input.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME));

      log.info("Making Feature List SQL call : " + featureListQuery);

      Observable<RxMessage<JsonObject>> featureQueryObservable = rxeb.send(jdbcAddress, featureListQuery);

      featureQueryObservable
          .onErrorReturn(new Func1<Throwable, RxMessage<JsonObject>>() {
            @Override
            public RxMessage<JsonObject> call(Throwable throwable) {
              log.error("UploadScores failed for: " + partitionId, throwable);
              output.port(DefaultOutputs.ERROR_OUT).send(input.mergeIn(buildException(throwable)));
              addErrors(input, throwable.getMessage(), throwable);
              String subject = String.format("Error while processing input for SF upload for: %s [%s]", input.getString(Utils.PropertyConstants.CUSTOMER), input.getString(PARTITION_ID));
              output.port(DefaultOutputs.SEND_EMAIL).send(createEmailObject(input, subject, "Error", true));
              return null;
            }
          })
          .subscribe(new Action1<RxMessage<JsonObject>>() {
            @Override
            public void call(RxMessage<JsonObject> featuresResult) {

              try {
                log.info("Results size from SQL : " + featuresResult.body().size());

                JsonArray results = featuresResult.body().getArray("result");

                if(results != null) {
                  customConfig.getOppScoreVersion().getHelper().initFeaturesMap(results);
                } else {
                	String errMsg = featuresResult.body().getField("message");
                	log.warn("Could not get features from ADS, continue to process scores: " + errMsg);
                }

                 processScoreCSV(input, customConfig);
              } catch(Exception e){
                log.error("UploadScores failed for: " + partitionId, e);
                output.port(DefaultOutputs.ERROR_OUT).send(input.mergeIn(buildException(e)));
                addErrors(input, e.getMessage(), e);
                String subject = String.format("Error while processing input for SF upload for: %s [%s]", input.getString(Utils.PropertyConstants.CUSTOMER), input.getString(PARTITION_ID));
                output.port(DefaultOutputs.SEND_EMAIL).send(createEmailObject(input, subject, "Error", true));
              }
            }
          });
    } catch (Exception ex) {
      log.error("UploadScores failed for: " + partitionId, ex);
      output.port(DefaultOutputs.ERROR_OUT).send(input.mergeIn(buildException(ex)));
      addErrors(input, ex.getMessage(), ex);
      String subject = String.format("Error while processing input for SF upload for: %s [%s]", input.getString(Utils.PropertyConstants.CUSTOMER), input.getString(PARTITION_ID));
      output.port(DefaultOutputs.SEND_EMAIL).send(createEmailObject(input, subject, "Error", true));
    }
  }

  private void performOperationOnData(JsonObject input, Queue<Operation> operations) {

    Operation opp = operations.poll();
    if(opp == null){
      StringBuilder subject = new StringBuilder();
      subject.append("Score upload results for: ").append(input.getString(Utils.PropertyConstants.CUSTOMER)).append(" [").append(input.getString(PARTITION_ID)).append("]");
      output.port(DefaultOutputs.SEND_EMAIL).send(createEmailObject(input, subject.toString(), "UploadCompleteEmail", false));
      return;
    }

    int totalCount = 0;
    int batchCount = 0;
    int batchSize = BATCH_SIZE;

    final String userName = input.getString(USERNAME);
    final String password = input.getString(PASSWORD);

    List<BatchInfo> batchInfoList = new ArrayList<>();
    BufferedReader rdr = null;
    File tmpFile = null;
    BulkConnection connection = null;
    JobInfo job = null;
    try {
      connection = getBulkConnection(userName, password);
      job = createJob(CUSTOM_SCORE_TABLE, connection, opp.jobType);

      //to write to a batch
      tmpFile = File.createTempFile("bulkAPI_"+opp.jobType+"_"+input.getString(Utils.PropertyConstants.CORRELATION_ID), ".csv");
      FileOutputStream tmpOut = new FileOutputStream(tmpFile);

      tmpOut.write(opp.headerRow);

      for(byte[] row : opp.rows){
        batchCount++;
        totalCount++;

        tmpOut.write(row);

        if(batchCount >= batchSize && batchCount % batchSize == 0) {
          batchInfoList.add(createBatch(tmpOut, tmpFile, connection, job));
          batchCount = 0;
        }
      }

      if(batchCount > 0) {

        batchInfoList.add(createBatch(tmpOut, tmpFile, connection, job));
      }
    } catch(Exception ex) {
      log.error("Could not process scores csv", ex);
      output.port(DefaultOutputs.ERROR_OUT).send(input.mergeIn(buildException(ex)));
      addErrors(input, ex.getMessage(), ex);
      output.port(DefaultOutputs.SEND_EMAIL).send(createEmailObject(input, String.format("Error while processing scores csv for SF Upload for %s [%s]", input.getString(Utils.PropertyConstants.CUSTOMER), input.getString(Utils.PropertyConstants.PARTITION_ID)), "Error", true));
    } finally {
      try {
        if(rdr != null) {
          rdr.close();
        }
        tmpFile.delete();
        tmpFile = null;
      } catch(IOException io) {}

      log.info(String.format("Total scores processed for operation %s on partition %s : %s", opp.jobType, input.getString(PARTITION_ID), totalCount));

      try {
        closeJob(connection, job.getId());
        awaitCompletion(connection, job, batchInfoList, input, operations, totalCount);
      } catch (AsyncApiException ex) {
        log.error("Could not process scores csv", ex);
        output.port(DefaultOutputs.ERROR_OUT).send(input.mergeIn(buildException(ex)));
        addErrors(input, ex.getMessage(), ex);
        output.port(DefaultOutputs.SEND_EMAIL).send(createEmailObject(input, String.format("Error while processing scores csv for SF Upload for %s [%s]", input.getString(Utils.PropertyConstants.CUSTOMER), input.getString(Utils.PropertyConstants.PARTITION_ID)), "Error", true));
      }

    }
  }

  public class Operation {
    public OppScoreVersion oppScoreVersion;
    public OperationEnum jobType;
    public byte[] headerRow;
    public List<byte[]> rows;

    public Operation(OppScoreVersion oppScoreVersion, OperationEnum jobType, byte[] headerRow, List<byte[]> rows){
      this.oppScoreVersion = oppScoreVersion;
      this.jobType = jobType;
      this.headerRow = headerRow;
      this.rows = rows;
    }
  }

  /**
   *
   * @param input
   * Process the input scores csv and send and event to do a bulk query in batches of 1000.
   */
  private void processScoreCSV(final JsonObject input, CustomConfig customConfig) throws Exception {
    //Read from csv
    String scoreCSV = input.getString(SCORE_CSV);

    CSVParser parser = new CSVParser(new FileReader(scoreCSV), CSVFormat.DEFAULT.withHeader());

    Queue<Operation> operations = new ConcurrentLinkedQueue<>();

    List<CSVRecord> sfdcIds = getSFDCIds(input, customConfig);

    List<byte[]> rows = new ArrayList<>();
    if(sfdcIds != null && sfdcIds.size() > 0) {
      for (CSVRecord r : sfdcIds) {
        rows.add((r.get("Id") + "\n").getBytes("UTF-8"));
      }
      operations.add(new Operation(customConfig.getOppScoreVersion(), OperationEnum.delete, "Id\n".getBytes("UTF-8"), rows));
    }

    List<byte[]> insertRows = new ArrayList<>();
    for(CSVRecord record : parser.getRecords()){
      insertRows.add(customConfig.getOppScoreVersion().getHelper().convertScoreRowToSFDCBytes(record));
    }
    operations.add(new Operation(customConfig.getOppScoreVersion(), OperationEnum.upsert, customConfig.getOppScoreVersion().getHelper().getHeaderBytes(), insertRows));

    performOperationOnData(input, operations);
  }

  public List<CSVRecord> getData(String query, final BulkConnection bulkConnection, JobInfo job) throws Exception {

    BatchInfo info;
    ByteArrayInputStream bout = new ByteArrayInputStream(query.getBytes());// convert query into ByteArrayInputStream
    info = bulkConnection.createBatchFromStream(job, bout);// creates batch from ByteArrayInputStream
    QueryResultList list;
    int totalLoops=0;
    int timeToWait = 5000;
    for (int i = 0; i < 20; i++) // Max waits
    {
      totalLoops++;
      // This is not ideal since its blocking, but we deployed this component as a worker so it will not block the event loop.
      Thread.sleep(timeToWait); // 5 sec

      info = bulkConnection.getBatchInfo(job.getId(), info.getId());
      // If Batch Status is Completed,get QueryResultList and store in
      // queryResults.
      if (info.getState() == BatchStateEnum.Completed) {
        list = bulkConnection.getQueryResultList(job.getId(), info.getId());
        List<CSVRecord> records = new ArrayList<>();
        if(list == null || list.getResult() == null){
          return records;
        }

        for (String resultId : list.getResult()) {
          Reader decoder = new InputStreamReader(bulkConnection.getQueryResultStream(job.getId(), job.getId(), resultId), "UTF-8");

          CSVParser parser = new CSVParser(new BufferedReader(decoder), CSVFormat.DEFAULT.withHeader());
          records.addAll(parser.getRecords());
        }

        return records;
      } else if (info.getState() == BatchStateEnum.Failed) {
        logger.error("-------------- failed ----------" + info);
        throw new Exception("Failed to get id's from object");
      } else {
        logger.info("-------------- waiting ----------"+ info);
      }
    }
    throw new RuntimeException("Query was not able to process in " + (totalLoops * timeToWait) + " seconds.");
  }

  private List<CSVRecord> getSFDCIds(JsonObject input, CustomConfig customConfig) throws Exception {
    final String userName = input.getString(USERNAME);
    final String password = input.getString(PASSWORD);

    BulkConnection connection;
    JobInfo job;
    try {
      connection = getBulkConnection(userName, password);
      job = createJob(CUSTOM_SCORE_TABLE, connection, OperationEnum.query);

      String scoreCSV = input.getString(SCORE_CSV);
      CSVParser parser = new CSVParser(new FileReader(scoreCSV), CSVFormat.DEFAULT.withHeader());
      List<String> externalIds = new ArrayList<>();
      List<CSVRecord> returnRecords = new ArrayList<>();
      log.info(String.format("Creating batches of %d ", customConfig.getSFDCBatchUploadSize()));
      List<List<CSVRecord>> batches = Lists.partition(parser.getRecords(), customConfig.getSFDCBatchUploadSize());

      for(List<CSVRecord> batch : batches){
        for(CSVRecord r : batch){
          externalIds.add(new String(customConfig.getOppScoreVersion().getHelper().getOpportunityIdFromRow(r)));
        }
        String query =String.format(
            "SELECT Id FROM %s where %s in (%s)", CUSTOM_SCORE_TABLE, OppScoreDefinitionHelper.EXTERNAL_ID, "'" + Joiner.on("','").join(externalIds).concat("'"));
        returnRecords.addAll(getData(query, connection, job));
        externalIds.clear();
      }
      return returnRecords;
    } catch(Exception ex) {
      log.error("Could not process scores csv", ex);
      output.port(DefaultOutputs.ERROR_OUT).send(input.mergeIn(buildException(ex)));
      addErrors(input, ex.getMessage(), ex);
      throw ex;
    }
  }

  private BatchInfo createBatch(FileOutputStream tmpOut, File tmpFile,
			BulkConnection connection, JobInfo jobInfo) throws IOException, AsyncApiException {
		tmpOut.flush();
		tmpOut.close();
		FileInputStream tmpInputStream = new FileInputStream(tmpFile);
		try {
			return connection.createBatchFromStream(jobInfo,tmpInputStream);
		} finally {
			tmpInputStream.close();
		}
	}

  private JobInfo createJob(String sobjectType, BulkConnection connection, OperationEnum jobType) throws AsyncApiException {
    JobInfo job = new JobInfo();
    job.setObject(sobjectType);
    job.setOperation(jobType);
    job.setExternalIdFieldName(OppScoreDefinitionHelper.EXTERNAL_ID);
    job.setContentType(ContentType.CSV);
    return connection.createJob(job);
  }

  private void closeJob(BulkConnection connection, String jobId) throws AsyncApiException {
    JobInfo job = new JobInfo();
    job.setId(jobId);
    job.setState(JobStateEnum.Closed);
    connection.updateJob(job);
  }

  /**
     * Gets the results of the operation and checks for errors.
     */
  private void checkResults(BulkConnection connection, JobInfo job, BatchInfo batchInfo, JsonObject input, int totalCount) {
    int updateCount = 0;
    int createCount = 0;
    int errorCount = 0;
    int deletedCount = 0;

    try {
      if(job.getOperation() == OperationEnum.delete){
        deletedCount = totalCount;
      } else {

        CSVReader rdr = new CSVReader(connection.getBatchResultStream(job.getId(), batchInfo.getId()));
        List<String> resultHeader = rdr.nextRecord();
        int resultCols = resultHeader.size();

        List<String> row;
        while ((row = rdr.nextRecord()) != null) {

          Map<String, String> resultInfo = new HashMap<>();
          for (int i = 0; i < resultCols; i++) {
            resultInfo.put(resultHeader.get(i), row.get(i));
          }
          boolean success = Boolean.valueOf(resultInfo.get("Success"));
          boolean created = Boolean.valueOf(resultInfo.get("Created"));
          String id = resultInfo.get("Id");
          String error = resultInfo.get("Error");
          if (success && created) {
            if (log.isDebugEnabled()) {
              createCount++;
              log.debug("Created row with id " + id);
            }
          } else if (!success) {
            log.error("Failed with error: " + error);
            addErrors(input, error);
            errorCount++;
          } else {
            updateCount++;
          }
        }
      }
    } catch(Exception ex) {
      log.error("Exception", ex);
      addErrors(input, ex.getMessage(), ex);
    }

    input.putNumber("DeletedCount", input.getInteger("DeletedCount", 0) + deletedCount);
    input.putNumber("UpdateCount", input.getInteger("UpdateCount", 0) + updateCount);
    input.putNumber("CreateCount", input.getInteger("CreateCount", 0) + createCount);
    input.putNumber("ErrorCount", input.getInteger("ErrorCount", 0) + errorCount);

    log.info("Number of rows deleted: " + deletedCount);
    log.info("Number of rows updated: " + updateCount);
    log.info("Number of rows in error: " + errorCount);
    log.info("Number of rows created: " + createCount);
  }

  /**
   * Wait for a job to complete by polling the Bulk API.
   *
   * @param connection
   *          BulkConnection used to check results.
   * @param job
   *          The job awaiting completion.
   * @param operations
   * @param totalCount
   * @throws AsyncApiException
   */
  private void awaitCompletion(final BulkConnection connection, final JobInfo job, final List<BatchInfo> batchInfoList, final JsonObject input, final Queue<Operation> operations, final int totalCount)  {

    final Set<String> incomplete = new HashSet<>();
    for(BatchInfo batchInfo : batchInfoList) {
      incomplete.add(batchInfo.getId());
    }

    vertx.setPeriodic(5000l, new Handler<Long>() {

      public void handle(Long timerID) {
        if (incomplete.isEmpty()) {
          vertx.cancelTimer(timerID);
          for (BatchInfo batchInfo : batchInfoList) {
            checkResults(connection, job, batchInfo, input, totalCount);
             performOperationOnData(input, operations);
          }
        }

        if (log.isInfoEnabled()) {
          log.info("Awaiting results..." + incomplete.size());
        }

        BatchInfo[] statusList = null;
        try {
          statusList = connection.getBatchInfoList(job.getId()).getBatchInfo();
        } catch (AsyncApiException aex) {
          log.error("Error in AsyncAPI", aex);
          addErrors(input, aex.getMessage(), aex);
        }

        for (BatchInfo b : statusList) {
          if (b.getState() == BatchStateEnum.Completed || b.getState() == BatchStateEnum.Failed) {
            logger.info("Batch state: " + b.getStateMessage());

            if(b.getState() == BatchStateEnum.Failed) {
              input.putString("BatchState", b.getStateMessage());
              addErrors(input, "SF Upload Batch status: failed. : " + b.getStateMessage());
            }

            if (incomplete.remove(b.getId())) {
              if (log.isInfoEnabled()) {
                log.info("BATCH STATUS:\n" + b);
              }
            }
          }
        }
      }
    });

  }

  /**
   * Create the BulkConnection used to call Bulk API operations.
   */
  private BulkConnection getBulkConnection(String userName, String password)
      throws ConnectionException, AsyncApiException {
    ConnectorConfig partnerConfig = new ConnectorConfig();
    partnerConfig.setUsername(userName);
    partnerConfig.setPassword(password);
    partnerConfig
        .setAuthEndpoint("https://login.salesforce.com/services/Soap/u/30.0");
    // Creating the connection automatically handles login and stores
    // the session in partnerConfig
    new PartnerConnection(partnerConfig);
    // When PartnerConnection is instantiated, a login is implicitly
    // executed and, if successful,
    // a valid session is stored in the ConnectorConfig instance.
    // Use this key to initialize a BulkConnection:
    ConnectorConfig config = new ConnectorConfig();
    config.setSessionId(partnerConfig.getSessionId());
    // The endpoint for the Bulk API service is the same as for the normal
    // SOAP uri until the /Soap/ part. From here it's '/async/versionNumber'
    String soapEndpoint = partnerConfig.getServiceEndpoint();
    String apiVersion = "30.0";
    String restEndpoint = soapEndpoint.substring(0,
        soapEndpoint.indexOf("Soap/"))
        + "async/" + apiVersion;
    config.setRestEndpoint(restEndpoint);
    // This should only be false when doing debugging.
    config.setCompression(true);
    // Set this to true to see HTTP requests and responses on stdout
    config.setTraceMessage(false);
    BulkConnection connection = new BulkConnection(config);
    return connection;
  }
}
