package com.c9.predictive.components.sfdcupload;

import com.c9.generic.components.common.BasicDecisionRouter;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 7/14/14.
 */
public class UploadScoreToSFDCDecisionComponent extends BasicDecisionRouter {
  @Override
  public void processInput(JsonObject input) {
    if(input.containsField(booleanFieldName)){
      // Overridden param present so honor whatever value that is first
      if(input.getBoolean(booleanFieldName, false)){
        copyFile(input);
        output.port(BasicDecisionRouter.OUTPUTS().YES).send(input);
      } else {
        output.port(BasicDecisionRouter.OUTPUTS().NO).send(input);
      }
    } else if(input.containsField(objectFieldName)){
      if(input.getObject(objectFieldName, new JsonObject()).getBoolean(booleanFieldName, false)){
        copyFile(input);
        output.port(BasicDecisionRouter.OUTPUTS().YES).send(input);
      }
      else {
        output.port(BasicDecisionRouter.OUTPUTS().NO).send(input);
      }
    } else {
      output.port(BasicDecisionRouter.OUTPUTS().NO).send(input);
    }
  }

  private void copyFile(JsonObject input) {
    try {
      String sfdcScoreFileLocation = input.getString("scoreFileLocation") + "_sfdc";
      if(!vertx.fileSystem().existsSync(sfdcScoreFileLocation))
        vertx.fileSystem().copySync(input.getString("scoreFileLocation"), sfdcScoreFileLocation, true);
      input.putString("scoreFileLocationSFDC", sfdcScoreFileLocation);
    } catch (Exception e) {
      addErrors(input, e.getMessage());
      output.port(BasicDecisionRouter.OUTPUTS().ERROR_OUT).send(input);
    }
  }
}
