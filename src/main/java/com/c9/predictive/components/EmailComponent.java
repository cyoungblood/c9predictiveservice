package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.components.config.CustomConfig;
import com.c9.predictive.util.Utils;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import rx.util.functions.Action1;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by yngbldjr on 6/27/14.
 */
public class EmailComponent extends DefaultBaseComponentVerticle {

  RxEventBus rxeb;
  JsonObject emailConfig;
  String toEmailAddress;
  String ccEmailAddress;
  String fromEmailAddress;
  String baseInternalUrl;

  TemplateEngine emailTemplateEngine;

  Logger log = LoggerFactory.getLogger(EmailComponent.class);

  @Override
  public void start(final Future<Void> startResult) {
    super.start(startResult);

    rxeb = new RxEventBus(this.vertx.eventBus());

    baseInternalUrl = String.format("http://%s:%d/internal/api/1/", Utils.getHostName(), container.config().getInteger("webPort", 9999));

    emailConfig = this.container.config().getObject("emailModule");

    if(emailConfig == null){
      Exception e = new IllegalArgumentException("emailModule object is required in config.");
      log.error("emailModule object is required in config.", e);
      startResult.setFailure(e);
      return;
    }

    if(emailConfig.getString("mailTemplateLocation") == null){
      Exception e = new IllegalArgumentException("emailModule.mailTemplateLocation property is required in config.");
      log.error("emailModule.mailTemplateLocation property is required in config.", e);
      startResult.setFailure(e);
      return;
    }

    container.deployModule(emailConfig.getString("identifier"), emailConfig.getObject("config"), new Handler<AsyncResult<String>>() {
      @Override
      public void handle(AsyncResult<String> stringAsyncResult) {
        toEmailAddress = emailConfig.getString("notifications.to.email.address", "dl.qe-pass@c9inc.com");
        ccEmailAddress = emailConfig.getString("notifications.cc.email.address");
        fromEmailAddress = emailConfig.getString("notifications.from.email.address", "predictive-service-default@c9inc.com");

        FileTemplateResolver emailTemplateResolver = new FileTemplateResolver();
        emailTemplateResolver.setTemplateMode("HTML5");
        emailTemplateResolver.setPrefix(emailConfig.getString("mailTemplateLocation"));
        emailTemplateResolver.setSuffix(".html");
        emailTemplateResolver.setCacheable(false);

        emailTemplateEngine = new TemplateEngine();
        emailTemplateEngine.setTemplateResolver(emailTemplateResolver);

        startResult.setResult(null);
      }
    });
  }

  @Override
  public void processInput(JsonObject input) {

    CustomConfig customConfig = CustomConfig.getCustomConfig(input.getObject("templateModel"));
    boolean isError = input.getBoolean("isError", false);

    if(!isError && customConfig.suppressSuccessEmails()){
      // Ignore this email as its not an error and customer is suppressing success emails.
      return;
    }

    Context ctx = new Context(new Locale("en", "US"));

    String template = input.getString("template");

    JsonObject templateModel = input.getObject("templateModel");
    templateModel.putArray("links", generateLinks(templateModel, input.getBoolean("generateProgressLink", false)));

    ctx.setVariables(templateModel.toMap());

    String body = emailTemplateEngine.process(template, ctx);

    JsonObject email = new JsonObject();

    email.putString("to", toEmailAddress);
    if(ccEmailAddress!=null){
      email.putString("cc", ccEmailAddress);
    }
    email.putString("from", fromEmailAddress);
    if(container.env().get("environment") != null){
      email.putString("subject", container.env().get("environment") + ":" + input.getString("subject"));
    } else {
      email.putString("subject", input.getString("subject"));
    }

    email.putString("body", body);

    log.info("\n\tEmail Sent with subject : " + email.getString("subject") + "\n");

    rxeb.send(emailConfig.getObject("config").getString("address"), email).subscribe(new Action1<RxMessage<Object>>() {
      @Override
      public void call(RxMessage<Object> objectRxMessage) {
        output.port(OUTPUTS().OUT).send(objectRxMessage.body());
      }
    });
  }

  private JsonArray generateLinks(JsonObject input, boolean generateProgressUrl) {
    JsonArray links = input.getArray("links", new JsonArray());

    String partitionId = input.getString(Utils.PropertyConstants.PARTITION_ID);

    if(links.size() > 0){
      for(Object l : links){
        JsonObject link = (JsonObject)l;

        String href = link.getString("href");
        if(href != null && !href.contains(baseInternalUrl)) {
          href = baseInternalUrl + href;
          link.putString("href", href);
        }
      }
    }

    if(partitionId != null) {
      if (generateProgressUrl){
        JsonObject inProgress = new JsonObject();
        inProgress.putString("href", baseInternalUrl + "map/scoring_progress/" + partitionId).putString("title", "Scoring Progress for " + partitionId);
        links.add(inProgress);
      }
      JsonObject history = new JsonObject();
      history.putString("href", baseInternalUrl + "map/scoring_history/"+partitionId).putString("title", "Scoring History for " + partitionId);
      links.add(history);
    } else {
      if(generateProgressUrl) {
        JsonObject inProgress = new JsonObject();
        inProgress.putString("href", baseInternalUrl + "map/scoring_progress").putString("title", "Scoring Progress for all.");
        links.add(inProgress);
      }
      JsonObject history = new JsonObject();
      history.putString("href", baseInternalUrl + "map/scoring_history").putString("title", "Scoring History for all.");
      links.add(history);
    }
    return links;
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {

    Map<String, String> errors = new HashMap<>();

    if(input.getString("subject") == null){
      errors.put("subject", "subject or body is required");
    }

    if(input.getString("template") == null && input.getString("body") == null){
      errors.put("template_or_body", "template or body is required");
    }

    if(input.getString("template") != null && input.getObject("templateModel") == null){
      errors.put("templateBody", "templateModel is required");
    }
    return errors;
  }
}