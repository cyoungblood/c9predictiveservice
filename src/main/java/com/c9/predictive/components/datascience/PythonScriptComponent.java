package com.c9.predictive.components.datascience;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.UpdateStatsOutputs;
import com.c9.predictive.components.config.CustomConfig;
import com.c9.predictive.util.Utils;
import com.google.common.base.Joiner;
import org.apache.commons.exec.*;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.json.JsonObject;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by yngbldjr on 4/29/14.
 *
 *
 */
public class PythonScriptComponent extends DefaultBaseComponentVerticle {

  private String pythonScript;
  private String predictiveBaseDir;
  private String loggingConfig;
  private String pythonExecutable;
  private String scoreFileName;
  private String predictiveHome;
  private String pipelineWrapper;
  private String configProperty;
  private String modelProperty;
  private String scriptTitle;

  private String defaultConfig;
  private String defaultModel;
  private String fileLocationOutProperty;
  private String resultOutProperty;
  private String customerLogName;
  private String verboseLoggingProperty;

  static final SimpleDateFormat LOG_FORMAT = new SimpleDateFormat("MM-dd-yyyy");

  Logger log = LoggerFactory.getLogger(PythonScriptComponent.class);

  public static UpdateStatsOutputs OUTPUTS() {
    return new UpdateStatsOutputs();
  }

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    JsonObject scoreConfig = this.container.config().getObject("scoreEngine");

    if(scoreConfig == null){
      throw new RuntimeException("Score Config is mandatory.");
    }

    pythonExecutable = scoreConfig.getString("pythonExecutable");
    if(pythonExecutable == null){
      throw new RuntimeException("pythonExecutable is mandatory part of score config.");
    }
    predictiveBaseDir = scoreConfig.getString("predictiveBaseDir");
    if(predictiveBaseDir == null){
      throw new RuntimeException("predictiveBaseDir is mandatory part of score config.");
    }
    loggingConfig = scoreConfig.getString("loggingConfig");
    if(loggingConfig == null){
      throw new RuntimeException("loggingConfig is mandatory part of score config.");
    }
    predictiveHome = scoreConfig.getString("predictiveHome");
    if(predictiveHome == null){
      throw new RuntimeException("predictiveHome is mandatory part of score config.");
    }
    pipelineWrapper = scoreConfig.getString("pipelineWrapper");
    if(pipelineWrapper == null){
      throw new RuntimeException("pipelineWrapper is mandatory part of score config.");
    }


    // Script deviations score/duration
    pythonScript = this.container.config().getString("pythonScript");
    if(pythonScript == null){
      throw new RuntimeException("pythonScript is mandatory part of score config.");
    }
    scoreFileName = this.container.config().getString("scoreFileName");
    if(scoreFileName == null){
      throw new RuntimeException("scoreFileName is mandatory part of score config.");
    }
    defaultConfig = this.container.config().getString("defaultConfig", "config.yaml");   //config.yaml
    if(defaultConfig == null){
      throw new RuntimeException("defaultConfig is mandatory part of score config.");
    }
    defaultModel = this.container.config().getString("defaultModel");     //opp_score.model
    if(defaultModel == null){
      throw new RuntimeException("defaultModel is mandatory part of score config.");
    }
    configProperty = this.container.config().getString("configProperty"); // config.yaml
    if(configProperty == null){
      throw new RuntimeException("configProperty is mandatory part of score config.");
    }
    modelProperty = this.container.config().getString("modelProperty");   // opp_score.model
    if(modelProperty == null){
      throw new RuntimeException("modelProperty is mandatory part of score config.");
    }
    scriptTitle = this.container.config().getString("scriptTitle");       //Opportunity Score or Duration model Score
    if(scriptTitle == null){
      throw new RuntimeException("scriptTitle is mandatory part of score config.");
    }
    fileLocationOutProperty = this.container.config().getString("fileLocationOutProperty"); // durationFileLocation
    if(fileLocationOutProperty == null){
      throw new RuntimeException("fileLocationOutProperty is mandatory part of score config.");
    }
    resultOutProperty = this.container.config().getString("resultOutProperty");             // scoreResults
    if(resultOutProperty == null){
      throw new RuntimeException("resultOutProperty is mandatory part of score config.");
    }
    customerLogName = this.container.config().getString("customerLogName");                 // durationmodelscores_
    if(customerLogName == null){
      throw new RuntimeException("customerLogName is mandatory part of score config.");
    }
    verboseLoggingProperty = this.container.config().getString("verboseLoggingProperty");
    if(verboseLoggingProperty == null){
      throw new RuntimeException("verboseLoggingProperty is mandatory part of score config.");
    }

    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();

    if(input.getString(Utils.PropertyConstants.CUSTOMER) == null) {
      errors.put(Utils.PropertyConstants.CUSTOMER, "customer is required input.");
    }

    if(input.getObject("config") == null){
      errors.put("config", "config object is required input.");
    }

    return errors;
  }

  public static class TimeoutException extends Exception {}

  @Override
  public void processInput(final JsonObject input) {
    CustomConfig customerConfig = CustomConfig.getCustomConfig(input);

    final String companyName = input.getString(Utils.PropertyConstants.CUSTOMER);

    final String partitionId = input.getString(Utils.PropertyConstants.PARTITION_ID);

    final String baseCustomerLocation = String.format("%s%s_%s", predictiveBaseDir, companyName, partitionId);

    Date today = new Date();

    String customerDataExtractLocation = input.getString(Utils.PropertyConstants.DATA_EXTRACTION_LOCATION);

    final String mode = input.getString(Utils.PropertyConstants.SCORE_MODE, "score");

    final String customerLocationLog = String.format("%s/logs/%s_%s_%s.log", baseCustomerLocation, customerLogName, mode, LOG_FORMAT.format(today));

    final String scoreLocation = String.format("%s/scores/%s", baseCustomerLocation, input.getString(Utils.PropertyConstants.CORRELATION_ID));

    // Defaults
    String modelConfigYaml = String.format("%s/%s", defaultConfig, baseCustomerLocation);
    if(customerConfig.getProperty(configProperty) != null){
      modelConfigYaml = customerConfig.getProperty(configProperty);
    }

    String modelLocation = String.format("%s/%s",defaultModel, baseCustomerLocation);

    if(customerConfig.getProperty(modelProperty) != null){
      modelLocation = customerConfig.getProperty(modelProperty);
    }

    File logFile = new File(customerLocationLog);

    // Make sure log dir is created.
    if(!logFile.getParentFile().exists()){
      logFile.getParentFile().mkdirs();
    }

    String dateSuffix = input.getString("dateSuffix");

    if(dateSuffix == null) {
      SimpleDateFormat dateSuffixFormat = new SimpleDateFormat("dd-MM-yyyy");
      dateSuffix = dateSuffixFormat.format(today);
    }
    try {

      if(!(new File(modelConfigYaml)).exists()){
        throw new IllegalArgumentException("Config not found : " + modelConfigYaml);
      }

      if(!(new File(modelLocation)).exists()){
        throw new IllegalArgumentException("Model not found : " + modelLocation);
      }

      final CommandLine cmdLine = new CommandLine(pipelineWrapper);

      cmdLine.addArgument(predictiveHome);

      List<String> arguments = new ArrayList<>();

      arguments.add(pythonExecutable);
      arguments.add(pythonScript);
      arguments.add("--config");
      arguments.add(modelConfigYaml);
      arguments.add("--customer");
      arguments.add(companyName);
      arguments.add("--model");
      arguments.add(modelLocation);
      arguments.add("--mode");
      arguments.add(mode);
      arguments.add("--scores");
      arguments.add(scoreLocation);
      arguments.add("--scoresname");
      arguments.add(scoreFileName);
      arguments.add("--open");
      arguments.add(customerDataExtractLocation);
      arguments.add("--suffix");
      arguments.add(dateSuffix);
      arguments.add("-l");
      arguments.add(loggingConfig) ;
      arguments.add("-g");
      arguments.add(customerLocationLog);

      if("true".equalsIgnoreCase(customerConfig.getProperty(verboseLoggingProperty, "false"))){
          arguments.add("-v");
      }

      log.info(cmdLine.getExecutable());
      log.info(Joiner.on(" ").join(cmdLine.getArguments()));

      cmdLine.addArgument(Joiner.on(' ').join(arguments), false);

      log.info("Running python for : " + companyName + " with args : " + Joiner.on(" ").join(cmdLine.getArguments()));

      updateStartStatus(input, cmdLine.getArguments());

      DefaultExecutor executor = new DefaultExecutor();
      // 0 is considered success.
      executor.setExitValue(0);

      final Long start = System.currentTimeMillis();

      DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler(){
        @Override
        public void onProcessComplete(int exitValue) {
          super.onProcessComplete(exitValue);
          completeProcess(input, exitValue, customerLocationLog, scoreLocation, start, companyName, cmdLine.getArguments());
        }

        @Override
        public void onProcessFailed(ExecuteException e) {
          super.onProcessFailed(e);
          failProcess(input, e, customerLocationLog, start, companyName, cmdLine.getArguments());
        }
      };

      Long timeoutTimestamp = input.getLong("timeoutTimestamp", 0L);

      if(timeoutTimestamp > 0){
        DateTime now = DateTime.now();

        DateTime timeout = new DateTime(timeoutTimestamp);

        if(now.isAfter(timeout)){
          log.error("Now is after timeout - stop and sent to timeout error path.");
          throw new TimeoutException();
        } else {
          // Set the watchdog from now till timestamp.
          Interval interval = new Interval(now, timeout);
          ExecuteWatchdog watchdog = new ExecuteWatchdog(interval.toDurationMillis());
          executor.setWatchdog(watchdog);
        }
      }

      PumpStreamHandler ps = new PumpStreamHandler(new FileOutputStream(logFile));

      executor.setStreamHandler(ps);

      executor.execute(cmdLine, resultHandler);
    } catch(TimeoutException e) {
      output.port(OUTPUTS().ERROR_OUT).send(input);
    } catch (IllegalArgumentException e){
      output.port(OUTPUTS().ERROR_OUT).send(addErrors(input, e.getMessage()));
      String emailTitle = String.format("Error while producing %s for %s [%s]", scriptTitle, companyName, partitionId);
      output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(input, emailTitle, "ScriptCompletionEmail", true));
    } catch(Exception e){
      e.printStackTrace();
      log.error("sending to error output " + e.getMessage());
      addErrors(input, e.getMessage());
      output.port(PythonScriptComponent.OUTPUTS().ERROR_OUT).send(addErrors(input, "Error executing " + pythonScript));
      String emailTitle = String.format("Error while producing %s for %s [%s]", scriptTitle, companyName, partitionId);
      output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(input, emailTitle, "ScriptCompletionEmail", true));
    }
  }

  private void failProcess(final JsonObject input, final ExecuteException e, final String customerLocationLogFile, final Long start, final String companyName, final String[] arguments) {

    log.error("Failed script : " + e.getMessage() + " log : " + customerLocationLogFile, e);
    vertx.fileSystem().readFile(customerLocationLogFile, new Handler<AsyncResult<Buffer>>() {
      @Override
      public void handle(AsyncResult<Buffer> event) {
        if (event.succeeded()) {
          String fileContents = event.result().toString(StandardCharsets.UTF_8.name());
          logger.info("Script results for " + companyName + " : " + fileContents);

          int rc = e.getExitValue();

          String partitionId = input.getString(Utils.PropertyConstants.PARTITION_ID);
          String emailTitle = String.format("Error producing %s for %s [%s]", scriptTitle, companyName, partitionId);

          addFileLink(input, String.format("%s Log location", scriptTitle), customerLocationLogFile);

          addErrors(input, "Tried to run script with arguments " + Joiner.on(" ").join(arguments));
          input.putString(resultOutProperty, fileContents);

          switch (rc) {
            case 2:
              addErrors(input, scriptTitle + " returned error code 2 : Fatal Error thrown by argparse module.");
              break;
            case 11:
              addErrors(input, scriptTitle + " returned error code 11 : Fatal Exception during command line parsing, config.");
              break;
            case 12:
              addErrors(input, scriptTitle + " returned error code 12 - Fatal Exception.");
              break;
            case 13:
              addErrors(input, "Unable to run " + scriptTitle + " due to not enough data.");
              emailTitle = String.format("Error : %s - Not enough data for %s [%s]", scriptTitle, companyName, partitionId);
              break;
            case 14:
              addErrors(input, "Unable to run " + scriptTitle + ". Dataset is invalid. Error code 14");
              emailTitle = String.format("Error : %s - Dataset is invalid for %s [%s]", scriptTitle, companyName, partitionId);
              break;
            case 143:
              addErrors(input, scriptTitle + " Timed out");
              emailTitle = String.format("Error : Timeout occurred while producing %s -  for %s [%s]", scriptTitle, companyName, partitionId);
              break;
            default:
              addErrors(input, pythonScript + " returned unknown error code.", e);
          }

          output.port(PythonScriptComponent.OUTPUTS().ERROR_OUT).send(input);
          output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(input, emailTitle, "ScriptCompletionEmail", true));
        }
      }
    });
  }

  private void completeProcess(final JsonObject input, final int rc, String customerLocationLog, final String scoreLocation, Long start, final String companyName, final String... arguments) {
    log.info("Completed process with exitCode : " + rc);

    addTimer(input, pythonScript, start, Arrays.asList("Ran script with " + Joiner.on(" ").join(arguments)));
    // Put the location of the score script into the response so that we can put it on the email and look it up.
    addFileLink(input, String.format("%s Score Log location", scriptTitle), customerLocationLog);

    String partitionId = input.getString(Utils.PropertyConstants.PARTITION_ID);
    final String emailTitle = String.format("Successfully produced %s for %s [%s]", scriptTitle, companyName, partitionId);

    vertx.fileSystem().readFile(customerLocationLog, new Handler<AsyncResult<Buffer>>() {
      @Override
      public void handle(AsyncResult<Buffer> event) {
        if (event.succeeded()) {
          String fileContents = event.result().toString(StandardCharsets.UTF_8.name());
          Date today = new Date();
          String fileOutLocation = String.format("%s/%s_%tY_%tm_%td.csv", scoreLocation, scoreFileName, today, today, today);

          input.getObject("config", new JsonObject()).putString(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE, Utils.LAST_EXTRACT_DATE_FORMAT.format(new Date()));
          input.getObject("lastOpenExtractInfo", new JsonObject()).putString(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE, Utils.LAST_EXTRACT_DATE_FORMAT.format(new Date()));

          output.port(PythonScriptComponent.OUTPUTS().OUT).send(input.putString(resultOutProperty, fileContents).putString(fileLocationOutProperty, fileOutLocation));
        } else {
          output.port(PythonScriptComponent.OUTPUTS().ERROR_OUT).send(addErrors(input, "" + event.cause().getMessage()));
          output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(input, emailTitle, "ScriptCompletionEmail", true));
        }
      }
    });
  }

  private void updateStartStatus(JsonObject input, String... wrapper) {
    JsonObject publishStatus = new JsonObject();
    publishStatus.putObject("input", input);
    publishStatus.putString(Utils.PropertyConstants.PARTITION_ID, input.getString(Utils.PropertyConstants.PARTITION_ID));
    publishStatus.putString("status", String.format("%s in progress.", scriptTitle));
    publishStatus.putString("info", "Running command with args : " + Joiner.on(" ").join(wrapper));
    output.port(OUTPUTS().UPDATE_STATUS).send(publishStatus);
  }
}
