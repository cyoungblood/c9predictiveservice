package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;
import org.yaml.snakeyaml.resolver.Resolver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 4/25/14.
 */
public class ParseYAMLComponent extends DefaultBaseComponentVerticle {

  private String fileKey;
  private String outKey;
  private boolean failOnError;
  private JsonArray valuesToKeep;

  Logger log = LoggerFactory.getLogger(LookupCustomerComponent.class);

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    fileKey = container.config().getString("fileKey", "file");
    outKey = container.config().getString("outKey", "config");
    failOnError = container.config().getBoolean("failOnError", true);

    valuesToKeep = container.config().getArray("valuesToKeep");


    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();
    String file = input.getString(fileKey);
    if(org.apache.commons.lang3.StringUtils.isBlank(file)){
      errors.put(fileKey, fileKey + " parameter is required.");
    }
    return errors;
  }

  @Override
  public void processInput(final JsonObject o) {
    final String file = o.getString(fileKey);

    vertx.fileSystem().exists(file, new Handler<AsyncResult<Boolean>>() {
      @Override
      public void handle(AsyncResult<Boolean> booleanAsyncResult) {
        if(booleanAsyncResult.succeeded() && booleanAsyncResult.result()){
          try {
            JsonObject config = parse(file);

            if(valuesToKeep != null) {
              config = trimmedConfig(config, valuesToKeep);
            }

            output.port(OUTPUTS().OUT).send(o.putObject(outKey, config));

          } catch (FileNotFoundException e){
            // This should never happen!
            log.error("Yaml file could not be found.", e);
            if(failOnError)
              output.port(OUTPUTS().ERROR_OUT).send(addErrors(o, "File does not exist : " + file));
            else
              output.port(OUTPUTS().OUT).send(addInfo(o, "File does not exist : " + file));
          }
          catch (Exception e){
            // This should never happen!
            log.error("Exception while parsing YAML file", e);
            if(failOnError)
              output.port(OUTPUTS().ERROR_OUT).send(addErrors(o, String.format("Error reading yaml for file (%s), with error : %s",file, e.getMessage()+"")));
            else
              output.port(OUTPUTS().OUT).send(addErrors(o, "Could not parse yaml file: " + file));
          }
        } else {
          if(failOnError)
            output.port(OUTPUTS().ERROR_OUT).send(addErrors(o, "File does not exist : " + file));
          else
            output.port(OUTPUTS().OUT).send(addInfo(o, "File does not exist : " + file));
        }
      }
    });
  }

  private static JsonObject trimmedConfig(JsonObject config, JsonArray valuesToKeep) {
    JsonObject trimmedConfig = new JsonObject();
    for(Object valueName : valuesToKeep){
      if(config.containsField((String)valueName)){
        trimmedConfig.putValue((String)valueName, config.getValue((String)valueName));
      }
    }
    return trimmedConfig;
  }

  @SuppressWarnings("unchecked")
  private JsonObject parse(String yamlFile) throws FileNotFoundException {
    Yaml yaml = new Yaml(new Constructor(), new Representer(), new DumperOptions(), new CustomResolver());
    return new JsonObject((Map<String, Object>) yaml.load(new FileInputStream(new File(yamlFile))));
  }

  // Use custom resolver.
  // This ensures that timestamps/dates are not automatically converted as Date is not support in the JsonObject
  public class CustomResolver extends Resolver {
    @Override
    protected void addImplicitResolvers() {
      addImplicitResolver(Tag.BOOL, BOOL, "yYnNtTfFoO");
      addImplicitResolver(Tag.FLOAT, FLOAT, "-+0123456789.");
      addImplicitResolver(Tag.INT, INT, "-+0123456789");
      addImplicitResolver(Tag.MERGE, MERGE, "<");
      addImplicitResolver(Tag.NULL, NULL, "~nN\0");
      addImplicitResolver(Tag.NULL, EMPTY, null);
      // addImplicitResolver(Tags.TIMESTAMP, TIMESTAMP, "0123456789");
      addImplicitResolver(Tag.VALUE, VALUE, "=");
    }
  }
}
