package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.json.JsonObject;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by yngbldjr on 4/25/14.
 */
public class UpdateConfigComponent extends DefaultBaseComponentVerticle {

  DumperOptions options;

  Logger log = LoggerFactory.getLogger(UpdateConfigComponent.class);

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    options = new DumperOptions();
    options.setPrettyFlow(true);
    options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
    options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
    options.setExplicitStart(false);
    options.setExplicitEnd(false);
    options.setWidth(1000);

    startResult.setResult(null);

  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();

    if(input.getString("file") == null){
      errors.put("file", "file parameter is required.");
    }

    return errors;
  }

  @Override
  public void processInput(final JsonObject input) {
    final String file = input.getString("historyFile");

    try {
      File historyConfig = createEmptyIfNotExist(file);

      Yaml yaml = new Yaml();

      Map<String, Object> configMap = (Map<String, Object>) yaml.load(new FileInputStream(historyConfig));

      if(configMap == null){
        configMap = new HashMap<>();
      }

      Date today = new Date();

      String last_open_extract_date = Utils.LAST_EXTRACT_DATE_FORMAT.format(today);

      Object extractDatesObject = configMap.get(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE);
      List<String> extractDatesList = new ArrayList<>();

      if(extractDatesObject != null && extractDatesList instanceof List) {
        extractDatesList = (List<String>)extractDatesObject;
      }

      extractDatesList.add(last_open_extract_date);

      configMap.put(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE, extractDatesList);

      Object runs = configMap.get("runs");
      Map<String, Object> runData = new HashMap<>();

      if(runs != null && runs instanceof Map){
        runData = (HashMap<String, Object>) runs;
      }

      // This can be found in the logs
      runData.remove("scoreResults");

      runData.put(last_open_extract_date, input.toMap());

      configMap.put("runs", runData);

      yaml = new Yaml(options);

      // This can be found in the logs
      configMap.remove("scoreResults");

      String configString = yaml.dump(configMap);

      vertx.fileSystem().writeFile(file, new Buffer(configString), new Handler<AsyncResult<Void>>() {
        @Override
        public void handle(AsyncResult<Void> voidAsyncResult) {
          if(voidAsyncResult.succeeded()){
            output.port(OUTPUTS().OUT).send(input);
          } else {
            String message = String.format("Not able to save config to file (%s).  Reason : %s", file, voidAsyncResult.cause().getMessage() + "");
            output.port(OUTPUTS().ERROR_OUT).send(addErrors(input, message));
          }
        }
      });
    } catch(Exception e){
      log.error("Unable to get yaml file ", e);
      output.port(OUTPUTS().ERROR_OUT).send(addErrors(input, "Not able to load yaml config file."));
      return;
    }
  }

  private File createEmptyIfNotExist(String file) throws IOException {
    File f = new File(file);
    if(!f.exists()){
      f.getParentFile().mkdirs();
      f.createNewFile();
    }
    return f;
  }
}
