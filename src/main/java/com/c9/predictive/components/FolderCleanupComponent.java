package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by yngbldjr on 6/10/14.
 */
public class FolderCleanupComponent extends DefaultBaseComponentVerticle {

  Logger log = LoggerFactory.getLogger(FolderCleanupComponent.class);

  private Long daysToKeep;

  @Override
  public void start(Future<Void> startResult) {

    daysToKeep = container.config().getLong("daysToKeep", 2L);

    super.start(startResult);
  }

  @Override
  public void processInput(final JsonObject input) {

    try {
      String folder = null;
      JsonArray folders = null;

      if(input.getString("folderToClean") != null){
        folder = input.getString("folderToClean");
      }

      if(input.getArray("foldersToClean") != null){
        folders = input.getArray("foldersToClean");
      }

      if(folder == null && folders == null){
        output.port(OUTPUTS().OUT).send(input.putString("status", "ok"));
      }

      Long keepDays = input.getLong("daysToKeep", daysToKeep);

      if(folders != null){
        for(Object fld : folders){
          cleanFolder((String)fld, keepDays);
        }
      } else {
        cleanFolder(folder, keepDays);
      }

      output.port(OUTPUTS().OUT).send(input.putString("status", "ok"));
    } catch(Exception e){
      output.port(OUTPUTS().OUT).send(addInfo(input, "Failed to delete file or directory.", e));
      log.error("Error Deleting old Files.", e);
    }
  }

  private void cleanFolder(final String folder, final Long keepDays) throws IOException {
    if(!Files.exists(Paths.get(folder))){
      log.info("Folder does not yet exist (" + folder + ").  Nothing to cleanup.");
      return;
    }

    final Path origFolder = Paths.get(folder);
    // Keep only a week worth of data
    final long purgeTime = System.currentTimeMillis() - (keepDays * 24 * 60 * 60 * 1000);

    Files.walkFileTree(Paths.get(folder), new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(file.toFile().lastModified() < purgeTime)
          Files.delete(file);

        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (dir == null)
          return FileVisitResult.TERMINATE;

        if(!Files.isSameFile(origFolder, dir) && dir.toFile().lastModified() < purgeTime)
          Files.delete(dir);

        return FileVisitResult.CONTINUE;
      }
    });

  }
}