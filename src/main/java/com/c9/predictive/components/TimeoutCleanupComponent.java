package com.c9.predictive.components;

import com.c9.generic.components.DefaultInputs;
import com.c9.generic.components.TimeoutOutputs;
import com.c9.generic.components.data.HasKeyInClusterMap;
import com.c9.predictive.helpers.RxEBUtil;
import com.c9.predictive.util.Utils;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Action1;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 9/3/14.
 */
public class TimeoutCleanupComponent extends HasKeyInClusterMap {

  Map<String, Long> cleanupTimers = new HashMap<>();
  Map<String, Long> timeoutTimers = new HashMap<>();

  public static class TimeoutCleanupInputs extends DefaultInputs {
    public static final String CLEAR_TIMER = "clear_timer";
  }

  public static TimeoutCleanupInputs INPUTS() {return new TimeoutCleanupInputs(); }


  public static TimeoutOutputs OUTPUTS() {return new TimeoutOutputs(); }

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    input.port(INPUTS().CLEAR_TIMER).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject input) {

        final String keyId = input.getString(key);

        clearTimers(keyId);
      }
    });
  }

  private void clearTimers(String keyId) {
    if(cleanupTimers.containsKey(keyId)){
      logger.info("Removing cleanup timer watcher for " + keyId);
      vertx.cancelTimer(cleanupTimers.get(keyId));
    }
    if(timeoutTimers.containsKey(keyId)){
      logger.info("Removing timeout timer watcher for " + keyId);
      vertx.cancelTimer(timeoutTimers.get(keyId));
    }
  }

  @Override
  public void processInput(final JsonObject input) {
    if(input.containsField("timeoutTimestamp")){

      final String keyId = input.getString(key);

      DateTime now = DateTime.now();
      DateTime timeout = new DateTime(input.getLong("timeoutTimestamp"));
      // Check 10 seconds after the timeout to make sure its cleaned up
      DateTime timeoutCleanup = new DateTime(input.getLong("timeoutTimestamp")+5000);

      if(now.isAfter(timeoutCleanup)){
        return;
      }

      Interval cleanupInterval = new Interval(now, timeoutCleanup);
      Interval timeoutInterval = new Interval(now, timeout);
      // Set the timeout timer

      long timeoutTimerId = vertx.setTimer(timeoutInterval.toDurationMillis(), new Handler<Long>() {
        @Override
        public void handle(Long event) {
          output.port(OUTPUTS().TIMEOUT).send(input);
          clearTimers(keyId);
        }
      });

      timeoutTimers.put(keyId, timeoutTimerId);

      Long timerId = vertx.setTimer(cleanupInterval.toDurationMillis(), new Handler<Long>() {
        @Override
        public void handle(Long timerEvent) {

          JsonObject lookupKey = new JsonObject().putString("type", "map").putString("name", mapName).putString("action", "get").putString("key", keyId);

          Observable<RxMessage<JsonObject>> keyLookupObs = rxeb.send(sharedDataCluster, lookupKey);

          RxEBUtil.raise(keyLookupObs).subscribe(new Action1<JsonObject>() {
            @Override
            public void call(JsonObject jsonObject) {
              if ("ok".equalsIgnoreCase(jsonObject.getString("status"))) {
                String value = jsonObject.getString("result");
                if (value != null) {
                  JsonObject infoInMap = new JsonObject(value);
                  addErrors(input, "The normal process failed to remove the in progress data from the map.  This would cause future calls to fail.  This data has been removed as it was left in a state that would require manual intervention.");
                  String errorTitleAndSubject = String.format("Data not cleared correctly for %s [%s]", infoInMap.getString(Utils.PropertyConstants.CUSTOMER), input.getString(Utils.PropertyConstants.PARTITION_ID));
                  output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(input, errorTitleAndSubject, "Error", true));
                  output.port(OUTPUTS().OUT).send(input);
                  deleteInfo(input, keyId);
                } else {
                  // Nothing to do here, data is not present in map which is correct, though the timer should have been removed.
                  addErrors(input, "Failed to access map in cluster.");
                  output.port(OUTPUTS().ERROR_OUT).send(input);
                }
              } else {

                addErrors(input, "Failed to access map in cluster.");
                output.port(OUTPUTS().ERROR_OUT).send(input);

              }
            }
          });
        }
      });

      cleanupTimers.put(keyId, timerId);
    }
  }

  private void deleteInfo(final JsonObject input, final String keyId) {

    JsonObject lookupKey = new JsonObject().putString("type", "map").putString("name", mapName).putString("action", "remove").putString("key", keyId);

    Observable<RxMessage<JsonObject>> keyLookupObs = rxeb.send(sharedDataCluster, lookupKey);

    RxEBUtil.raise(keyLookupObs).subscribe(new Action1<JsonObject>() {
      @Override
      public void call(JsonObject jsonObject) {
        if ("ok".equalsIgnoreCase(jsonObject.getString("status"))) {
          String value = jsonObject.getString("result");
          if (value != null) {
            logger.info(keyId + " exists in map.");
            output.port(OUTPUTS().ERROR_OUT).send(input.putString("mapValue", value));
          } else {
            logger.info("map key not in from cluster.");
            output.port(OUTPUTS().OUT).send(input);
          }
        } else {
          addErrors(input, "Failed to access map in cluster.");
          output.port(OUTPUTS().ERROR_OUT).send(input);
        }
      }
    });
  }
}
