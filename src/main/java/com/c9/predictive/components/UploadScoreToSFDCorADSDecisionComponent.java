package com.c9.predictive.components;

import com.c9.generic.components.common.BasicDecisionRouter;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 7/14/14.
 */
public class UploadScoreToSFDCorADSDecisionComponent extends BasicDecisionRouter {
  @Override
  public void processInput(JsonObject input) {
    if (input.getObject(objectFieldName, new JsonObject()).getBoolean(booleanFieldName, false) || input.getBoolean(booleanFieldName, false)) {
      try {
        String sfdcScoreFileLocation = input.getString("scoreFileLocation") + "_sfdc";
        if(!vertx.fileSystem().existsSync(sfdcScoreFileLocation))
          vertx.fileSystem().copySync(input.getString("scoreFileLocation"), sfdcScoreFileLocation, true);
        input.putString("scoreFileLocationSFDC", sfdcScoreFileLocation);
        output.port(BasicDecisionRouter.OUTPUTS().YES).send(input);
      } catch (Exception e) {
        addErrors(input, e.getMessage());
        output.port(BasicDecisionRouter.OUTPUTS().ERROR_OUT).send(input);
      }
    } else {
      output.port(BasicDecisionRouter.OUTPUTS().NO).send(input);
    }
  }
}
