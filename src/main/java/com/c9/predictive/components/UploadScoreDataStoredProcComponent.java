package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.components.config.CustomConfig;
import com.c9.predictive.components.config.OppScoreVersion;
import com.c9.predictive.helpers.RxEBUtil;
import com.c9.predictive.util.Utils;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.json.impl.Json;
import rx.Observable;
import rx.util.functions.Action1;
import rx.util.functions.Func1;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 6/27/14.
 */
public class UploadScoreDataStoredProcComponent extends DefaultBaseComponentVerticle {

  private String scoredProcTemplate;
  private String oppScoreFileDefinition;
  private RxEventBus rxeb;
  private String jdbcAddress;


  Logger log = LoggerFactory.getLogger(UploadScoreDataStoredProcComponent.class);

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);


    scoredProcTemplate = container.config().getString("scoreStoredProcUpdateTemplate");
    oppScoreFileDefinition = container.config().getString("tableDefinitionLocation") + Utils.PropertyConstants.OPP_SCORES_ADS_TABLE_DEFINITION;

    if(scoredProcTemplate == null){
      Exception e = new IllegalArgumentException("scoreStoredProcUpdateTemplate was not found in config : " + container.config());
      log.error("Failed to start component.", e);
      startResult.setFailure(new IllegalArgumentException("scoreStoredProcUpdateTemplate was not found in config : " + container.config()));
    } else {

      jdbcAddress = container.config().getString("address");

      rxeb = new RxEventBus(vertx.eventBus());

      startResult.setResult(null);
    }
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();
    if(input.getString("scoreFileLocation") == null){
      errors.put("scoreFileLocation", "scoreFileLocation is required.");
    }
    if(input.getString(Utils.PropertyConstants.PARTITION_ID) == null){
      errors.put(Utils.PropertyConstants.PARTITION_ID, "partitionId is required.");
    }
    return errors;
  }

  @Override
  public void processInput(final JsonObject input) {
    // Override for retrospective cen-3319
    Map<String, Object> additionAlParametersMap = input.getObject("uploadADSStoredProdAdditionalParams", new JsonObject()).toMap();

    StringBuffer additionAlParameters = new StringBuffer();

    if(additionAlParametersMap.size() > 0){
      for(Map.Entry<String, Object> param : additionAlParametersMap.entrySet()){
        additionAlParameters.append(", ").append(param.getKey()).append("='").append(param.getValue()).append("'");
      }
    }

    // Need a better way to manage the templates - the format is extracted to the config, but really can only support 1 format...so seems silly to hide it from the code?
    // "scoreStoredProcUpdateTemplate":"execute IntegrateOpportunityScore(FilePath='%s', FileDefinitionPath='%s' '%s');",
    String sql = String.format(scoredProcTemplate, input.getString("scoreFileLocation"), oppScoreFileDefinition, additionAlParameters);

    String partitionId = input.getString(Utils.PropertyConstants.PARTITION_ID);

    JsonObject csvQueryy = new JsonObject()
        .putString(Utils.PropertyConstants.PARTITION_ID, partitionId)
        .putString("stmt", sql)
        .putString("format", "json")
        .putString("queryName", sql + " for " + partitionId)
        .putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, input.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME));

    Observable<RxMessage<JsonObject>> uploadScore = rxeb.send(jdbcAddress, csvQueryy);

    RxEBUtil.raise(uploadScore)
        .onErrorReturn(new Func1<Throwable, JsonObject>() {
                         @Override
                         public JsonObject call(Throwable throwable) {
                           addErrors(input, throwable.getMessage(), throwable);
                           output.port(OUTPUTS().ERROR_OUT).send(input);
                           output.port(OUTPUTS().SEND_EMAIL).send(createErrorEmail(input));
                           return null;
                         }
                       }
        )
        .subscribe(new Action1<JsonObject>() {
          @Override
          public void call(JsonObject jsonObject) {
            if ("ok".equalsIgnoreCase(jsonObject.getString("status"))) {
              output.port(OUTPUTS().OUT).send(input);
              output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(input, "Upload score data to ADS for " + input.getString(Utils.PropertyConstants.CUSTOMER) + " [" + input.getString(Utils.PropertyConstants.PARTITION_ID) + "] was successful.", "CompletionEmail", false));
            } else {
              addErrors(input, "Failed to upload new score information. " + jsonObject.getString("message", ""));
              output.port(OUTPUTS().ERROR_OUT).send(input);
              output.port(OUTPUTS().SEND_EMAIL).send(createErrorEmail(input, "Failed to upload score data to ADS for " + input.getString(Utils.PropertyConstants.CUSTOMER) + " [" + input.getString(Utils.PropertyConstants.PARTITION_ID) + "]"));
            }
          }
        });
  }
}
