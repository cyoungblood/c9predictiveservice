package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.common.YesNoOutputs;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 1/27/15.
 */
public class UpdateConfigDecisionRouter extends DefaultBaseComponentVerticle {

  public static YesNoOutputs OUTPUTS(){ return new YesNoOutputs(); }

  @Override
  public void processInput(JsonObject input) {
    if(input.getArray("errors", new JsonArray()).size() > 0){
      output.port(OUTPUTS().NO).send(input);
    } else {
      output.port(OUTPUTS().YES).send(input);
    }
  }
}
