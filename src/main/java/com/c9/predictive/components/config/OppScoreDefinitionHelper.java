package com.c9.predictive.components.config;

import com.c9.predictive.util.Utils;
import com.google.common.base.Joiner;
import org.apache.commons.csv.CSVRecord;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import java.util.*;

/**
 * Created by yngbldjr on 1/25/15.
 */
public abstract class OppScoreDefinitionHelper implements OppScoreDefinitionHelperInterface {

  public static final String EXTERNAL_ID = "c9oppscoring__External_ID__c";
  //score csv file metadata.
  protected static final String OPPORTUNITY_ID = "c9oppscoring__Opportunity_Id__c";

  protected static final String CAT_0_WINLOSS = "c9oppscoring__cat_0_winloss__c"; //Boolean
  protected static final String CAT_0_SCORE = "c9oppscoring__cat_0_score__c"; //Float

  protected static final String CAT_0_FEATURE1_NAME = "c9oppscoring__cat_0_fea_1_name__c";
  protected static final String CAT_0_FEATURE1_VALUE = "c9oppscoring__cat_0_fea_1_value__c";
  protected static final String CAT_0_FEATURE1_IMPORTANCE = "c9oppscoring__cat_0_fea_1_importance__c"; //Float
  protected static final String CAT_0_FEATURE1_COMMENT = "c9oppscoring__cat_0_fea_1_comment__c"; //String - 128 chars

  protected static final String CAT_0_FEATURE2_NAME = "c9oppscoring__cat_0_fea_2_name__c"; //String - 128 chars
  protected static final String CAT_0_FEATURE2_VALUE = "c9oppscoring__cat_0_fea_2_value__c";
  protected static final String CAT_0_FEATURE2_IMPORTANCE = "c9oppscoring__cat_0_fea_2_importance__c"; //Float
  protected static final String CAT_0_FEATURE2_COMMENT = "c9oppscoring__cat_0_fea_2_comment__c";

  protected static final String CAT_0_FEATURE3_NAME = "c9oppscoring__cat_0_fea_3_name__c";
  protected static final String CAT_0_FEATURE3_VALUE = "c9oppscoring__cat_0_fea_3_value__c";
  protected static final String CAT_0_FEATURE3_IMPORTANCE = "c9oppscoring__cat_0_fea_3_importance__c"; //Float
  protected static final String CAT_0_FEATURE3_COMMENT = "c9oppscoring__cat_0_fea_3_comment__c";

  protected static final String CAT_0_NAME = "c9oppscoring__cat_0_name__c";
  protected static final String CAT_0_WEIGHT = "c9oppscoring__cat_0_weight__c"; //Float

  protected static final String CAT_1_WINLOSS = "c9oppscoring__cat_1_winloss__c";
  protected static final String CAT_1_SCORE = "c9oppscoring__cat_1_score__c";
  protected static final String CAT_1_NAME = "c9oppscoring__cat_1_name__c";
  protected static final String CAT_1_WEIGHT = "c9oppscoring__cat_1_weight__c";

  protected static final String CAT_1_FEATURE1_NAME = "c9oppscoring__cat_1_fea_1_name__c";
  protected static final String CAT_1_FEATURE1_VALUE = "c9oppscoring__cat_1_fea_1_value__c";
  protected static final String CAT_1_FEATURE1_IMPORTANCE = "c9oppscoring__cat_1_fea_1_importance__c";
  protected static final String CAT_1_FEATURE1_COMMENT = "c9oppscoring__cat_1_fea_1_comment__c";

  protected static final String CAT_1_FEATURE2_NAME = "c9oppscoring__cat_1_fea_2_name__c";
  protected static final String CAT_1_FEATURE2_VALUE = "c9oppscoring__cat_1_fea_2_value__c";
  protected static final String CAT_1_FEATURE2_IMPORTANCE = "c9oppscoring__cat_1_fea_2_importance__c";
  protected static final String CAT_1_FEATURE2_COMMENT = "c9oppscoring__cat_1_fea_2_comment__c";

  protected static final String CAT_1_FEATURE3_NAME = "c9oppscoring__cat_1_fea_3_name__c";
  protected static final String CAT_1_FEATURE3_VALUE = "c9oppscoring__cat_1_fea_3_value__c";
  protected static final String CAT_1_FEATURE3_IMPORTANCE = "c9oppscoring__cat_1_fea_3_importance__c";
  protected static final String CAT_1_FEATURE3_COMMENT = "c9oppscoring__cat_1_fea_3_comment__c";

  protected static final String CAT_2_WINLOSS = "c9oppscoring__cat_2_winloss__c";
  protected static final String CAT_2_SCORE = "c9oppscoring__cat_2_score__c";
  protected static final String CAT_2_NAME = "c9oppscoring__cat_2_name__c";
  protected static final String CAT_2_WEIGHT = "c9oppscoring__cat_2_weight__c";

  protected static final String CAT_2_FEATURE1_NAME = "c9oppscoring__cat_2_fea_1_name__c";
  protected static final String CAT_2_FEATURE1_VALUE = "c9oppscoring__cat_2_fea_1_value__c";
  protected static final String CAT_2_FEATURE1_IMPORTANCE = "c9oppscoring__cat_2_fea_1_importance__c";
  protected static final String CAT_2_FEATURE1_COMMENT = "c9oppscoring__cat_2_fea_1_comment__c";

  protected static final String CAT_2_FEATURE2_NAME = "c9oppscoring__cat_2_fea_2_name__c";
  protected static final String CAT_2_FEATURE2_VALUE = "c9oppscoring__cat_2_fea_2_value__c";
  protected static final String CAT_2_FEATURE2_IMPORTANCE = "c9oppscoring__cat_2_fea_2_importance__c";
  protected static final String CAT_2_FEATURE2_COMMENT = "c9oppscoring__cat_2_fea_2_comment__c";

  protected static final String CAT_2_FEATURE3_NAME = "c9oppscoring__cat_2_fea_3_name__c";
  protected static final String CAT_2_FEATURE3_VALUE = "c9oppscoring__cat_2_fea_3_value__c";
  protected static final String CAT_2_FEATURE3_IMPORTANCE = "c9oppscoring__cat_2_fea_3_importance__c";
  protected static final String CAT_2_FEATURE3_COMMENT = "c9oppscoring__cat_2_fea_3_comment__c";

  protected static final String CAT_3_WINLOSS = "c9oppscoring__cat_3_winloss__c";
  protected static final String CAT_3_SCORE = "c9oppscoring__cat_3_score__c";
  protected static final String CAT_3_NAME = "c9oppscoring__cat_3_name__c";
  protected static final String CAT_3_WEIGHT = "c9oppscoring__cat_3_weight__c";

  protected static final String CAT_3_FEATURE1_NAME = "c9oppscoring__cat_3_fea_1_name__c";
  protected static final String CAT_3_FEATURE1_VALUE = "c9oppscoring__cat_3_fea_1_value__c";
  protected static final String CAT_3_FEATURE1_IMPORTANCE = "c9oppscoring__cat_3_fea_1_importance__c";
  protected static final String CAT_3_FEATURE1_COMMENT = "c9oppscoring__cat_3_fea_1_comment__c";

  protected static final String CAT_3_FEATURE2_NAME = "c9oppscoring__cat_3_fea_2_name__c";
  protected static final String CAT_3_FEATURE2_VALUE = "c9oppscoring__cat_3_fea_2_value__c";
  protected static final String CAT_3_FEATURE2_IMPORTANCE = "c9oppscoring__cat_3_fea_2_importance__c";
  protected static final String CAT_3_FEATURE2_COMMENT = "c9oppscoring__cat_3_fea_2_comment__c";

  protected static final String CAT_3_FEATURE3_NAME = "c9oppscoring__cat_3_fea_3_name__c";
  protected static final String CAT_3_FEATURE3_VALUE = "c9oppscoring__cat_3_fea_3_value__c";
  protected static final String CAT_3_FEATURE3_IMPORTANCE = "c9oppscoring__cat_3_fea_3_importance__c";
  protected static final String CAT_3_FEATURE3_COMMENT = "c9oppscoring__cat_3_fea_3_comment__c";


protected static final String CAT_0_FEA_4_NAME = "c9oppscoreing__cat_0_fea_4_name_c";
protected static final String CAT_0_FEA_4_VALUE = "c9oppscoreing__cat_0_fea_4_value_c";
protected static final String CAT_0_FEA_4_IMPORTANCE = "c9oppscoreing__cat_0_fea_4_importance_c";
protected static final String CAT_0_FEA_4_COMMENT = "c9oppscoreing__cat_0_fea_4_comment_c";
protected static final String CAT_0_FEA_5_NAME = "c9oppscoreing__cat_0_fea_5_name_c";
protected static final String CAT_0_FEA_5_VALUE = "c9oppscoreing__cat_0_fea_5_value_c";
protected static final String CAT_0_FEA_5_IMPORTANCE = "c9oppscoreing__cat_0_fea_5_importance_c";
protected static final String CAT_0_FEA_5_COMMENT = "c9oppscoreing__cat_0_fea_5_comment_c";
protected static final String CAT_0_FEA_6_NAME = "c9oppscoreing__cat_0_fea_6_name_c";
protected static final String CAT_0_FEA_6_VALUE = "c9oppscoreing__cat_0_fea_6_value_c";
protected static final String CAT_0_FEA_6_IMPORTANCE = "c9oppscoreing__cat_0_fea_6_importance_c";
protected static final String CAT_0_FEA_6_COMMENT = "c9oppscoreing__cat_0_fea_6_comment_c";
protected static final String CAT_0_FEA_7_NAME = "c9oppscoreing__cat_0_fea_7_name_c";
protected static final String CAT_0_FEA_7_VALUE = "c9oppscoreing__cat_0_fea_7_value_c";
protected static final String CAT_0_FEA_7_IMPORTANCE = "c9oppscoreing__cat_0_fea_7_importance_c";
protected static final String CAT_0_FEA_7_COMMENT = "c9oppscoreing__cat_0_fea_7_comment_c";
protected static final String CAT_0_FEA_8_NAME = "c9oppscoreing__cat_0_fea_8_name_c";
protected static final String CAT_0_FEA_8_VALUE = "c9oppscoreing__cat_0_fea_8_value_c";
protected static final String CAT_0_FEA_8_IMPORTANCE = "c9oppscoreing__cat_0_fea_8_importance_c";
protected static final String CAT_0_FEA_8_COMMENT = "c9oppscoreing__cat_0_fea_8_comment_c";
protected static final String CAT_0_FEA_9_NAME = "c9oppscoreing__cat_0_fea_9_name_c";
protected static final String CAT_0_FEA_9_VALUE = "c9oppscoreing__cat_0_fea_9_value_c";
protected static final String CAT_0_FEA_9_IMPORTANCE = "c9oppscoreing__cat_0_fea_9_importance_c";
protected static final String CAT_0_FEA_9_COMMENT = "c9oppscoreing__cat_0_fea_9_comment_c";
protected static final String CAT_0_FEA_10_NAME = "c9oppscoreing__cat_0_fea_10_name_c";
protected static final String CAT_0_FEA_10_VALUE = "c9oppscoreing__cat_0_fea_10_value_c";
protected static final String CAT_0_FEA_10_IMPORTANCE = "c9oppscoreing__cat_0_fea_10_importance_c";
protected static final String CAT_0_FEA_10_COMMENT = "c9oppscoreing__cat_0_fea_10_comment_c";



    protected static final String VERSION_DATE = "version_date";

  protected static final int FEATURE_LENGTH = 64; //length for feature name/value/comment/label

  protected static final String FEATURE_NAME = "feature_name";
  protected static final String FEATUER_DESC = "feature_desc";

  public interface RowConverter {
    String convert(String originalValue);
  }

  public class NoConverter implements RowConverter {

    @Override
    public String convert(String originalValue) {
      return originalValue;
    }
  }

  public class ScoreConverter implements RowConverter {

    Map<String, String> featureMap;
    public ScoreConverter(Map<String, String> featureMap){
      this.featureMap = featureMap;
    }

    @Override
    public String convert(String originalValue) {
      return OppScoreDefinitionHelperUtils.formatScores(originalValue);
    }
  }

  public class TruncateConverter implements RowConverter {

    Map<String, String> featureMap;
    public TruncateConverter(Map<String, String> featureMap){
      this.featureMap = featureMap;
    }

    @Override
    public String convert(String originalValue) {
      return OppScoreDefinitionHelperUtils.truncate(OppScoreDefinitionHelperUtils.lookupFeatureDesc(originalValue, getFeatureMap()), FEATURE_LENGTH);
    }
  }

  public class LabelTruncateConverter implements RowConverter {

    Map<String, String> featureMap;
    public LabelTruncateConverter(Map<String, String> featureMap) {
      this.featureMap = featureMap;
    }

    @Override
    public String convert(String originalValue) {
      return OppScoreDefinitionHelperUtils.truncate(OppScoreDefinitionHelperUtils.lookupBucketDesc(originalValue, getFeatureMap()), 15);
    }
  }

  public class DS_TO_SFDCMapping {
    public String sfdcColumn;
    public String scoreColumn;
    public RowConverter converter;
    public DS_TO_SFDCMapping(String sfdcColumn, String scoreColumn, RowConverter converter){
      this.sfdcColumn = sfdcColumn;
      this.scoreColumn = scoreColumn;
      this.converter = converter;
    }
  }

  @Override
  public byte[] getHeaderBytes() {
    List<String> header = new ArrayList<>();
    for(DS_TO_SFDCMapping key : getC9SFDCMapping()){
      header.add(key.sfdcColumn);
    }
    try {
      return Joiner.on(",").join(header).concat("\n").getBytes("UTF-8");

    } catch(Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  Set<DS_TO_SFDCMapping> DS_TO_SFDCMappings;

  @Override
  public Set<DS_TO_SFDCMapping> getC9SFDCMapping() {

    if(DS_TO_SFDCMappings != null ){
      return DS_TO_SFDCMappings;
    }

    DS_TO_SFDCMappings = new LinkedHashSet<>();

    ScoreConverter sc = new ScoreConverter(getFeatureMap());
    TruncateConverter tc = new TruncateConverter(getFeatureMap());
    NoConverter nc = new NoConverter();

    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(EXTERNAL_ID, "Opportunity_Id", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(OPPORTUNITY_ID, "Opportunity_Id", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_WINLOSS, "cat_0_winloss", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_SCORE, "cat_0_score", sc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE1_NAME, "cat_0_fea_1_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE1_VALUE, "cat_0_fea_1_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE1_IMPORTANCE, "cat_0_fea_1_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE1_COMMENT, "cat_0_fea_1_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE2_NAME, "cat_0_fea_2_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE2_VALUE, "cat_0_fea_2_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE2_IMPORTANCE, "cat_0_fea_2_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE2_COMMENT, "cat_0_fea_2_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE3_NAME, "cat_0_fea_3_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE3_VALUE, "cat_0_fea_3_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE3_IMPORTANCE, "cat_0_fea_3_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEATURE3_COMMENT, "cat_0_fea_3_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_NAME, "cat_0_name", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_WEIGHT, "cat_0_weight", nc));

    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_WINLOSS, "cat_1_winloss", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_SCORE, "cat_1_score", sc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE1_NAME, "cat_1_fea_1_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE1_VALUE, "cat_1_fea_1_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE1_IMPORTANCE, "cat_1_fea_1_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE1_COMMENT, "cat_1_fea_1_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE2_NAME, "cat_1_fea_2_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE2_VALUE, "cat_1_fea_2_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE2_IMPORTANCE, "cat_1_fea_2_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE2_COMMENT, "cat_1_fea_2_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE3_NAME, "cat_1_fea_3_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE3_VALUE, "cat_1_fea_3_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE3_IMPORTANCE, "cat_1_fea_3_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_FEATURE3_COMMENT, "cat_1_fea_3_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_NAME, "cat_1_name", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_1_WEIGHT, "cat_1_weight", nc));

    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_WINLOSS, "cat_2_winloss", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_SCORE, "cat_2_score", sc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE1_NAME, "cat_2_fea_1_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE1_VALUE, "cat_2_fea_1_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE1_IMPORTANCE, "cat_2_fea_1_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE1_COMMENT, "cat_2_fea_1_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE2_NAME, "cat_2_fea_2_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE2_VALUE, "cat_2_fea_2_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE2_IMPORTANCE, "cat_2_fea_2_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE2_COMMENT, "cat_2_fea_2_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE3_NAME, "cat_2_fea_3_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE3_VALUE, "cat_2_fea_3_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE3_IMPORTANCE, "cat_2_fea_3_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_FEATURE3_COMMENT, "cat_2_fea_3_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_NAME, "cat_2_name", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_2_WEIGHT, "cat_2_weight", nc));

    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_WINLOSS, "cat_3_winloss", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_SCORE, "cat_3_score", sc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE1_NAME, "cat_3_fea_1_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE1_VALUE, "cat_3_fea_1_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE1_IMPORTANCE, "cat_3_fea_1_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE1_COMMENT, "cat_3_fea_1_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE2_NAME, "cat_3_fea_2_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE2_VALUE, "cat_3_fea_2_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE2_IMPORTANCE, "cat_3_fea_2_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE2_COMMENT, "cat_3_fea_2_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE3_NAME, "cat_3_fea_3_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE3_VALUE, "cat_3_fea_3_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE3_IMPORTANCE, "cat_3_fea_3_importance", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_FEATURE3_COMMENT, "cat_3_fea_3_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_NAME, "cat_3_name", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_WEIGHT, "cat_3_weight", nc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_3_WEIGHT, "cat_3_weight", nc));

    // New for 5.6 - CEN-3600 & CEN-3850
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_4_NAME, "cat_0_fea_4_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_4_VALUE, "cat_0_fea_4_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_4_IMPORTANCE, "cat_0_fea_4_importance", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_4_COMMENT, "cat_0_fea_4_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_5_NAME, "cat_0_fea_5_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_5_VALUE, "cat_0_fea_5_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_5_IMPORTANCE, "cat_0_fea_5_importance", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_5_COMMENT, "cat_0_fea_5_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_6_NAME, "cat_0_fea_6_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_6_VALUE, "cat_0_fea_6_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_6_IMPORTANCE, "cat_0_fea_6_importance", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_6_COMMENT, "cat_0_fea_6_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_7_NAME, "cat_0_fea_7_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_7_VALUE, "cat_0_fea_7_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_7_IMPORTANCE, "cat_0_fea_7_importance", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_7_COMMENT, "cat_0_fea_7_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_8_NAME, "cat_0_fea_8_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_8_VALUE, "cat_0_fea_8_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_8_IMPORTANCE, "cat_0_fea_8_importance", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_8_COMMENT, "cat_0_fea_8_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_9_NAME, "cat_0_fea_9_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_9_VALUE, "cat_0_fea_9_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_9_IMPORTANCE, "cat_0_fea_9_importance", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_9_COMMENT, "cat_0_fea_9_comment", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_10_NAME, "cat_0_fea_10_name", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_10_VALUE, "cat_0_fea_10_value", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_10_IMPORTANCE, "cat_0_fea_10_importance", tc));
    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(CAT_0_FEA_10_COMMENT, "cat_0_fea_10_comment", tc));

    DS_TO_SFDCMappings.add(new DS_TO_SFDCMapping(VERSION_DATE, "version_date", nc));




    return DS_TO_SFDCMappings;
  }

  @Override
  public byte[] convertScoreRowToSFDCBytes(CSVRecord record) throws Exception {
    List<String> values = new ArrayList<>();
    for(DS_TO_SFDCMapping DS_TO_SFDCMapping : getC9SFDCMapping()){
      if(record.isMapped(DS_TO_SFDCMapping.scoreColumn)) {
        values.add(DS_TO_SFDCMapping.converter.convert(record.get(DS_TO_SFDCMapping.scoreColumn)));
      }
    }
    return Joiner.on(",").join(values).concat("\n").getBytes("UTF-8");
  }

  private Map<String, String> featuresMap;

  public void initFeaturesMap(JsonArray featureResults) {
    featuresMap = new HashMap<>();
    if(featureResults == null) {
      return;
    }
    for(int i = 0; i < featureResults.size(); i++) {
      JsonObject objectMap = featureResults.get(i);
      featuresMap.put(objectMap.getString(FEATURE_NAME), objectMap.getString(FEATUER_DESC));
    }
  }

  @Override
  public Map<String, String> getFeatureMap(){
    return this.featuresMap;
  }

  public String getOpportunityIdFromRow(CSVRecord record){
    return (record.get("Opportunity_Id"));
  }

  public String getOppScoreFileDefinition(String tableDefinitionLocation) {
    return tableDefinitionLocation + Utils.PropertyConstants.OPP_SCORES_ADS_TABLE_DEFINITION;
  }
  public String getDurationScoreFileDefinition(String durationScoreFileDefinition) {
    return durationScoreFileDefinition  + Utils.PropertyConstants.DURATION_ADS_TABLE_DEFINITION;
  }
}
