package com.c9.predictive.components.config;

import com.c9.predictive.util.Utils;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 1/21/15.
 */
public class CustomConfig {

  private JsonObject config;
  OppScoreVersion oppScoreVersion;

  public CustomConfig(JsonObject config){

    this.config = config == null ? new JsonObject() : config;
    this.oppScoreVersion = OppScoreVersion.lookupVersion(trim(this.config.getString("oppScoreVersion", OppScoreVersion.V1_4.name())));
  }

  public static CustomConfig getCustomConfig(JsonObject config){
    return new CustomConfig(config.getObject(Utils.PropertyConstants.CUSTOM_CONFIG_OUT_KEY));
  }

  public boolean suppressSuccessEmails(){
    return "true".equalsIgnoreCase(trim(config.getString("suppressSuccessfulEmail", "false")));
  }

  public OppScoreVersion getOppScoreVersion(){
    return oppScoreVersion;
  }

  public Integer DEFAULT_SFDC_SELECT_ID_BATCH_SIZE = 500;

  public Integer getSFDCBatchUploadSize(){

    try {
      if(config.getString("sfdcSelectIdBatchSize", null) != null)
        return Integer.parseInt(config.getString("sfdcSelectIdBatchSize"));
    }catch(Exception e){
      // Ignore as the property may not be there or not a valid integer
    }
    return DEFAULT_SFDC_SELECT_ID_BATCH_SIZE;
  }

  public String getProperty(String configProperty) {
    return getProperty(configProperty, null);
  }
  public String getProperty(String configProperty, String defaultValue) {
    return trim(config.getString(configProperty, defaultValue));
  }
  private String trim(String string) {
    if(string == null){
      return null;
    }
    // Remove all whitespace
    return string.replaceAll("\\s+", "");
  }
}
