package com.c9.predictive.components.config;

import java.text.NumberFormat;
import java.util.Map;

/**
 * Created by yngbldjr on 1/21/15.
 */
public class OppScoreDefinitionHelperUtils {

  protected static final String BUCKET_LABEL_FEATURE_FORMAT = "bucket_%s_label";

  public static String lookupBucketDesc(String bucketValue, Map<String, String> featureMap) {
    String bucketLabel = String.format(BUCKET_LABEL_FEATURE_FORMAT, bucketValue);
    if(featureMap == null){
      return bucketLabel;
    }

    String label = featureMap.get(bucketLabel);

    return label == null ? bucketLabel : label;

  }

  public static String lookupFeatureDesc(String featureName, Map<String, String> featureMap) {
    if(featureMap == null) {
      return featureName;
    }

    String featureDesc = featureMap.get(featureName);
    return featureDesc == null ? featureName : featureDesc;
  }

  private static NumberFormat nf = NumberFormat.getInstance();

  public static String formatScores(String score) {
    try {
      float scoreFl = Float.parseFloat(score);
      nf.setMaximumFractionDigits(2);
      return nf.format(scoreFl);
    } catch(Exception ex) {
      return score;
    }
  }

  public static  String truncate(String str, int length) {
    if(str != null && str.length() > length) {
      return str.substring(0, length - 1);
    }

    return str;
  }


}
