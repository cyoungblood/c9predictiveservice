package com.c9.predictive.components.config;

import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by yngbldjr on 1/21/15.
 */
public class OppScoreV1_6DefinitionHelper extends OppScoreDefinitionHelper {

  protected static final String CAT_0_BIN_LABEL = "c9oppscoring__cat_0_bin_label__c";

  protected static final String CAT_1_BIN_LABEL = "c9oppscoring__cat_1_bin_label__c";

  protected static final String CAT_2_BIN_LABEL = "c9oppscoring__cat_2_bin_label__c";

  protected static final String CAT_3_BIN_LABEL = "c9oppscoring__cat_3_bin_label__c";


  private Map<String, String> featuresMap;

  public void initFeaturesMap(JsonArray featureResults) {
    featuresMap = new HashMap<>();
    if(featureResults == null) {
      return;
    }
    for(int i = 0; i < featureResults.size(); i++) {
      JsonObject objectMap = featureResults.get(i);
      featuresMap.put(objectMap.getString(FEATURE_NAME), objectMap.getString(FEATUER_DESC));
    }
  }

  @Override
  public Map<String, String> getFeatureMap(){
    return this.featuresMap;
  }

  Set<DS_TO_SFDCMapping> DS_TO_SFDCMapping;

  @Override
  public Set<DS_TO_SFDCMapping> getC9SFDCMapping() {

    if(DS_TO_SFDCMapping != null){
      return DS_TO_SFDCMapping;
    }

    DS_TO_SFDCMapping = super.getC9SFDCMapping();

    LabelTruncateConverter lc = new LabelTruncateConverter(getFeatureMap());

    DS_TO_SFDCMapping.add(new DS_TO_SFDCMapping(CAT_0_BIN_LABEL, "cat_0_bin", lc));

    DS_TO_SFDCMapping.add(new DS_TO_SFDCMapping(CAT_1_BIN_LABEL, "cat_1_bin", lc));

    DS_TO_SFDCMapping.add(new DS_TO_SFDCMapping(CAT_2_BIN_LABEL, "cat_2_bin", lc));

    DS_TO_SFDCMapping.add(new DS_TO_SFDCMapping(CAT_3_BIN_LABEL, "cat_3_bin", lc));

    return DS_TO_SFDCMapping;
  }
}
