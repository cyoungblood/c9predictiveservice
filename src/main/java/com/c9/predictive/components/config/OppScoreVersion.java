package com.c9.predictive.components.config;

import java.util.Arrays;
import java.util.List;

/**
 * Created by yngbldjr on 1/21/15.
 */
public enum OppScoreVersion {
  V1_4(Arrays.asList("1.4"), new OppScoreV1_4DefinitionHelper()),
  V1_5(Arrays.asList("1.5","1.6","1.7","1.8"), new OppScoreV1_6DefinitionHelper()),
  V1_9(Arrays.asList("1.9"), new OppScoreV1_9DefinitionHelper());

  public List<String> version;
  private OppScoreDefinitionHelperInterface helper;

  public OppScoreDefinitionHelperInterface getHelper(){
    return this.helper;
  }
  OppScoreVersion(List<String> version, OppScoreDefinitionHelperInterface helper){
    this.version = version;
    this.helper = helper;
  }

  public static OppScoreVersion lookupVersion(String ver){

    if(ver == null)
      return OppScoreVersion.V1_4;

    for(OppScoreVersion osv : OppScoreVersion.values()){
      if(osv.version.contains(ver)){
        return osv;
      }
    }

    return OppScoreVersion.V1_4;
  }
}