package com.c9.predictive.components.config;

import org.apache.commons.csv.CSVRecord;
import org.vertx.java.core.json.JsonArray;

import java.util.Map;
import java.util.Set;

/**
 * Created by yngbldjr on 1/21/15.
 */
public interface OppScoreDefinitionHelperInterface {

  void initFeaturesMap(JsonArray results);

  Map<String, String> getFeatureMap();

  byte[] getHeaderBytes();

  Set<OppScoreDefinitionHelper.DS_TO_SFDCMapping> getC9SFDCMapping();

  String getOpportunityIdFromRow(CSVRecord record);

  // DS Score output to byte[] for SFDC
  byte[] convertScoreRowToSFDCBytes(CSVRecord record) throws Exception;

  String getOppScoreFileDefinition(String baseLocation);

  String getDurationScoreFileDefinition(String baseLocation);
}