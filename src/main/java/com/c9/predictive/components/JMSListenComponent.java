package com.c9.predictive.components;

import com.c9.generic.components.BaseComponentVerticle;
import com.c9.predictive.util.Utils;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

import javax.jms.*;

public class JMSListenComponent extends BaseComponentVerticle {

	String activeMQHost;
  String queue;

  ActiveMQConnectionFactory connectionFactory;
  Connection connection;
  Session session;
  Destination destination;
  MessageConsumer consumer;
  static final Long defaultTimeout = 60 * 60 * 1000L; // 60 minutes in milliseconds;

  Logger log = LoggerFactory.getLogger(JMSListenComponent.class);

  public void start(final Future<Void> startedResult) {
		super.start(startedResult);

		// local activeMQ if not set in config
		activeMQHost = container.config().getString("activeMQHost", "tcp://localhost:61616");

    queue = container.config().getString("jms.listen.queue");

    connectionFactory = new ActiveMQConnectionFactory(activeMQHost);

    try
    {
      connection = connectionFactory.createConnection();

      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

      connection.start();

      destination = session.createQueue(queue);

      consumer = session.createConsumer(destination);

      // Wait for the network to be completely started before listening to the queue.
      vertx.eventBus().registerHandler(container.config().getString("startedAddress", "started.event"), new Handler<org.vertx.java.core.eventbus.Message<JsonObject>>() {
        @Override
        public void handle(Message<JsonObject> event) {
          setMessageConsumer(event.body());
        }
      });
    }
    catch(Exception e)
    {
      e.printStackTrace();
      log.error(String.format("Error creating Queue : %s", queue), e);
      startedResult.setFailure(e);
      return;
    }
    startedResult.setResult(null);
	}

  public void setMessageConsumer(JsonObject input){
    try {
      consumer.setMessageListener(new MessageListener() {
        @Override
        public void onMessage(javax.jms.Message message) {
          if (message instanceof TextMessage) {
            JsonObject jsonResponse = new JsonObject();
            TextMessage textMessage = (TextMessage) message;

            String text;
            try {
              text = textMessage.getText();
              jsonResponse.putString(Utils.PropertyConstants.PARTITION_ID, text);
              jsonResponse.putString(Utils.PropertyConstants.CORRELATION_ID, textMessage.getJMSCorrelationID());
              jsonResponse.putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, textMessage.getStringProperty(Utils.JMSPropertyConstants.HOSTNAME));
              jsonResponse.putString("servicename", textMessage.getStringProperty("servicename"));

              long predictivemaximumwaittime = textMessage.getLongProperty("predictivemaximumwaittime");
              if(predictivemaximumwaittime > 0){
                jsonResponse.putNumber("predictiveMaximumWaitTime", predictivemaximumwaittime);
                jsonResponse.putNumber("timeoutTimestamp", determineTimeoutTimestamp(predictivemaximumwaittime));
              }  else {
                jsonResponse.putNumber("predictiveMaximumWaitTime", defaultTimeout);
                jsonResponse.putNumber("timeoutTimestamp", determineTimeoutTimestamp(defaultTimeout));
              }

              jsonResponse.putBoolean("fromJMS", true);
              jsonResponse.putBoolean("runExtraction", true);
              jsonResponse.putString("server", Utils.getHostName());
              jsonResponse.putNumber("start", System.currentTimeMillis());

              if (log.isDebugEnabled())
                log.debug("Received message on queue : " + queue + " with value : " + text + " and passing along to output port.");
              output.port(OUTPUTS().OUT).send(jsonResponse);
            } catch (JMSException e) {
              log.error("Error consuming JMS Message", e);
              output.port(OUTPUTS().ERROR_OUT).send(addErrors(jsonResponse, "" + e.getMessage()));
            }
          } else {
            log.error("Received but not passed along since it was not a text message : " + message);
          }
        }
      });
    } catch (Exception e){
      e.printStackTrace();
      log.error(String.format("Failed to set message listener : %s", queue), e);
      output.port(OUTPUTS().ERROR_OUT).send(addErrors(input, "Failed to set Message Listener.", e));
    }
  }

  private Long determineTimeoutTimestamp(Long defaultTimeout) {
    DateTime dt = DateTime.now().plusMillis(defaultTimeout.intValue());
    return dt.getMillis();
  }

  @Override
  public void stop() {
    try {
      log.info("Stopping JMS Listener.");
      session.close();

      // Cleanup;
      session = null;
      destination = null;

      connection.stop();
      connection.close();
      connection = null;

      connectionFactory = null;

    } catch(Exception e){
      e.printStackTrace();
    }
  }
}