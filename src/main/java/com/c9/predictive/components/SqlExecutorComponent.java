package com.c9.predictive.components;

import com.c9.predictive.util.CSVWriter;
import com.c9.predictive.util.JsonUtils;
import com.c9.predictive.util.LimitedMapListHandler;
import com.c9.predictive.util.Utils;
import com.certive.jdbc.sql.JdbcStatement3;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;
import rx.Observable;
import rx.util.functions.Action1;

import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by yngbldjr on 10/31/14.
 */
public class SqlExecutorComponent extends Verticle {

  RxEventBus rxeb;
  String macURL;
  String address;
  Integer timeout;
  Integer fetchSize;

  Logger log = LoggerFactory.getLogger(SqlExecutorComponent.class);

  public static final String COM_CERTIVE_JDBC_CERTIVE_DRIVER = "com.certive.jdbc.CertiveDriver";

  static {
    try {
      Class.forName(COM_CERTIVE_JDBC_CERTIVE_DRIVER);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void start(Future<Void> startedResult) {
    super.start(startedResult);

    rxeb = new RxEventBus(vertx.eventBus());

    macURL = container.config().getString("macURL", "jdbc:certive://dev1-mac1:5432/NW;SSL=false;username='su';password='password'");
    address = container.config().getString("address", "predictive.jdbc");
    // Timeout in minutes.
    timeout = container.config().getInteger("fetchTimeout", 20);
    fetchSize = container.config().getInteger("fetchSize", 5000);

    log.info(String.format("Deploying with the following config %s", container.config()));

    Observable<RxMessage<JsonObject>> jdbc = rxeb.registerLocalHandler(address);
    jdbc.subscribe(new Action1<RxMessage<JsonObject>>(){
      @Override
      public void call(RxMessage<JsonObject> jsonObjectRxMessage) {
        performJDBC(jsonObjectRxMessage);
      }
    });

    startedResult.setResult(null);
  }


  private void performJDBC(RxMessage<JsonObject> event){

    log.debug("Received request : " + event.body());
    JsonObject request = event.body();
    JsonObject response = new JsonObject();
    response.putString("status", "ok");

    String partitionId = request.getString(Utils.PropertyConstants.PARTITION_ID, null);
    // If the partitionId is the mac then use the mac url otherwise use the hostname

    String jdbcAddress = partitionId.equalsIgnoreCase("mac") ? macURL : String.format("jdbc:certive://%s:5432/NW;SSL=false;username='su';password='password'", request.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME));

    String sql = request.getString("stmt", null);
    String format = request.getString("format", "csv");
    boolean includeHeader = request.getBoolean("includeHeader", true);

    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;

    try {

      log.debug("Getting Connection for " + partitionId);
      connection = DriverManager.getConnection(jdbcAddress, "su", "password");
      log.debug("Got Connection for " + partitionId);
      statement = connection.createStatement();

      ((JdbcStatement3) statement).setFetchTimeout(timeout);

      statement.setFetchSize(fetchSize);

      if(!"mac".equalsIgnoreCase(partitionId))
        statement.execute("set partition to '" + partitionId + "'");

      // Timer info.
      final AtomicBoolean isTimeout = new AtomicBoolean(false);

      final Long timeoutTimerId = vertx.setTimer(1200000, new Handler<Long>() {
        @Override
        public void handle(Long event) {
          isTimeout.set(true);
        }
      });

      log.debug("Executing sql for " + partitionId);
      rs = statement.executeQuery(sql);
      log.debug("Sql complete for " + partitionId);

      if(isTimeout.get()){
        response.putString("status", "error");
        response.putString("message", "Timeout while executing sql : " + sql);
        event.reply(response);
        return;
      }

      vertx.cancelTimer(timeoutTimerId);

      if("csv".equalsIgnoreCase(format)) {
        response.mergeIn(convertToCSV(rs, request.getString("csvSeparator"), includeHeader, request.getString("fileName")));
      } else {
        LimitedMapListHandler handler = new LimitedMapListHandler();
        JsonArray results = JsonUtils.listOfMapsToJsonArray(handler.handle(rs));
        response.putArray("result", results);
      }

      log.debug("Completed request : " + event.body());
      event.reply(response);
    } catch(Exception e){
      e.printStackTrace();
      response.putString("status", "error");
      response.putString("message", e.getMessage());
      event.reply(response);
    } finally {
      close(rs, statement, connection, sql, partitionId);
    }
  }

  private void close(ResultSet rs,Statement statement,Connection connection, String sql, String partitionId) {
    try {
      if (rs != null) rs.close();
    } catch(Exception ex) {
      ex.printStackTrace();
      log.error(String.format("Failed to close ResultSet for partitionId %s with sql %s", partitionId, sql), ex);
    }
    try {
      if (statement != null) statement.close();
    } catch(Exception ex) {
      ex.printStackTrace();
      log.error(String.format("Failed to close Statement for partitionId %s with sql %s", partitionId, sql), ex);
    }
    try {
      if (connection != null) connection.close();
    }catch(Exception ex) {
      log.error(String.format("Failed to close Connection for partitionId %s with sql %s", partitionId, sql), ex);
    }
  }

  private JsonObject convertToCSV(ResultSet rs, String csvSeparator, Boolean includeHeader, String fileName) throws SQLException, IOException {
    JsonObject result = new JsonObject();

    if(fileName != null){
      // Write the results directly to the given file
      Long start = System.currentTimeMillis();
      File file = new File(fileName);
      if(!file.exists()) {
        file.getParentFile().mkdirs();
        file.createNewFile();
      } else {
        file.delete();
        file.createNewFile();
      }

      FileWriter fw = new FileWriter(file.getAbsoluteFile());
      BufferedWriter bw = new BufferedWriter(fw);

      CSVWriter writer;

      if (csvSeparator == null)
        writer = new CSVWriter(bw);
      else
        writer = new CSVWriter(bw, csvSeparator.charAt(0));

      // Just write out the time it takes to read all the records
      result.putNumber("resultCount", writer.writeAll(rs, includeHeader));

      Long elapsed = System.currentTimeMillis() - start;

      File statsFile = new File(fileName + ".stats");
      if(!statsFile.exists()) {
        statsFile.createNewFile();
      } else {
        statsFile.delete();
        statsFile.createNewFile();
      }

      FileWriter fws = new FileWriter(statsFile.getAbsoluteFile());
      BufferedWriter bws = new BufferedWriter(fws);

      bws.write("Took " + elapsed + " ms to write all records");
      bws.close();

      try {
        bw.close();
        writer.close();
        container.logger().info("Writing results to file : " + fileName);
        result.putString("result", file.getAbsolutePath());
        return result;
      } catch (IOException e) {
        container.logger().error("Failed to close resources for CSV Conversion.", e);
        throw e;
      }
    } else {

      StringWriter sw = new StringWriter();

      CSVWriter writer;

      if (csvSeparator == null)
        writer = new CSVWriter(sw);
      else
        writer = new CSVWriter(sw, csvSeparator.charAt(0));

      if(!includeHeader) {
        writer.timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        writer.dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      }

      result.putNumber("resultCount", writer.writeAll(rs, includeHeader));

      try {
        sw.close();
        writer.close();
      } catch (IOException e) {
        container.logger().error("Failed to close resources for CSV Conversion.", e);
      }
      result.putString("result", sw.toString());

      return result;
    }
  }
}
