package com.c9.predictive.components;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.predictive.util.Utils;
import net.kuujo.vertigo.io.batch.OutputBatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 4/29/14.
 */
public class ParseQueriesFromConfigComponent extends DefaultBaseComponentVerticle {

  private String fieldNameFromConfig;

  Logger log = LoggerFactory.getLogger(ParseQueriesFromConfigComponent.class);

  @Override
  public void start(Future<Void> startResult) {

    fieldNameFromConfig = container.config().getString("fieldNameFromConfig", "open_extract");

    super.start(startResult);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();

    if(input.getString(Utils.PropertyConstants.PARTITION_ID) == null){
      errors.put(Utils.PropertyConstants.PARTITION_ID, "partitionId is required");
    }

    if(input.getString(Utils.PropertyConstants.CUSTOMER) == null){
      errors.put(Utils.PropertyConstants.CUSTOMER, "customer is required");
    }

    if(input.getObject("config") == null || (input.getObject("config").getObject(fieldNameFromConfig, new JsonObject()).getObject("tables") == null)){
      errors.put("config."+fieldNameFromConfig, "config." + fieldNameFromConfig + ".tables is required.");
    }
    return errors;
  }

  // TODO : All these components need to be refactored to be able to be side effect free and not rely on init data.
  // TODO : All info that is needed to make the component work should be passed into the input.
  // TODO : This should follow a more purely functional approach! - too many possible side effects now.

  /**
   *
   * @param input
   *
   * This component will enhance the message with the needed path information for the resulting output.
   *
   */
  @Override
  public void processInput(final JsonObject input) {

    output.port(OUTPUTS().OUT).batch(new Handler<OutputBatch>() {
      @Override
      public void handle(OutputBatch outputBatch) {

        // Support overrides of the default.
        final String extractQueriesSection = input.getString(Utils.PropertyConstants.EXTRACT_QUERIES_SECTION, fieldNameFromConfig);

        final JsonObject configQueriesObject = input.getObject("config").getObject(extractQueriesSection, new JsonObject()).getObject("tables");

        if(configQueriesObject == null){
          // This is an error as the extract section needs to be present!
          addErrors(input, "Unable to find the " + Utils.PropertyConstants.EXTRACT_QUERIES_SECTION + " from the predictive.yaml.");
          output.port(OUTPUTS().SEND_EMAIL).send(createErrorEmail(input, "Unable to find the " + Utils.PropertyConstants.EXTRACT_QUERIES_SECTION + " from the predictive.yaml."));
          output.port(OUTPUTS().ERROR_OUT).send(input);
          return;
        }

        final Date today = new Date();

        final String customerName = input.getString(Utils.PropertyConstants.CUSTOMER);

        final String dataLocation = input.getString(Utils.PropertyConstants.DATA_EXTRACTION_LOCATION);

        JsonObject queryReplacements = input.getObject("queryReplacements", new JsonObject());

        if(input.containsField(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE_OVERRIDE)){
          input.putObject("lastOpenExtractInfo", new JsonObject().putString(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE, input.getString(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE_OVERRIDE)));
        } else {
          if (!input.containsField("lastOpenExtractInfo")) {
            input.putObject("lastOpenExtractInfo", new JsonObject().putString(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE, "01-01-1970 01:00:00"));
          }
        }

        String lastExtractString = input.getObject("lastOpenExtractInfo", new JsonObject()).getString(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE, "01-01-1970 01:00:00");

        if(input.getBoolean("fullRescore", false)){
          lastExtractString = "01-01-1970 01:00:00";
        }

        if(lastExtractString != null){
          try {
            Utils.LAST_EXTRACT_DATE_FORMAT.parse(lastExtractString);
            queryReplacements.putString(Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE, lastExtractString);
          } catch(Exception e){
            addErrors(input, "Unable to parse the " + Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE + " from the config entry.");
            output.port(OUTPUTS().SEND_EMAIL).send(createErrorEmail(input, "Failed to parse " + Utils.PropertyConstants.LAST_OPEN_EXTRACT_DATE + " from the config."));
            output.port(OUTPUTS().ERROR_OUT).send(input);
            return;
          }
        }

        for(Map.Entry<String, Object> me : configQueriesObject.toMap().entrySet()){
          String tableName = me.getKey();
          JsonObject sqlQuery = new JsonObject();
          if(me.getValue() instanceof Map){
            sqlQuery = new JsonObject((Map<String, Object>)me.getValue());
          }

          sqlQuery.putString("tableName", tableName);
          sqlQuery.putNumber(Utils.PropertyConstants.LAST_REFRESH_TIME, input.getObject("refreshInfo", new JsonObject()).getNumber("LastRefreshTime", 0));

          queryReplacements.putString(Utils.PropertyConstants.LAST_REFRESH_TIME, input.getObject("refreshInfo", new JsonObject()).getNumber("LastRefreshTime", 0) + "");

          input.putObject(Utils.PropertyConstants.QUERY_REPLACEMENTS, queryReplacements);

          sqlQuery.mergeIn(input);

          String fileOutLocation = String.format("%s%s_%s_%td-%tm-%tY.csv", dataLocation, customerName, tableName, today, today, today);

          // Put the final path on the message for future steps.
          sqlQuery.putString("path", fileOutLocation);

          outputBatch.send(sqlQuery);
        }

        Long start = System.currentTimeMillis();

        outputBatch.end(input.putNumber("start", start).putString("startDate", formatTime(start)));

        output.port(OUTPUTS().SEND_EMAIL).send(createStartEmail(input));
      }
    });
  }

  private JsonObject createStartEmail(JsonObject input) {
    JsonObject email = new JsonObject();

    String subject = "Extraction Started for " + input.getString(Utils.PropertyConstants.CUSTOMER) + " [" + input.getString(Utils.PropertyConstants.PARTITION_ID) + "]";
    email.putString("subject", subject);
    email.putString("template","StartEmail");
    email.putBoolean("generateProgressLink", true);
    email.putObject("templateModel", new JsonObject().putString("title", subject).mergeIn(input));

    return email;
  }
}
