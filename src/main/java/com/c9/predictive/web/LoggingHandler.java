package com.c9.predictive.web;

import com.jetdrone.vertx.yoke.middleware.YokeRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Handler;

public abstract class LoggingHandler<T> implements Handler<YokeRequest> {
	
	private static Logger logger=LoggerFactory.getLogger(LoggingHandler.class);

	public LoggingHandler(){}

	@Override
	public void handle(final YokeRequest request) {
		
		if(logger.isInfoEnabled())
			logger.info("*********************** API :"+request.path()+" ****************** , Requestor : " + request.ip());
		
		handleRequest(request);
	}
	protected abstract void handleRequest(YokeRequest request);
}