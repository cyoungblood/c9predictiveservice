package com.c9.predictive.web;

import com.c9.predictive.util.Utils;
import com.jetdrone.vertx.yoke.Yoke;
import com.jetdrone.vertx.yoke.middleware.Router;
import com.jetdrone.vertx.yoke.middleware.YokeRequest;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.cluster.data.AsyncMap;
import net.kuujo.vertigo.network.ActiveNetwork;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Action1;

import javax.rmi.CORBA.Util;
import java.util.*;

/**
 * Created by yngbldjr on 5/21/14.
 */
public class PredictiveScoringRouter extends Router {

  Logger log = LoggerFactory.getLogger(PredictiveScoringRouter.class);
  RxEventBus rxeb;
  String baseInternalUrl;

  private Map<String, Object> validateForRetrospective(JsonObject jo){

    Map<String, Object> errors = new HashMap<>();

    //Check offSet and offSetDate
    if(!jo.containsField("offset") && !jo.containsField("offsetDate")){
      errors.put("offset/offsetDate", "At least offset or offsetDate is required.");
    } else if(jo.containsField("offSet") && jo.containsField("offsetDate")){
      errors.put("offset/offsetDate", "Only offset or offsetDate are needed, but not both.");
    } else if(jo.containsField("offset")){
      try{
        Integer offSet = jo.getNumber("offset").intValue();
        if(offSet <=0){
          errors.put("offset", "offset must be a number greater than 0.");
        }
      } catch(Exception e){
        errors.put("offset", "offset must be a number greater than 0.");
      }
    } else if(jo.containsField("offsetDate")){
      try {
        Utils.LAST_EXTRACT_DATE_FORMAT.parse(jo.getString("offsetDate"));
      } catch(Exception e){
        errors.put("offsetDate", String.format("offsetDate is not in the correct format (%s).", Utils.LAST_EXTRACT_DATE_FORMAT.toPattern()));
      }
    }

    if(!jo.containsField("extractQueriesSection")){
      errors.put("extractQueriesSection", "extractQueriesSection is required for retrospective call.");
    }

    if(!jo.containsField("partitionId")){
      errors.put("partitionId", "partitionId is required.");
    }

    if(jo.getObject("uploadADS", new JsonObject()).toMap().size() == 0){
      errors.put("uploadADS", "uploadADS is required to have at least a single parameter (SplitDateColumnName or CleanupPreviousRetrospectiveScore)");
    }

    if(!jo.getObject("uploadADS", new JsonObject()).containsField("SplitDateColumnName") && !jo.getObject("uploadADS", new JsonObject()).containsField("CleanupPreviousRetrospectiveScore")){
      errors.put("uploadADS", "uploadADS is required to have at least a single parameter (SplitDateColumnName or CleanupPreviousRetrospectiveScore)");
    }

    if(jo.containsField("mode") && !jo.getString("mode").equalsIgnoreCase("score") && !jo.getString("mode").equalsIgnoreCase("retrospective_scoring")){
      errors.put("mode", "mode must be either score or retrospective_scoring");
    }
    return errors;
  }

  public PredictiveScoringRouter(final Cluster cluster, final Vertx vertx, final JsonObject config){

    rxeb = new RxEventBus(vertx.eventBus());

    post("/internal/api/1/baseextract", new Handler<YokeRequest>(){

      @Override
      public void handle(final YokeRequest event) {

        try {
          JsonObject body = event.body();
          // Valid - can now build the predictive message
          // Set the params that are needed from run Predictive process
          //
          boolean runScore = body.getBoolean("runScore", false);

          boolean runDuration = body.getBoolean("runDuration", false);

          boolean uploadScoreSFDC = body.getBoolean("uploadScoreSFDC", false);

          JsonObject uploadADSJson = body.getObject("uploadADS");
          JsonObject additionalParams = new JsonObject().putObject("uploadADSStoredProdAdditionalParams", uploadADSJson);
          boolean uploadADS = uploadADSJson != null;
          boolean uploadDurationADS = body.getBoolean("uploadDurationADS", false);
          // true ? - is this needed ?? <- Not needed, but keeping for legacy
          boolean runExtraction = body.getBoolean("runExtraction", true);

          //Mandatory
          String partitionId = body.getString(Utils.PropertyConstants.PARTITION_ID);

          String customConfigFile = body.getString("customConfigFile");

          // 0 ? find out why its not just null - aybe its ignored if not > 0, lets check...?
          Long timeout = body.getLong("timeout", 0);

          String hostName = body.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME);
          // Can ignore this and not expect it to come in.
          boolean fullRescore = body.getBoolean("fullRescore", false);
          boolean fromJMS = body.getBoolean("fromJMS", false);

          // If you want to override the score file that will be uploaded to SFDC/ADS
          String scoreFileLocation = body.getString("scoreFileLocation");

          String extractQueriesSection = body.getString("extractQueriesSection");
          Integer offSet = body.getInteger("offset", 0);
          String offSetDate = body.getString("offsetDate");
          String mode = body.getString("mode", "score");

          // This is hideous and should be refactored to a builder pattern, or more explicit api/request object
          runPredictiveProcess(runScore, runDuration, uploadScoreSFDC, uploadADS, uploadDurationADS, runExtraction, partitionId,
              customConfigFile, timeout, hostName, fullRescore, fromJMS, scoreFileLocation, customConfigFile, customConfigFile,
              extractQueriesSection, offSet, offSetDate, mode, vertx, event, additionalParams);

        } catch (Exception e) {
          event.response().end(new JsonObject().putString("message", "Request failed " + e.getMessage()));
          e.printStackTrace();
        }

      }
    });

    post("/internal/api/1/retrospective", new Handler<YokeRequest>(){

      @Override
      public void handle(final YokeRequest event) {

        try {
          JsonObject body = event.body();
          Map<String, Object> errors = validateForRetrospective(body);
          if (errors.size() == 0) {
            // Valid - can now build the predictive message
            // Set the params that are needed from run Predictive process
            //
            boolean runScore = body.getBoolean("runScore", true);

            boolean runDuration = body.getBoolean("runDuration", false);

            boolean uploadScoreSFDC = body.getBoolean("uploadScoreSFDC", false);

            JsonObject uploadADSJson = body.getObject("uploadADS");
            JsonObject additionalParams = new JsonObject().putObject("uploadADSStoredProdAdditionalParams", uploadADSJson);
            boolean uploadADS = uploadADSJson != null;
            boolean uploadDurationADS = body.getBoolean("uploadDurationADS", false);
            // true ? - is this needed ?? <- Not needed, but keeping for legacy
            boolean runExtraction = body.getBoolean("runExtraction", true);

            //Mandatory
            String partitionId = body.getString(Utils.PropertyConstants.PARTITION_ID);

            String customConfigFile = body.getString("customConfigFile");

            // 0 ? find out why its not just null - aybe its ignored if not > 0, lets check...?
            Long timeout = body.getLong("timeout", 0);

            String hostName = body.getString(Utils.PropertyConstants.ANALYTIC_HOSTNAME);
            // Can ignore this and not expect it to come in.
            boolean fullRescore = body.getBoolean("fullRescore", false);
            boolean fromJMS = body.getBoolean("fromJMS", false);

            // If you want to override the score file that will be uploaded to SFDC/ADS
            String scoreFileLocation = body.getString("scoreFileLocation");

            String extractQueriesSection = body.getString("extractQueriesSection");
            Integer offSet = body.getInteger("offset", 0);
            String offSetDate = body.getString("offsetDate");
            String mode = body.getString("mode", "score");

            // This is hideous and should be refactored to a builder pattern, or more explicit api/request object
            runPredictiveProcess(runScore, runDuration, uploadScoreSFDC, uploadADS, uploadDurationADS, runExtraction, partitionId,
                customConfigFile, timeout, hostName, fullRescore, fromJMS, scoreFileLocation, customConfigFile, customConfigFile,
                extractQueriesSection, offSet, offSetDate, mode, vertx, event, additionalParams);

          } else {

            JsonObject errorsJson = new JsonObject(errors);

            event.response().end(new JsonObject().putString("message", "Request is not valid.").putObject("errors", errorsJson));

          }

        } catch (Exception e) {
          event.response().end(new JsonObject().putString("message", "Request failed " + e.getMessage()));
          e.printStackTrace();
        }

      }
    });


    get("/internal/api/1/startnetwork", new Handler<YokeRequest>() {
      @Override
      public void handle(final YokeRequest yokeRequest) {

        final JsonObject jsonNetwork = new JsonObject(vertx.fileSystem().readFileSync("/cloud9/Perforce/Services/nextgen/PredictiveService/config/default_network.json").toString());

        cluster.undeployNetwork(jsonNetwork, new Handler<AsyncResult<Void>>() {
          @Override
          public void handle(AsyncResult<Void> voidAsyncResult) {
            cluster.deployNetwork(jsonNetwork, new Handler<AsyncResult<ActiveNetwork>>() {
              @Override
              public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
                if(activeNetworkAsyncResult.failed()){
                  log.error(activeNetworkAsyncResult.cause().getMessage(), activeNetworkAsyncResult.cause());
                  yokeRequest.response().end("Failed : " + activeNetworkAsyncResult.cause().getMessage());
                } else {
                  yokeRequest.response().end("Deployed Json Network!!");
                  log.info("Deployed Json Network!!");
                }
              }
            });
          }
        });
      }
    });

    baseInternalUrl = String.format("http://%s:%d/internal/api/1/", Utils.getHostName(), config.getInteger("webPort", 9999));

    get("/internal/api/1/viewFile", new Handler<YokeRequest>() {
      @Override
      public void handle(YokeRequest yokeRequest) {
        String file = yokeRequest.getParameter("file");

        if(file == null) {
          yokeRequest.response().end("file parameter was not present.");
          return;
        }

        if(!vertx.fileSystem().existsSync(file)){
          yokeRequest.response().setStatusCode(404);
          yokeRequest.response().end("file not found.");
          return;
        }

        yokeRequest.response().end(vertx.fileSystem().readFileSync(file));
      }
    });

    get("/internal/api/1/run/:partitionId", new Handler<YokeRequest>() {
      @Override
      public void handle(YokeRequest yokeRequest) {
        runPredictiveProcess(yokeRequest, vertx);
      }
    });

    get("/internal/api/1/run/extraction/:partitionId", new Handler<YokeRequest>() {
      @Override
      public void handle(YokeRequest yokeRequest) {
        runPredictiveProcess(yokeRequest, vertx);
      }
    });

    get("/internal/api/1/map/:mapName/remove/:key", new Handler<YokeRequest>() {
      @Override
      public void handle(final YokeRequest yokeRequest) {

        final AsyncMap<String, String> map = cluster.getMap(yokeRequest.params().get("mapName"));

        final String partitionId = yokeRequest.params().get("key");

        map.remove(partitionId, new Handler<AsyncResult<String>>() {
          @Override
          public void handle(AsyncResult<String> stringAsyncResult) {
            if (stringAsyncResult.succeeded()) {
              JsonObject mapContents = new JsonObject();
              if(stringAsyncResult.result() == null){
                mapContents.putString(partitionId, "No data for key");
              } else {
                try {
                  mapContents.putObject(partitionId, new JsonObject(stringAsyncResult.result()));
                } catch(Exception e){
                  mapContents.putString(partitionId, stringAsyncResult.result());
                }
              }
              yokeRequest.response().end(mapContents);
            } else {
              yokeRequest.response().end(new JsonObject().putString("status", "error").putString("message", stringAsyncResult.cause().getMessage()));
            }
          }
        });
      }
    }) ;


    get("/internal/api/1/map/:mapName/:key", new Handler<YokeRequest>() {
      @Override
      public void handle(final YokeRequest yokeRequest) {

        final AsyncMap<String, String> map = cluster.getMap(yokeRequest.params().get("mapName"));

        final String partitionId = yokeRequest.params().get("key");

        map.get(partitionId, new Handler<AsyncResult<String>>() {
          @Override
          public void handle(AsyncResult<String> stringAsyncResult) {
            if (stringAsyncResult.succeeded()) {
              JsonObject mapContents = new JsonObject();
              if(stringAsyncResult.result() == null){
                mapContents.putString(partitionId, "No data for key");
              } else {
                try {
                  mapContents.putObject(partitionId, new JsonObject(stringAsyncResult.result()));
                } catch(Exception e){
                  mapContents.putString(partitionId, stringAsyncResult.result());
                }
              }
              yokeRequest.response().end(mapContents);
            } else {
              yokeRequest.response().end(new JsonObject().putString("status", "error").putString("message", stringAsyncResult.cause().getMessage()));
            }
          }
        });
      }
    });

    get("/internal/api/1/map/:mapName", new Handler<YokeRequest>() {
      @Override
      public void handle(final YokeRequest yokeRequest) {

        String mapName = yokeRequest.params().get("mapName");

        log.info("Trying to get map (" + mapName + ") data");

        final AsyncMap<String, String> map = cluster.getMap(mapName);

        map.values(new Handler<AsyncResult<Collection<String>>>() {
          @Override
          public void handle(AsyncResult<Collection<String>> collectionAsyncResult) {
            if (collectionAsyncResult.succeeded()) {
              JsonObject mapContents = new JsonObject();
              JsonArray results = new JsonArray();
              for (String s : collectionAsyncResult.result()) {
                try {
                  results.add(new JsonObject(s));
                } catch (Exception e) {
                  results.add(s);
                }
              }
              mapContents.putArray("values", results);
              yokeRequest.response().end(mapContents);

            } else {
              yokeRequest.response().end(new JsonObject().putString("status", "error").putString("message", collectionAsyncResult.cause().getMessage()));
            }
          }
        });
      }
    });

    get("/internal/api/1/workqueue/pause/:partitionId", pauseWorkQueueHandler);
    get("/internal/api/1/workqueue/resume/:partitionId", resumeWorkQueueHandler);
    get("/internal/api/1/workqueue/info/:partitionId", infoWorkQueueHandler);
  }

  private void runPredictiveProcess(boolean runScore, boolean runDuration, boolean uploadScoreSFDC,  boolean uploadADS, boolean uploadDurationADS, boolean runExtraction, String partitionId, String customConfigFile, Long timeout, String hostName, boolean fullRescore,
                                    boolean fromJMS,String scoreFileLocationOverride, String sfdcUsernameOverride, String sfdcPasswordOverride,
                                    // Introduced to support retrosoective
                                    String extractQueriesSection,
                                    Integer offSet,
                                    String offSetDate,
                                    // score | retrospective
                                    String mode,
                                    Vertx vertx,
                                    YokeRequest yokeRequest,
                                    JsonObject additionalOptions) {
    JsonObject predictionMessage = new JsonObject()
        .mergeIn(additionalOptions)
        .putString(Utils.PropertyConstants.PARTITION_ID, partitionId)
        .putBoolean("runScore", runScore)
        .putBoolean("runDuration", runDuration)
        .putBoolean("uploadScoreSFDC", uploadScoreSFDC)
        .putBoolean("uploadADS", uploadADS)
        .putBoolean("uploadDurationADS", uploadDurationADS)
        .putBoolean("runExtraction", runExtraction)
        .putBoolean("fullRescore", fullRescore)
        .putBoolean("fromJMS", fromJMS)
        .putString("servicename", "fromweb")
        .putString(Utils.PropertyConstants.CORRELATION_ID, UUID.randomUUID().toString())
        .putString("server", Utils.getHostName());

    if(hostName != null){
      predictionMessage.putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, hostName);
    }

    if(timeout != null && timeout > 0){
      predictionMessage.putNumber("timeoutTimestamp", DateTime.now().plusMillis(timeout.intValue()).getMillis());
    }

    if(customConfigFile != null){
      predictionMessage.putString("customConfigFile", customConfigFile);
    }

    if(scoreFileLocationOverride != null){
      predictionMessage.putString("scoreFileLocation", scoreFileLocationOverride);
    }

    if (uploadScoreSFDC) {

      String username = sfdcUsernameOverride;
      String password = sfdcUsernameOverride;

      if(username != null  && password != null) {
        predictionMessage
            .putString("userName", username)
            .putString("password", password);
      }
    }

    if(extractQueriesSection != null){
      predictionMessage.putString(Utils.PropertyConstants.EXTRACT_QUERIES_SECTION, extractQueriesSection);
    }

    JsonObject queryReplacements = new JsonObject();

     if(offSet > 0){
      DateTime dt = DateTime.now().minusDays(offSet);
      queryReplacements.putString("offsetDate", Utils.LAST_EXTRACT_DATE_FORMAT.format(dt.toDate()));
    } else if(offSetDate != null){
      try {
        Date d = Utils.LAST_EXTRACT_DATE_FORMAT.parse(offSetDate);
        queryReplacements.putString("offsetDate", Utils.LAST_EXTRACT_DATE_FORMAT.format(d));
      } catch(Exception e){
        e.printStackTrace();
      }
    }

    predictionMessage.putObject(Utils.PropertyConstants.QUERY_REPLACEMENTS, queryReplacements);

    if(mode != null){
      predictionMessage.putString(Utils.PropertyConstants.SCORE_MODE, mode);
    }

    vertx.eventBus().send("predictive.from.web", predictionMessage);
    yokeRequest.response().end(new JsonObject().putString("message", "Scoring request complete")
        .putObject("predictionMessage", predictionMessage)
        .putString("progressUrl", baseInternalUrl + "map/scoring_progress/" + partitionId)
        .putString("historyUrl", baseInternalUrl + "map/scoring_history/" + partitionId));


  }

  private void runPredictiveProcess(YokeRequest yokeRequest, Vertx vertx){

    boolean runScore = Boolean.parseBoolean(yokeRequest.getParameter("runScore", "false"));
    boolean runDuration = Boolean.parseBoolean(yokeRequest.getParameter("runDuration", "false"));
    boolean uploadScoreSFDC = Boolean.parseBoolean(yokeRequest.getParameter("uploadScoreSFDC", "false"));
    boolean uploadADS = Boolean.parseBoolean(yokeRequest.getParameter("uploadADS", "false"));
    boolean uploadDurationADS = Boolean.parseBoolean(yokeRequest.getParameter("uploadDurationADS", "false"));
    boolean runExtraction = Boolean.parseBoolean(yokeRequest.getParameter("runExtraction", "true"));
    String partitionId = yokeRequest.params().get(Utils.PropertyConstants.PARTITION_ID);
    String customConfigFile = yokeRequest.getParameter("customConfigFile");
    Long timeout = Long.parseLong(yokeRequest.getParameter("timeout", "0"));
    String hostName = yokeRequest.getParameter(Utils.PropertyConstants.ANALYTIC_HOSTNAME);
    boolean fullRescore = Boolean.parseBoolean(yokeRequest.getParameter("fullRescore", "false"));
    boolean fromJMS = Boolean.parseBoolean(yokeRequest.getParameter("fromJMS", "false"));
    String scoreFileLocation = yokeRequest.getParameter("scoreFileLocation");

    // For Retrospective - movng to explicit api call and refactoring the above
    String extractQueriesSection = yokeRequest.getParameter("extractQueriesSection");
    Integer offSet = Integer.parseInt(yokeRequest.getParameter("offset", "0"));
    String offSetDate = yokeRequest.getParameter("offsetDate");
    String mode = yokeRequest.getParameter("mode");

    // This is hideous and should be refactored to a builder pattern, or more explicit api/request object
    runPredictiveProcess(runScore, runDuration, uploadScoreSFDC, uploadADS, uploadDurationADS, runExtraction, partitionId, customConfigFile, timeout, hostName, fullRescore, fromJMS, scoreFileLocation, customConfigFile, customConfigFile, extractQueriesSection, offSet, offSetDate, mode, vertx, yokeRequest, new JsonObject());


  }

  Handler<YokeRequest> infoWorkQueueHandler = new LoggingHandler<YokeRequest>(){
    @Override
    protected void handleRequest(final YokeRequest request) {
      final String partitionId = request.params().get(Utils.PropertyConstants.PARTITION_ID);

      Observable<RxMessage<JsonObject>> info = rxeb.send(String.format("work.queue.info", partitionId), new JsonObject().putString(Utils.PropertyConstants.PARTITION_ID, partitionId));

      info.subscribe(new Action1<RxMessage<JsonObject>>(){
        @Override
        public void call(RxMessage<JsonObject> t1) {
          request.response().end(t1.body());
        }
      });
    }
  };

  Handler<YokeRequest> pauseWorkQueueHandler = new LoggingHandler<YokeRequest>(){
    @Override
    protected void handleRequest(final YokeRequest request) {
      final String partitionId = request.params().get(Utils.PropertyConstants.PARTITION_ID);

      Observable<RxMessage<JsonObject>> pause = rxeb.send(String.format("work.queue.pause", partitionId), new JsonObject().putString(Utils.PropertyConstants.PARTITION_ID, partitionId));

      pause.subscribe(new Action1<RxMessage<JsonObject>>(){
        @Override
        public void call(RxMessage<JsonObject> t1) {
          request.response().end(t1.body());
        }
      });
    }
  };

  Handler<YokeRequest> resumeWorkQueueHandler = new LoggingHandler<YokeRequest>(){
    @Override
    protected void handleRequest(final YokeRequest request) {
      final String partitionId = request.params().get(Utils.PropertyConstants.PARTITION_ID);

      Observable<RxMessage<JsonObject>> resume = rxeb.send(String.format("work.queue.resume", partitionId), new JsonObject().putString(Utils.PropertyConstants.PARTITION_ID, partitionId));

      resume.subscribe(new Action1<RxMessage<JsonObject>>(){
        @Override
        public void call(RxMessage<JsonObject> t1) {
          request.response().end(t1.body());
        }
      });
    }
  };


}
