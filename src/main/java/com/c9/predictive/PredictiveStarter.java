package com.c9.predictive;

import com.c9.generic.components.common.BasicDecisionRouter;
import com.c9.generic.components.common.SimpleAggregator;
import com.c9.generic.components.data.DeleteDataInClusterMap;
import com.c9.generic.components.data.HasKeyInClusterMap;
import com.c9.generic.components.data.PutDataInClusterMap;
import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusPublisherComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import com.c9.predictive.components.*;
import com.c9.predictive.components.datascience.PythonScriptComponent;
import com.c9.predictive.components.sfdcupload.UploadScoreToSFDCDecisionComponent;
import com.c9.predictive.components.sfdcupload.UploadScoresToSF;
import com.c9.predictive.hooks.C9CustomHook;
import com.c9.predictive.util.Utils;
import com.c9.predictive.web.PredictiveScoringRouter;
import com.c9.predictive.web.ServerHook;
import com.jetdrone.vertx.yoke.Yoke;
import com.jetdrone.vertx.yoke.middleware.BodyParser;
import com.jetdrone.vertx.yoke.middleware.ErrorHandler;
import com.jetdrone.vertx.yoke.middleware.Static;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.component.VerticleConfig;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.sockjs.SockJSServer;
import org.vertx.java.platform.Verticle;

/**
 * Created by yngbldjr on 4/25/14.
 */
public class PredictiveStarter extends Verticle {

  public static final String PREDICTIVE_STARTED_EVENT = "predictive.started.event";
  static JsonObject defaultDataServicesConfig = new JsonObject();

  Integer webPort;
  String webRoot;
  JsonObject dataServicesConfig;

  Logger log = LoggerFactory.getLogger(PredictiveStarter.class);

  public static final String SHARED_DATA_CLUSTER_NAME = "predictive-shared-data-cluster";

  static {
    // Defaults if no config set
    defaultDataServicesConfig.putString("macURL", "jdbc:certive://qen-mac1:5432/NW;SSL=false;username='su';password='password'");
    defaultDataServicesConfig.putString("workQueueVersion", "com.c9~workqueue~1.0");
    defaultDataServicesConfig.putBoolean("enableMetrics", false);
    defaultDataServicesConfig.putString("address", "predictive.jdbc"); // Used to deploy the dataservices mod
  }

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    dataServicesConfig = container.config().getObject("dataServices", defaultDataServicesConfig);

    deployClusterAndNetwork(startResult);
  }

  private void deployClusterAndNetwork(final Future<Void> startResult) {
    final Vertigo vertigo = new Vertigo(this);

    // First start the cluster that shares the map of data - allows the 2 services to lookup from the same shared map.
    // Then deploy the data service module to each service
    // Finally deploy the network - each service will deploy the same network as to isolate the end to end run on a single service
    vertigo.deployCluster(SHARED_DATA_CLUSTER_NAME, new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterAsyncResult) {
        if(clusterAsyncResult.succeeded()){
          Cluster cluster = clusterAsyncResult.result();
          startWebInterface(cluster, startResult);
          vertigo.deployCluster("predictive-cluster-"+Utils.getHostName(), new Handler<AsyncResult<Cluster>>() {
            @Override
            public void handle(AsyncResult<Cluster> clusterAsyncResult) {
              if(clusterAsyncResult.succeeded()){
                Cluster cluster = clusterAsyncResult.result();
                startDataServices(cluster, startResult);
              }  else{
                startResult.setFailure(clusterAsyncResult.cause());
              }
            }
          });
        }  else{
          startResult.setFailure(clusterAsyncResult.cause());
        }
      }
    });
  }

  private void startDataServices(final Cluster cluster, final Future<Void> startResult) {

    container.deployWorkerVerticle(SqlExecutorComponent.class.getName(), dataServicesConfig, 4, true, new Handler<AsyncResult<String>>() {
      @Override
      public void handle(AsyncResult<String> stringAsyncResult) {
        log.info("Deployed SqlExecutorComponent on local jvm.");
        if(stringAsyncResult.succeeded()){
          startScoreNetwork(cluster, startResult);
        } else{
          startResult.setFailure(stringAsyncResult.cause());
        }
      }
    });
  }

  private void startScoreNetwork(final Cluster cluster, final Future<Void> startResult) {
    final Vertigo vertigo = new Vertigo(this);

    final JsonObject networkConfig = this.container.config().putString("sharedDataCluster", SHARED_DATA_CLUSTER_NAME);

    log.info("Starting Network for Predictive Scoring.");
    log.info("Current Config : " + networkConfig);

    NetworkConfig network = vertigo.createNetwork("score.listener");

    //network.addComponent("jmsListener", JMSListenComponent.class.getName(), networkConfig.putString("startedAddress", PREDICTIVE_STARTED_EVENT), 1).addHook(new C9CustomHook());
    network.addComponent("ebListener", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "predictive.from.web").mergeIn(networkConfig), 2).addHook(new C9CustomHook());

    JsonObject scoreMapJsonConfig = new JsonObject().mergeIn(networkConfig).putString("mapName", "scoring_progress").putString("key", Utils.PropertyConstants.PARTITION_ID);
    network.addComponent("isScoringInProgress", ScoringProgress.class.getName(), scoreMapJsonConfig, 1).addHook(new C9CustomHook());
    network.addComponent("setOrUpdateMap", PutDataInClusterMap.class.getName(), scoreMapJsonConfig, 1).addHook(new C9CustomHook());

    network.addComponent("errorHandler", ErrorHandlerComponent.class.getName(), networkConfig, 1).addHook(new C9CustomHook());

    network.addComponent("removeFromMap", DeleteDataInClusterMap.class.getName(), scoreMapJsonConfig, 1).addHook(new C9CustomHook());

    network.addComponent("setOrUpdateHistoryMap", PutDataInClusterMap.class.getName(), scoreMapJsonConfig.putString("mapName", "scoring_history"), 1).addHook(new C9CustomHook());

    network.addComponent("customerLookup", LookupCustomerComponent.class.getName(), networkConfig, 1).addHook(new C9CustomHook());

    network.addComponent("timeoutChecker", TimeoutCleanupComponent.class.getName(), scoreMapJsonConfig, 1).addHook(new C9CustomHook());

    network.addComponent("lookupCustomConfig", LookupCustomConfigOverrideComponent.class.getName(), networkConfig, 1).addHook(new C9CustomHook());
    network.addComponent("parseConfig", ParseYAMLComponent.class.getName(), networkConfig, 1).addHook(new C9CustomHook());
    network.addComponent("parseLastOpenExtractDate", ParseYAMLComponent.class.getName(), new JsonObject().putString("fileKey", "lastOpenExtractDateFile").putString("outKey", "lastOpenExtractInfo").putBoolean("failOnError", false).putArray("valuesToKeep", new JsonArray().add("last_open_extract_date")), 1).addHook(new C9CustomHook());
    network.addComponent("performExtract", BasicDecisionRouter.class.getName(), new JsonObject().putString("booleanFieldName", "runExtraction")).addHook(new C9CustomHook());
    network.addComponent("parseHistoryConfig", ParseYAMLComponent.class.getName(), networkConfig.putString("fileKey", "historyFile").putString("outKey", "historyConfig").putBoolean("failOnError", false), 1).addHook(new C9CustomHook());
    network.addComponent("parseQueriesFromJson", ParseQueriesFromConfigComponent.class.getName(), networkConfig, 1).addHook(new C9CustomHook());

    VerticleConfig extractComponent = network.addComponent("extractAndRunQueries", ExtractDataComponent.class.getName(), new JsonObject().putString("address", networkConfig.getObject("dataServices").getString("address")), 1);
    extractComponent.addHook(new C9CustomHook());

    network.addComponent("performScore", BasicDecisionRouter.class.getName(), new JsonObject().putString("booleanFieldName", "runScore")).addHook(new C9CustomHook());
    network.addComponent("performDuration", BasicDecisionRouter.class.getName(), new JsonObject().putString("booleanFieldName", "runDuration")).addHook(new C9CustomHook());

    network.addComponent("uploadScoresToSFDCDecision", UploadScoreToSFDCorADSDecisionComponent.class.getName(), new JsonObject().putString(UploadScoreToSFDCorADSDecisionComponent.OBJECT_FIELD_NAME, "config").putString(UploadScoreToSFDCorADSDecisionComponent.BOOLEAN_FIELD_NAME, "uploadScoreSFDC")).addHook(new C9CustomHook());

    network.addComponent("uploadScoresToADSDecision", UploadScoreToSFDCDecisionComponent.class.getName(), new JsonObject().putString(UploadScoreToSFDCorADSDecisionComponent.OBJECT_FIELD_NAME, "config").putString(UploadScoreToSFDCorADSDecisionComponent.BOOLEAN_FIELD_NAME, "uploadADS")).addHook(new C9CustomHook());
    network.addComponent("uploadDurationScoresToADSDecision", UploadScoreToSFDCDecisionComponent.class.getName(), new JsonObject().putString(UploadScoreToSFDCorADSDecisionComponent.OBJECT_FIELD_NAME, "config").putString(UploadScoreToSFDCorADSDecisionComponent.BOOLEAN_FIELD_NAME, "uploadDurationADS")).addHook(new C9CustomHook());

    network.addComponent("uploadScoreADS", UploadScoreDataStoredProcComponent.class.getName(), new JsonObject().mergeIn(networkConfig).putString("address", networkConfig.getObject("dataServices").getString("address")));
    network.addComponent("uploadDurationScoreADS", UploadDurationScoreDataStoredProcComponent.class.getName(), new JsonObject().mergeIn(networkConfig).putString("address", networkConfig.getObject("dataServices").getString("address")));

    network.addComponent("email", EmailComponent.class.getName(), networkConfig, 1);
    network.addComponent("lastRefreshTime", LastADSRefreshComponent.class.getName(), new JsonObject().putString("address", networkConfig.getObject("dataServices").getString("address")));

    network.addComponent("updateConfigDecision", UpdateConfigDecisionRouter.class.getName());
    network.addComponent("updateConfig", UpdateYAMLConfig.class.getName(), new JsonObject().putString("fileKey", "lastOpenExtractDateFile").putString("inKey", "lastOpenExtractInfo"), 1);

    network.addComponent("sendToJMS", BasicDecisionRouter.class.getName(), new JsonObject().putString("booleanFieldName", "fromJMS")).addHook(new C9CustomHook());

    network.addComponent("scoreAggregator", SimpleAggregator.class.getName(), new JsonObject().putNumber("count", 2).putString("key", "correlationId").putArray("mergedArrays", new JsonArray().add("errors").add("infoMessages").add("timers").add("links")), 1);

    VerticleConfig uploadSFDCScores = network.addComponent("uploadScoreSFDC", UploadScoresToSF.class.getName(), new JsonObject().mergeIn(networkConfig).putString("address", networkConfig.getObject("dataServices").getString("address")), 1);
    uploadSFDCScores.addHook(new C9CustomHook());
    uploadSFDCScores.setWorker(true);

    JsonObject scoreConfigObject = createScoreObject(networkConfig);
    VerticleConfig scoreVerticle = network.addComponent("score", PythonScriptComponent.class.getName(), scoreConfigObject, 1);
    scoreVerticle.addHook(new C9CustomHook());
    scoreVerticle.setWorker(true);

    JsonObject durationConfigObject = createDurationModelScoreObject(networkConfig);
    VerticleConfig durationVerticle = network.addComponent("duration", PythonScriptComponent.class.getName(), durationConfigObject, 1);
    durationVerticle.addHook(new C9CustomHook());
    durationVerticle.setWorker(true);

    network.addComponent("cleanupFolder", FolderCleanupComponent.class.getName(), networkConfig, 1).addHook(new C9CustomHook());

    network.addComponent("sendJMS", JMSSendComponent.class.getName(), networkConfig, 1).addHook(new C9CustomHook());
    network.addComponent("notifier", EventBusPublisherComponent.class.getName(), new JsonObject().mergeIn(networkConfig).putString("eventbus.address", "score.complete"), 1).addHook(new C9CustomHook());

    //network.createConnection("jmsListener", JMSListenComponent.OUTPUTS().OUT, "customerLookup", LookupCustomerComponent.INPUTS().IN);
    network.createConnection("ebListener", EventBusListenerComponent.OUTPUTS().OUT, "customerLookup", LookupCustomerComponent.INPUTS().IN);

    network.createConnection("customerLookup", LookupCustomerComponent.OUTPUTS().OUT, "isScoringInProgress", HasKeyInClusterMap.INPUTS().IN);
    network.createConnection("isScoringInProgress", HasKeyInClusterMap.OUTPUTS().YES, "errorHandler", ErrorHandlerComponent.INPUTS().DUPLICATE);

    network.createConnection("isScoringInProgress", HasKeyInClusterMap.OUTPUTS().YES, "notifier", EventBusPublisherComponent.INPUTS().IN);
    network.createConnection("isScoringInProgress", HasKeyInClusterMap.OUTPUTS().NO, "timeoutChecker", TimeoutCleanupComponent.INPUTS().IN);

    network.createConnection("timeoutChecker", TimeoutCleanupComponent.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);
    network.createConnection("timeoutChecker", TimeoutCleanupComponent.OUTPUTS().OUT, "sendToJMS", JMSSendComponent.INPUTS().IN);
    network.createConnection("timeoutChecker", TimeoutCleanupComponent.OUTPUTS().TIMEOUT, "scoreAggregator", SimpleAggregator.INPUTS().TIMEOUT);
    network.createConnection("timeoutChecker", TimeoutCleanupComponent.OUTPUTS().TIMEOUT, "errorHandler", ErrorHandlerComponent.INPUTS().TIMEOUT);
    network.createConnection("errorHandler", ErrorHandlerComponent.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);

    network.createConnection("isScoringInProgress", HasKeyInClusterMap.OUTPUTS().NO, "cleanupFolder", FolderCleanupComponent.INPUTS().IN);

    network.createConnection("cleanupFolder", FolderCleanupComponent.OUTPUTS().OUT, "lastRefreshTime", LastADSRefreshComponent.INPUTS().IN);

    network.createConnection("lastRefreshTime", LastADSRefreshComponent.OUTPUTS().OUT, "lookupCustomConfig", LookupCustomConfigOverrideComponent.INPUTS().IN);

    network.createConnection("lookupCustomConfig", LookupCustomConfigOverrideComponent.OUTPUTS().OUT, "parseConfig", ParseYAMLComponent.INPUTS().IN);

    network.createConnection("parseConfig", ParseYAMLComponent.OUTPUTS().OUT, "parseLastOpenExtractDate", BasicDecisionRouter.INPUTS().IN);

    network.createConnection("parseLastOpenExtractDate", ParseYAMLComponent.OUTPUTS().OUT, "performExtract", BasicDecisionRouter.INPUTS().IN);

    network.createConnection("performExtract", BasicDecisionRouter.OUTPUTS().YES, "parseQueriesFromJson", ParseYAMLComponent.INPUTS().IN);
    network.createConnection("performExtract", BasicDecisionRouter.OUTPUTS().NO, "performScore", BasicDecisionRouter.INPUTS().IN);
    network.createConnection("performExtract", BasicDecisionRouter.OUTPUTS().NO, "performDuration", BasicDecisionRouter.INPUTS().IN);

    network.createConnection("parseQueriesFromJson", ParseQueriesFromConfigComponent.OUTPUTS().OUT, "extractAndRunQueries", ExtractDataComponent.INPUTS().IN);
    network.createConnection("parseQueriesFromJson", ParseQueriesFromConfigComponent.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);

    network.createConnection("extractAndRunQueries", ExtractDataComponent.OUTPUTS().UPDATE_STATUS, "setOrUpdateMap", PutDataInClusterMap.INPUTS().IN);

    // Send to decision component to decide on running score/duration
    network.createConnection("extractAndRunQueries", ExtractDataComponent.OUTPUTS().OUT, "performScore", BasicDecisionRouter.INPUTS().IN);
    network.createConnection("extractAndRunQueries", ExtractDataComponent.OUTPUTS().OUT, "performDuration", BasicDecisionRouter.INPUTS().IN);

    network.createConnection("extractAndRunQueries", ExtractDataComponent.OUTPUTS().NO_DATA, "errorHandler", ErrorHandlerComponent.INPUTS().NO_DATA);
    network.createConnection("extractAndRunQueries", ExtractDataComponent.OUTPUTS().ERROR_OUT, "errorHandler", ErrorHandlerComponent.INPUTS().IN);

    // Add component to make a decision to score or not
    network.createConnection("performScore", BasicDecisionRouter.OUTPUTS().YES, "score", PythonScriptComponent.INPUTS().IN);
    network.createConnection("performDuration", BasicDecisionRouter.OUTPUTS().YES, "duration", PythonScriptComponent.INPUTS().IN);

    network.createConnection("performScore", BasicDecisionRouter.OUTPUTS().NO, "uploadScoresToSFDCDecision", UploadScoreToSFDCorADSDecisionComponent.INPUTS().IN);

    network.createConnection("performScore", BasicDecisionRouter.OUTPUTS().NO, "scoreAggregator", SimpleAggregator.INPUTS().IN);
    network.createConnection("performDuration", BasicDecisionRouter.OUTPUTS().NO, "scoreAggregator", SimpleAggregator.INPUTS().IN);

    // Only update the yaml (holds the last extract date) when score is successful.
    network.createConnection("score", PythonScriptComponent.OUTPUTS().UPDATE_STATUS, "setOrUpdateMap", PutDataInClusterMap.INPUTS().IN);
    network.createConnection("score", PythonScriptComponent.OUTPUTS().OUT, "scoreAggregator", SimpleAggregator.INPUTS().IN);
    network.createConnection("score", PythonScriptComponent.OUTPUTS().ERROR_OUT, "scoreAggregator", SimpleAggregator.INPUTS().IN);
    network.createConnection("score", PythonScriptComponent.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);

    // Only update the yaml (holds the last extract date) when score is successful.
    network.createConnection("duration", PythonScriptComponent.OUTPUTS().UPDATE_STATUS, "setOrUpdateMap", PutDataInClusterMap.INPUTS().IN);
    network.createConnection("duration", PythonScriptComponent.OUTPUTS().OUT, "scoreAggregator", SimpleAggregator.INPUTS().IN);
    network.createConnection("duration", PythonScriptComponent.OUTPUTS().ERROR_OUT, "scoreAggregator", SimpleAggregator.INPUTS().IN);
    network.createConnection("duration", PythonScriptComponent.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);

    // SFDC Process to upload data to opp score object
    network.createConnection("score", PythonScriptComponent.OUTPUTS().OUT, "uploadScoresToSFDCDecision", UploadScoreToSFDCorADSDecisionComponent.INPUTS().IN);
    network.createConnection("uploadScoresToSFDCDecision", UploadScoreToSFDCorADSDecisionComponent.OUTPUTS().YES, "uploadScoreSFDC", UploadScoresToSF.INPUTS().IN);
    network.createConnection("uploadScoreSFDC", UploadScoresToSF.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);

    network.createConnection("scoreAggregator", SimpleAggregator.OUTPUTS().OUT, "sendToJMS", BasicDecisionRouter.INPUTS().IN);

    network.createConnection("sendToJMS", BasicDecisionRouter.OUTPUTS().YES, "sendJMS", JMSSendComponent.INPUTS().IN);
    network.createConnection("sendToJMS", BasicDecisionRouter.OUTPUTS().NO, "notifier", EventBusSenderComponent.INPUTS().IN);

    network.createConnection("sendToJMS", BasicDecisionRouter.OUTPUTS().OUT, "removeFromMap", DeleteDataInClusterMap.INPUTS().IN);
    network.createConnection("sendToJMS", BasicDecisionRouter.OUTPUTS().OUT, "timeoutChecker", TimeoutCleanupComponent.INPUTS().CLEAR_TIMER);
    network.createConnection("sendToJMS", BasicDecisionRouter.OUTPUTS().OUT, "setOrUpdateHistoryMap", PutDataInClusterMap.INPUTS().IN);

    network.createConnection("scoreAggregator", SimpleAggregator.OUTPUTS().OUT, "updateConfigDecision", UpdateConfigDecisionRouter.INPUTS().IN);
    network.createConnection("updateConfigDecision", UpdateConfigDecisionRouter.OUTPUTS().YES, "updateConfig", UpdateYAMLConfig.INPUTS().IN);
    network.createConnection("scoreAggregator", SimpleAggregator.OUTPUTS().OUT, "uploadScoresToADSDecision", BasicDecisionRouter.INPUTS().IN);
    network.createConnection("scoreAggregator", SimpleAggregator.OUTPUTS().OUT, "uploadDurationScoresToADSDecision", BasicDecisionRouter.INPUTS().IN);
    network.createConnection("scoreAggregator", SimpleAggregator.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);


    network.createConnection("uploadScoresToADSDecision", UploadScoreToSFDCorADSDecisionComponent.OUTPUTS().YES, "uploadScoreADS", UploadScoreDataStoredProcComponent.INPUTS().IN);
    network.createConnection("uploadScoreADS", UploadScoreDataStoredProcComponent.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);

    network.createConnection("uploadDurationScoresToADSDecision", UploadScoreToSFDCorADSDecisionComponent.OUTPUTS().YES, "uploadDurationScoreADS", UploadDurationScoreDataStoredProcComponent.INPUTS().IN);
    network.createConnection("uploadDurationScoreADS", UploadDurationScoreDataStoredProcComponent.OUTPUTS().SEND_EMAIL, "email", EmailComponent.INPUTS().IN);


    // Possible Errors paths
    network.createConnection("lastRefreshTime", LastADSRefreshComponent.OUTPUTS().ERROR_OUT, "errorHandler", ErrorHandlerComponent.INPUTS().IN);
    network.createConnection("parseConfig", ParseYAMLComponent.OUTPUTS().ERROR_OUT, "errorHandler", ErrorHandlerComponent.INPUTS().IN);
    network.createConnection("parseHistoryConfig", ParseYAMLComponent.OUTPUTS().ERROR_OUT, "errorHandler", ErrorHandlerComponent.INPUTS().IN);
    network.createConnection("customerLookup", LookupCustomerComponent.OUTPUTS().ERROR_OUT, "errorHandler", ErrorHandlerComponent.INPUTS().IN);
    network.createConnection("parseQueriesFromJson", ParseQueriesFromConfigComponent.OUTPUTS().ERROR_OUT, "errorHandler", ErrorHandlerComponent.INPUTS().IN);

    network.createConnection("errorHandler", ErrorHandlerComponent.OUTPUTS().OUT, "sendToJMS", BasicDecisionRouter.INPUTS().IN);

    cluster.deployNetwork(network, new Handler<AsyncResult<ActiveNetwork>>() {
      @Override
      public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
        if (activeNetworkAsyncResult.failed()) {
          log.error("Failed to start Predictive Service.", activeNetworkAsyncResult.cause());
          startResult.setFailure(activeNetworkAsyncResult.cause());
        } else {
          log.info("Predictive Service Started successfully!");
          vertx.eventBus().publish(PREDICTIVE_STARTED_EVENT, new JsonObject());
          startResult.setResult(null);
        }
      }
    });
  }

  private JsonObject createScoreObject(JsonObject networkConfig) {
    JsonObject scoreObject = new JsonObject().mergeIn(networkConfig);

    scoreObject.putString("defaultModel", "opp_score.model");
    scoreObject.putString("configProperty", "score_config");
    scoreObject.putString("modelProperty", "score_model");
    scoreObject.putString("scriptTitle", "Opportunity Score");
    scoreObject.putString("fileLocationOutProperty", Utils.PropertyConstants.SCORE_FILE_LOCATION);
    scoreObject.putString("resultOutProperty", "scoreResults");
    scoreObject.putString("customerLogName", "opp_score");
    scoreObject.putString("verboseLoggingProperty", "score.verbose.logging");

    scoreObject.putString("pythonScript", networkConfig.getObject("scoreEngine").getString("pythonScript"));
    scoreObject.putString("scoreFileName", networkConfig.getObject("scoreEngine").getString("scoreFileName"));

    return scoreObject;
  }

  private JsonObject createDurationModelScoreObject(JsonObject networkConfig) {
    JsonObject scoreObject = new JsonObject().mergeIn(networkConfig);

    scoreObject.putString("defaultModel", "duration_model");
    scoreObject.putString("configProperty", "duration_config");
    scoreObject.putString("modelProperty", "duration_model");
    scoreObject.putString("scriptTitle", "Duration Model Score");
    scoreObject.putString("fileLocationOutProperty", Utils.PropertyConstants.DURATION_SCORE_FILE_LOCATION);
    scoreObject.putString("resultOutProperty", "durationScoreResults");
    scoreObject.putString("customerLogName", "duration_model_score");
    scoreObject.putString("verboseLoggingProperty", "duration.verbose.logging");

    scoreObject.putString("pythonScript", networkConfig.getObject("scoreEngine").getString("pythonDurationScript"));
    scoreObject.putString("scoreFileName", networkConfig.getObject("scoreEngine").getString("durationFileName"));

    return scoreObject;
  }

  private void startWebInterface(Cluster result, Future<Void> startedResult) {

    webPort = container.config().getInteger("webPort", 9999);
    webRoot = container.config().getString("webRoot", "webroot/");

    log.info("Config for starting web interface : " + container.config());

    final Yoke yoke = new Yoke(this);

    // Title for 404/Error page
    yoke.set("title", "C9Inc");
    yoke.set("x-powered-by", false);

    yoke.use(new Static("webroot"));

    yoke.use(new BodyParser());

    log.info("Starting web listeners.");

    yoke.use(new PredictiveScoringRouter(result, vertx, container.config()));
    // Create a http server

    HttpServer server = vertx.createHttpServer();
    // Set the server for Yoke
    yoke.listen(server);

    SockJSServer internalSockJSServer = vertx.createSockJSServer(server);

    JsonArray inboundPermitted = new JsonArray();
    // Let everything through
    // Here is where we can restrict the eventbus use from the client
    inboundPermitted.add(new JsonObject());
    // Here is where we can define the event bus addresses that can be allowed to the Socksjs client
    // For now let all addresses through
    JsonArray outboundPermitted = new JsonArray();
    // Let everything through
    // Here is where we can restrict the eventbus use from the client
    outboundPermitted.add(new JsonObject());

    // Track the connections of the socksjs connections
    internalSockJSServer.setHook(new ServerHook());

    yoke.use(new ErrorHandler(true));

    // Create event bus bridge for socksjs support in front end
    internalSockJSServer.bridge(new JsonObject().putString("prefix", "/eventbus"), inboundPermitted, outboundPermitted);

    server.listen(webPort);

    log.info("Started web listener on webport : " + webPort);

    startedResult.setResult(null);
  }
}
