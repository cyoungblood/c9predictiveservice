package com.c9.predictive.helpers;

import io.vertx.rxcore.java.eventbus.RxMessage;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Func1;


/**
 * Created by yngbldjr on 4/25/14.
 */
public class RxEBUtil {
  public static Observable<JsonObject> raise(Observable<RxMessage<JsonObject>> original){
    return original

        .flatMap(new Func1<RxMessage<JsonObject>, Observable<JsonObject>>() {
      @Override
      public Observable<JsonObject> call(RxMessage<JsonObject> jsonObjectRxMessage) {
        return Observable.just(jsonObjectRxMessage.body());
      }
    }).onErrorReturn(new Func1<Throwable, JsonObject>() {
         @Override
         public JsonObject call(Throwable throwable) {
           throwable.printStackTrace();
           return new JsonObject().putString("status", "error").putString("errorMessage", throwable.getMessage());
         }
       });
  }
}
