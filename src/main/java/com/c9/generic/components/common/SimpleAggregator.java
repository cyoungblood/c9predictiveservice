package com.c9.generic.components.common;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.DefaultInputs;
import com.c9.predictive.util.Utils;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by yngbldjr on 1/17/15.
 */
public class SimpleAggregator extends DefaultBaseComponentVerticle {

  public static class SimpleAggregatorInputs extends DefaultInputs {
    public static final String TIMEOUT = "timeout";
  }

  public static SimpleAggregatorInputs INPUTS() {return new SimpleAggregatorInputs(); }

  public static class AggResults {
    public AtomicInteger count;
    public JsonArray arrays;

    public AtomicReference<JsonObject> mergedResults = new AtomicReference<>(new JsonObject());
    public AtomicLong timeoutTime = new AtomicLong(0L);

    public AggResults(JsonObject initResult, JsonArray mergedArrays){
      this.count = new AtomicInteger(1);
      this.arrays = mergedArrays;
      this.mergedResults.get().mergeIn(initResult);
    }

    public static JsonArray mergeArrays(JsonArray a1, JsonArray a2){

      List<JsonObject> addedKeys = new ArrayList<>();

      for (Object o : a1) {
        if (o instanceof JsonObject && !addedKeys.contains(o)) {
          addedKeys.add((JsonObject) o);
        }
      }
      for (Object o : a2) {
        if(o instanceof JsonObject){
          if(!addedKeys.contains(o)) {
            addedKeys.add((JsonObject) o);
            a1.add(o);
          }
        } else {
          a1.add(o);
        }
      }
      return a1;

    }

    public void mergeIn(JsonObject nextResult){
      synchronized (mergedResults) {
        JsonObject mergedObject = new JsonObject().mergeIn(mergedResults.get()).mergeIn(nextResult);
        for (Object arrayName : arrays) {
          mergedObject.putArray((String) arrayName, mergeArrays(mergedResults.get().getArray((String) arrayName, new JsonArray()), nextResult.getArray((String) arrayName, new JsonArray())));

        }
        mergedResults.set(mergedObject);
      }
      count.incrementAndGet();
    }

  }

  private ConcurrentHashMap<String, AggResults> agg = new ConcurrentHashMap<>();

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    final Long count = container.config().getLong("count", 2);

    final JsonArray mergedArrays = container.config().getArray("mergedArrays", new JsonArray());

    final String key = container.config().getString("key", Utils.PropertyConstants.CORRELATION_ID);
    // Default check every 30 min
    final Long timeoutDelayCheck = container.config().getLong("delayCheck", 1800000);
    // Clear if older than 1 hr.
    final Long timeoutDelayCleanup = container.config().getLong("timeoutAge", 3600000);

    input.port(SimpleAggregatorInputs.TIMEOUT).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject event) {
        String correlationId = event.getString(key);
        logger.debug("Timeout " + correlationId);
        if(agg.containsKey(correlationId)){
          agg.get(correlationId).timeoutTime.set(System.currentTimeMillis());
        }
      }
    });

    input.port(INPUTS().IN).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject event) {
        String keyValue = event.getString(key);
        if(!agg.containsKey(keyValue)){
          agg.put(keyValue, new AggResults(event, mergedArrays));
        } else if (agg.get(keyValue).timeoutTime.get() > 0L) {
          logger.debug("Ignoring result as this key has timed out will be cleaned up.");
          return;
        } else {
          agg.get(keyValue).mergeIn(event);
        }

        if(agg.get(keyValue).count.get() == count){
          JsonObject out = agg.get(keyValue).mergedResults.get();
          output.port(OUTPUTS().OUT).send(out);
          sendCompletionEmail(out);
          agg.remove(keyValue);
        } else {
          logger.debug("Not all messages received for " + keyValue + ".  Received " + agg.get(keyValue).count.get() + " of " + count);
          output.port(OUTPUTS().INFO_LOGGER).send(new JsonObject().putString("msg", "Not all messages received for " + keyValue + ".  Received " + agg.get(keyValue).count.get() + " of " + count));
        }
      }
    });

    vertx.setPeriodic(timeoutDelayCheck, new Handler<Long>() {
      @Override
      public void handle(Long event) {
        List<String> cleanupKeys = new ArrayList<>();
        logger.debug("Total keys in agg holder : " + agg.keySet().size());
        for(Map.Entry<String, AggResults> map : agg.entrySet()){

          if(map.getValue().timeoutTime.get() != 0L && (System.currentTimeMillis() - map.getValue().timeoutTime.get() > timeoutDelayCleanup)){
            cleanupKeys.add(map.getKey());
          }
        }
        for(String key : cleanupKeys){
          logger.debug("Removing key from timeout. " + key);
          agg.remove(key);
        }
      }
    });
  }

  private void sendCompletionEmail(JsonObject event) {

    String partitionId = event.getString(Utils.PropertyConstants.PARTITION_ID);
    String customer = event.getString(Utils.PropertyConstants.CUSTOMER);

    boolean hasErrors = event.getArray("errors", new JsonArray()).size() > 0;
    String emailTitle = hasErrors ? "Error with Extraction or Score process for %s [%s]" : "Extraction and/or Score process completed for %s [%s]";
    output.port(OUTPUTS().SEND_EMAIL).send(createEmailObject(event, String.format(emailTitle, customer, partitionId), "CompletionEmail", hasErrors));
  }

}
