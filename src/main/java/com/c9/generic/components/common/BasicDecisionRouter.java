package com.c9.generic.components.common;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 6/10/14.
 */
public class BasicDecisionRouter extends DefaultBaseComponentVerticle{

  public static YesNoOutputs OUTPUTS(){ return new YesNoOutputs(); }

  public static final String BOOLEAN_FIELD_NAME = "booleanFieldName";
  public static final String OBJECT_FIELD_NAME = "objectFieldName";

  protected String booleanFieldName;
  protected String objectFieldName;

  Logger log = LoggerFactory.getLogger(BasicDecisionRouter.class.getName());

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    booleanFieldName = container.config().getString(BOOLEAN_FIELD_NAME);
    objectFieldName = container.config().getString(OBJECT_FIELD_NAME);

    if(booleanFieldName == null){
      Exception e = new IllegalArgumentException("booleanFieldName is required to make a decision on.");
      log.error("Failed to start component.", e);
      startResult.setFailure(e);
    }


    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();

    if(objectFieldName != null && input.getObject(objectFieldName) == null){
      errors.put(objectFieldName, objectFieldName + " is required for this decision.");
    }

    return errors;
  }

  @Override
  public void processInput(JsonObject input) {
    if (input.getObject(objectFieldName, new JsonObject()).getBoolean(booleanFieldName, false) || input.getBoolean(booleanFieldName, false)) {
      output.port(BasicDecisionRouter.OUTPUTS().YES).send(input);
    } else {
      output.port(BasicDecisionRouter.OUTPUTS().NO).send(input);
    }
    output.port(BasicDecisionRouter.OUTPUTS().OUT).send(input);
  }
}
