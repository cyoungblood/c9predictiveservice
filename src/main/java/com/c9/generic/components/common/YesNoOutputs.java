package com.c9.generic.components.common;

import com.c9.generic.components.UpdateStatsOutputs;

/**
 * Created by yngbldjr on 5/27/14.
 */
public class YesNoOutputs extends UpdateStatsOutputs {
  // Default outputs
  public static final String YES = "yes";
  public static final String NO = "no";
}
