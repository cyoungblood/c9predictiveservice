package com.c9.generic.components;

import com.c9.predictive.util.Utils;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 4/30/14.
 */
public abstract class DefaultBaseComponentVerticle extends BaseComponentVerticle {

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    input.port(BaseComponentVerticle.INPUTS().IN).messageHandler(inputHandler());
  }

  Logger log = LoggerFactory.getLogger(DefaultBaseComponentVerticle.class);

  public Handler<JsonObject> inputHandler () {
    return new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject input) {
        Map<String, String> errors = validateInput(input);
        if(errors.isEmpty()){
          try {
            processInput(input);
          } catch (Exception e){
            log.error("Error processing input : " + e);
            log.error("Details : " + buildException(e));
            output.port(BaseComponentVerticle.OUTPUTS().ERROR_OUT).send(input.mergeIn(buildException(e)));
          }
        }
        else {
          JsonObject errorJson = buildInputErrors(errors);
          log.error("Validation failed for component : " + errorJson.getObject("errors") + " with address : " + errorJson.getString("address"));
          output.port(BaseComponentVerticle.OUTPUTS().ERROR_OUT).send(input.mergeIn(errorJson));
        }
      }
    };
  }

  protected JsonObject addFileLink(JsonObject input, String title, String file){
    JsonArray files = input.getArray("links", new JsonArray());

    JsonObject link = new JsonObject();
    link.putString("title", title);
    link.putString("href", "viewFile?file="+file);

    files.add(link);

    input.putArray("links", files);

    return input;
  }

  protected JsonObject buildInputErrors(Map<String, String> errors){
    JsonObject errorJson = new JsonObject();
    errorJson.putString("status", "validation_error");
    errorJson.putString("message", "component failed to validate input");

    JsonObject errorList = new JsonObject();
    for(Map.Entry<String, String> err : errors.entrySet()){
      errorList.putString(err.getKey(), err.getValue());
    }

    errorJson.putObject("errors", errorList);
    errorJson.putString("address", context.address());
    errorJson.putString("uri", context.uri());
    return errorJson;
  }

  protected JsonObject buildException(Throwable e){
    JsonObject errorJson = new JsonObject();
    errorJson.putString("status", "exception");
    errorJson.putString("message", "" + e.getMessage());
    errorJson.putString("stacktrace", "" + Throwables.getStackTraceAsString(e));
    errorJson.putString("address", context.address());
    errorJson.putString("uri", context.uri());
    return errorJson;
  }

  protected JsonObject createErrorEmail(JsonObject input, String subject) {
    if(subject == null)
      subject = "Error while Processing " + input.getString(Utils.PropertyConstants.CUSTOMER) + " [" + input.getString(Utils.PropertyConstants.PARTITION_ID) + "]";

    return createEmailObject(input, subject, "Error", true);
  }

  protected JsonObject createErrorEmail(JsonObject input) {
    return createErrorEmail(input, null);
  }

  protected JsonObject createEmailObject(JsonObject input, String titleAndSubject, String template, boolean isError) {

    addCompletionTiming(input);

    input.putBoolean("hasErrors", input.getArray("errors", new JsonArray()).size()>0);
    input.putBoolean("hasInfo", input.getArray("infoMessages", new JsonArray()).size()>0);

    JsonObject email = new JsonObject();

    email.putString("subject", titleAndSubject);
    email.putString("template", template);
    email.putBoolean("isError", isError);
    email.putObject("templateModel", new JsonObject().putString("title", titleAndSubject).mergeIn(input));

    return email;
  }

  private void addCompletionTiming(JsonObject event) {

    Long start = event.getLong("start");
    Long end = System.currentTimeMillis();

    event.putNumber("end", end).putString("endDate", formatTime(end));

    if(start != null){
      String elapsed = formatElapsedTime(end - start);
      event.putString("elapsed", elapsed);
    }
  }

  public Map<String, String> validateInput(JsonObject input){ return new HashMap<>(); }
  public void processInput(JsonObject input){}
}
