package com.c9.generic.components;

import com.google.common.base.Throwables;
import net.kuujo.vertigo.java.ComponentVerticle;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by yngbldjr on 4/30/14.
 */
public abstract class BaseComponentVerticle extends ComponentVerticle {

  public static DefaultInputs INPUTS(){
    return new DefaultInputs();
  }

  public static DefaultOutputs OUTPUTS() {
    return new DefaultOutputs();
  }

  public JsonObject addErrors(JsonObject input, String error) {
    return addErrors(input, error, null);
  }

  public JsonObject addErrors(JsonObject input, String error, Throwable e) {
    input.putString("status", "error");
    JsonObject err = new JsonObject();
    err.putString("message", error);
    if(e != null){
      err.putString("stacktrace", Throwables.getStackTraceAsString(e));
    }
    input.putArray("errors", input.getArray("errors", new JsonArray()).add(err));
    return input;
  }

  public JsonObject addInfo(JsonObject input, String error) {
    return addInfo(input, error, null);
  }

  public JsonObject addInfo(JsonObject input, String error, Throwable e) {
    input.putString("status", "error");
    JsonObject err = new JsonObject();
    err.putString("message", error);
    if(e != null){
      err.putString("stacktrace", Throwables.getStackTraceAsString(e));
    }
    input.putArray("infoMessages", input.getArray("infoMessages", new JsonArray()).add(err));
    return input;
  }

  public String errorsToString(JsonObject input){
    String errors = "";
    for(Object eo : input.getArray("errors", new JsonArray())){
      errors += eo  + "\n";
    }
    return errors;
  }
  public static String formatElapsedTime(Long elapsed){
    if(elapsed==null){
      return "";
    }
    PeriodFormatter formatter = new PeriodFormatterBuilder()
        .appendHours()
        .appendSuffix(" hour", " hours")
        .appendSeparator(" and ")
        .appendMinutes()
        .appendSuffix(" minute", " minutes")
        .appendSeparator(" and ")
        .appendSeconds()
        .appendSuffix(" second", " seconds")
        .appendSeparator(" ")
        .appendMillis()
        .appendSuffix(" millisecond", " milliseconds")
        .toFormatter();

    return formatter.print(new Period(elapsed.longValue()));
  }
  final static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY HH:mm:ss");

  public static String formatTime(Long time){
    return dateFormat.format(new java.util.Date(time));
  }

  public static JsonObject addTimer(JsonObject input, String timerName, Long start, List<String> messages){
    JsonObject timer = new JsonObject();
    timer.putString("name", timerName);
    timer.putNumber("start", start);
    Long end = System.currentTimeMillis();
    timer.putNumber("end", end);
    timer.putString("elapsed", formatElapsedTime(end - start));
    if(messages != null){
      JsonArray messagesArray = new JsonArray();
      for(String m : messages){
        messagesArray.add(m);
      }
      timer.putArray("messages", messagesArray);
    }
    return input.putArray("timers", input.getArray("timers", new JsonArray()).add(timer));
  }

  public static JsonObject addTimer(JsonObject input, String timerName, Long start){
    return addTimer(input, timerName, start, null);
  }
}