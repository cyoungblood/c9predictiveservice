package com.c9.generic.components;

import com.c9.generic.components.common.YesNoOutputs;

/**
 * Created by yngbldjr on 7/11/14.
 */
public class TimeoutOutputs extends YesNoOutputs {
  public static final String TIMEOUT = "timeout";
}
