package com.c9.generic.components.csv.convert;

import au.com.bytecode.opencsv.CSVReader;
import com.c9.predictive.util.Utils;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;
import org.yaml.snakeyaml.resolver.Resolver;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 6/27/14.
 */
public class ConvertCSVToYaml {


  //Simple class to create the base predictive_service models based on existing csv files maintained by the DS team.
  public static void main(String... args) throws Exception {

    updateOrGenerateYaml("full_extract", "/Users/yngbldjr/csv/baseExtractList.csv", "/cloud9/Perforce/Services/nextgen/predictive2/test/%s/predictive_service.yaml");
    updateOrGenerateYaml("open_extract", "/Users/yngbldjr/newOpenOppsExtractList.csv", "/cloud9/Perforce/Services/nextgen/predictive2/test/%s/predictive_service.yaml");

  }


  private static void updateOrGenerateYaml(String yamlField, String csv, String configFormat) throws Exception {
    updateOrGenerateYaml(yamlField, csv, configFormat, false);
  }


  private static void updateOrGenerateYaml(String yamlField, String csv, String configFormat, boolean overwriteExisting) throws Exception {

    CSVReader reader = new CSVReader(new FileReader(csv), '\t');

    String [] nextLine;

    while ((nextLine = reader.readNext()) != null) {
      int queryCount = 0;
      Map<String, Object> predictiveServiceConfig = new HashMap<>();
      Map<String, Object> tables = new HashMap<>();

      String customer = nextLine[0];
      String configFileName = String.format(configFormat, customer);
      File configFile = new File(configFileName);
      configFile.getParentFile().mkdirs();
      if(!configFile.exists() || overwriteExisting){
        configFile.createNewFile();
      } else {
        predictiveServiceConfig = parse(configFileName);
      }

      String partitionId = nextLine[2];



      System.out.println(customer + "[" + partitionId + "]");
      int cell = 3;

      while(true){
        try {
          Map<String, Object> tableObject = new HashMap<>();
          queryCount++;

          String idSql = nextLine[cell++];
          if(idSql == null || idSql.trim().length() == 0){
            break;
          }

          //idSql.replaceAll("'", "''");


          int fromIndex = idSql.indexOf("From ads.") + "From ads.".length();

          String tableStart = idSql.substring(fromIndex);
          int spaceIndex = tableStart.indexOf(" ");
          String tableName = "unknown";
          if(spaceIndex == -1){
            tableName = idSql.substring(fromIndex);
          }
          else {
            tableName = idSql.substring(fromIndex, fromIndex+spaceIndex);
          }



          if(idSql.contains("From security.UserRole")){
            tableName = "UserRole";
          }
          else if(idSql.contains("From security.User")){
            tableName = "User";
          }

          String templateSql = nextLine[cell++].trim();

          templateSql = templateSql.replaceAll("in XXX", "in ('{id_list}')");

          tableObject.put("idSQL", idSql);
          tableObject.put("templateSQL", templateSql);

          tables.put(tableName, tableObject);

        } catch (ArrayIndexOutOfBoundsException e){

          break;
        }
      }

      Map<String, Object> openExtract = new HashMap<>();
      openExtract.put("tables", tables);
      predictiveServiceConfig.put(yamlField, openExtract);
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm");
      predictiveServiceConfig.put(yamlField + "_generated_date", sdf.format(new Date()));
      predictiveServiceConfig.put(Utils.PropertyConstants.PARTITION_ID, partitionId);
      predictiveServiceConfig.put(Utils.PropertyConstants.CUSTOMER, customer);


      DumperOptions options = new DumperOptions();
      options.setPrettyFlow(true);
      options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
      options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
      options.setExplicitStart(false);
      options.setExplicitEnd(false);
      options.setWidth(1000);
      Yaml yaml = new Yaml(new Constructor(), new Representer(), options, new CustomResolver());

      String configString = yaml.dump(predictiveServiceConfig);

      System.out.println(configString);

      FileWriter fw = new FileWriter(configFile);

      fw.write(configString);

      fw.close();
    }
  }

  private static Map<String, Object> parse(String yamlFile) throws FileNotFoundException {
    Yaml yaml = new Yaml(new Constructor(), new Representer(), new DumperOptions(), new CustomResolver());
    return (Map<String, Object>) yaml.load(new FileInputStream(new File(yamlFile)));
  }

  // Use custom resolver.
  // This ensures that timestamps/dates are not automatically converted as Date is not support in the JsonObject
  public static class CustomResolver extends Resolver {
    @Override
    protected void addImplicitResolvers() {
      addImplicitResolver(Tag.BOOL, BOOL, "yYnNtTfFoO");
      addImplicitResolver(Tag.FLOAT, FLOAT, "-+0123456789.");
      addImplicitResolver(Tag.INT, INT, "-+0123456789");
      addImplicitResolver(Tag.MERGE, MERGE, "<");
      addImplicitResolver(Tag.NULL, NULL, "~nN\0");
      addImplicitResolver(Tag.NULL, EMPTY, null);
      // addImplicitResolver(Tags.TIMESTAMP, TIMESTAMP, "0123456789");
      addImplicitResolver(Tag.VALUE, VALUE, "=");
    }
  }
}
