package com.c9.generic.components;

/**
 * Created by yngbldjr on 5/27/14.
 */
public class DefaultOutputs {
  // Default outputs
  public static final String OUT = "out";
  public static final String ERROR_OUT = "error_out";
  public static final String SEND_EMAIL = "email_start";

  // Special Logger outputs - automatically on components that log
  public static final String ERROR_LOGGER = "error";
  public static final String INFO_LOGGER = "info";
  public static final String DEBUG_LOGGER = "debug";
}
