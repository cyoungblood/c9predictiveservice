package com.c9.generic.components.eventbus;

import com.c9.generic.components.BaseComponentVerticle;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 4/30/14.
 */
public class EventBusListenerComponent extends BaseComponentVerticle {

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    String addressToWatch = container.config().getString("eventbus.address");

    vertx.eventBus().registerHandler(addressToWatch, handler());

    startResult.setResult(null);
  }


  protected Handler<Message<JsonObject>> handler(){
    return new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> event) {
        output.port(OUTPUTS().OUT).send(event.body());
      }
    };
  }

}
