package com.c9.generic.components.eventbus;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by yngbldjr on 4/26/14.
 */
public class EventBusSenderComponent extends DefaultBaseComponentVerticle {

  private static final Long DEFAULT_EB_TIMEOUT = 5000L;

  private String eventbusAddress;

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    eventbusAddress = container.config().getString("eventbus.address");

    input.port(DefaultBaseComponentVerticle.INPUTS().IN_ERROR).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject event) {
        logger.error("IN FROM ERROR_OUT : ");
        processInput(event);
      }
    });

    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {
    Map<String, String> errors = new HashMap<>();
    if(eventbusAddress == null && input.getString("eventbus.address") == null){
      errors.put("eventbus.address", "eventbus.address field is required input for this component.");
    }
    return errors;
  }

  @Override
  public void processInput(final JsonObject input) {
    String address = eventbusAddress == null ? input.getString("eventbus.address") : eventbusAddress;
    Long timeout = DEFAULT_EB_TIMEOUT;
    if(input.getNumber("timeout") != null){
      timeout = input.getNumber("timeout").longValue();
    }

    vertx.eventBus().sendWithTimeout(address , input.getObject("msg", input), timeout, new Handler<AsyncResult<Message<JsonObject>>>() {
      @Override
      public void handle(AsyncResult<Message<JsonObject>> event) {
        if(event.succeeded()) {
          output.port(EventBusSenderComponent.OUTPUTS().OUT).send(input.mergeIn(event.result().body()));
        } else {
          output.port(EventBusSenderComponent.OUTPUTS().ERROR_OUT).send(input.mergeIn(event.result().body()));
        }
      }
    });
  }
}
