package com.c9.generic.components.data;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.TimeoutOutputs;
import com.c9.predictive.helpers.RxEBUtil;
import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.vertx.java.core.Future;
import org.vertx.java.core.json.JsonObject;
import rx.Observable;
import rx.util.functions.Action1;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yngbldjr on 5/26/14.
 */
public class HasKeyInClusterMap extends DefaultBaseComponentVerticle {


//  public static class T extends DefaultOutputs{
//    public static final String YES = "yes";
//    public static final String NO = "no";
//  }

  public static TimeoutOutputs OUTPUTS() {
    return new TimeoutOutputs();
  }

  protected String sharedDataCluster;
  protected String key;
  protected String mapName;
  protected RxEventBus rxeb;

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    this.rxeb = new RxEventBus(vertx.eventBus());

    this.key = this.container.config().getString("key");
    this.mapName = this.container.config().getString("mapName");
    this.sharedDataCluster = this.container.config().getString("sharedDataCluster");

    if(key == null){
      startResult.setFailure(new IllegalArgumentException("Missing config parameter 'key'"));
      return;
    }
    if(mapName == null){
      startResult.setFailure(new IllegalArgumentException("Missing config parameter 'mapName'"));
      return;
    }
    if(sharedDataCluster == null){
      startResult.setFailure(new IllegalArgumentException("Missing config parameter 'sharedDataCluster'"));
      return;
    }

    startResult.setResult(null);
  }

  @Override
  public Map<String, String> validateInput(JsonObject input) {

    Map<String, String> errors = new HashMap<>();
    if(input.getString(key) == null){
      errors.put(key, key + " Is required to lookup value in map.");
    }

    return errors;
  }

  @Override
  public void processInput(final JsonObject input) {

    final String keyId = input.getString(key);

    JsonObject lookupKey = new JsonObject().putString("type", "map").putString("name", input.getString("mapName" , mapName)).putString("action", "get").putString("key", keyId);

    Observable<RxMessage<JsonObject>> keyLookupObs = rxeb.send(sharedDataCluster, lookupKey);

    RxEBUtil.raise(keyLookupObs).subscribe(new Action1<JsonObject>() {
      @Override
      public void call(JsonObject jsonObject) {
        if("ok".equalsIgnoreCase(jsonObject.getString("status"))) {
          String value = jsonObject.getString("result");
          if (value != null) {
            logger.info(keyId + " exists in map.");
            output.port(OUTPUTS().YES).send(input.putString("mapValue", value));
          } else {
            logger.info("map key not in from cluster.");
            output.port(OUTPUTS().NO).send(input);
          }
        }
        else {
          addErrors(input, "Failed to access map in cluster.");
          output.port(OUTPUTS().ERROR_OUT).send(input);
        }
      }
    });
  }
}
