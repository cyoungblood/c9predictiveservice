package test.com.c9.predictive.unit;

import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import com.c9.predictive.components.SqlExecutorComponent;
import com.c9.predictive.components.sfdcupload.UploadScoresToSF;
import com.c9.predictive.util.Utils;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import static org.vertx.testtools.VertxAssert.*;

/**
 * Created by yngbldjr on 7/1/14.
 */
public class UploadSFDCComponent extends TestVerticle {

  @Test
  public void testUploadToSFComponent() throws Exception {

    vertx.eventBus().registerHandler("kick_uploadSF.complete", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        container.logger().info("Done with : " + message.body());
        testComplete();
      }
    });

    System.setProperty("vertx.test.timeout", "3000000");
    
    final Vertigo vertigo = new Vertigo(this);

    // Deploying DataServices module - used to make all the SQL calls to the ADS
    final JsonObject dataServicesConfig = new JsonObject();
    dataServicesConfig.putString("address", "jdbc");
    dataServicesConfig.putString("macURL", "jdbc:certive://mac1.prod.cloud9analytics.com:5432/NW;SSL=false;username='su';password='password'");
    dataServicesConfig.putString("base.loader.dir.format", "/cloud9/loader/C9Dev_00DG0000000hTr8MAE/00DG0000000hTr8MAE");


    container.deployWorkerVerticle(SqlExecutorComponent.class.getName(), dataServicesConfig, 4, true, new Handler<AsyncResult<String>>() {
      @Override
      public void handle(AsyncResult<String> stringAsyncResult) {
        if(stringAsyncResult.succeeded()){


          vertigo.deployCluster("testUpload", new Handler<AsyncResult<Cluster>>() {

            @Override
            public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {

              final NetworkConfig uploadSF = vertigo.createNetwork("test");

              uploadSF.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "kick_uploadSF"));
              uploadSF.addComponent("uploadSF", UploadScoresToSF.class.getName(), dataServicesConfig);
              uploadSF.addComponent("end", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "kick_uploadSF.complete"));


              uploadSF.createConnection("kick", EventBusListenerComponent.OUTPUTS().OUT, "uploadSF", UploadScoresToSF.INPUTS().IN);
              uploadSF.createConnection("uploadSF", UploadScoresToSF.OUTPUTS().OUT, "end", UploadScoresToSF.INPUTS().IN);
              uploadSF.createConnection("uploadSF", UploadScoresToSF.OUTPUTS().SEND_EMAIL, "end", EventBusSenderComponent.INPUTS().IN);


              clusterManagerAsyncResult.result().deployNetwork(uploadSF, new Handler<AsyncResult<ActiveNetwork>>() {
                @Override
                public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
                  assertTrue(activeNetworkAsyncResult.succeeded());

                  String path = this.getClass().getResource("/OpportunityScoresV3_5.5.2.csv").getPath();

                  //Either pass userName/password OR pass a loaderDir that has c9.properties and c9_encryption.key
                  vertx.eventBus().send("kick_uploadSF", new JsonObject()
                      .putString(Utils.PropertyConstants.ANALYTIC_HOSTNAME, "dev1-analytic1.engr.cloud9analytics.com")
                      .putString(Utils.PropertyConstants.PARTITION_ID, "00DG0000000hTr8MAE")
//                      .putString("userName", "joe.dunbar@c9dev.com")
//                      .putString("password", "password9")
                      .putString("scoreFileLocation", path));
                  }
              });
            }

          });
        } else {
          stringAsyncResult.cause().printStackTrace();
          fail("Could not load DataServices Module");
        }
        }

    });




  }
}
