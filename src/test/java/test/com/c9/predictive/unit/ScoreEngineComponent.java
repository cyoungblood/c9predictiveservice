package test.com.c9.predictive.unit;

import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import com.c9.predictive.components.datascience.PythonScriptComponent;
import com.c9.predictive.util.Utils;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.joda.time.DateTime;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import static org.vertx.testtools.VertxAssert.*;

/**
 * Created by yngbldjr on 6/9/14.
 */
public class ScoreEngineComponent extends TestVerticle {

  @Test
  public void testScoreEngineWithErrors() throws Exception {
    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("scoreEngineWithErrors", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
        NetworkConfig scoreNetwork = vertigo.createNetwork("scoreexecutor");

        String confFile = "config/config-local.json";
        if(System.getProperty("conf") != null){
          confFile = System.getProperty("conf");
        }

        Buffer buffer = vertx.fileSystem().readFileSync(confFile);

        final JsonObject config =  new JsonObject(buffer.toString());

        scoreNetwork.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "jmsListen"), 1);
        scoreNetwork.addComponent("score", PythonScriptComponent.class.getName(), config);
        scoreNetwork.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "jmscomplete"), 1);

        scoreNetwork.createConnection("kick", EventBusListenerComponent.OUTPUTS().OUT, "score", PythonScriptComponent.INPUTS().IN);
        scoreNetwork.createConnection("score", PythonScriptComponent.OUTPUTS().OUT, "ebdone", EventBusSenderComponent.INPUTS().IN);
        scoreNetwork.createConnection("score", PythonScriptComponent.OUTPUTS().ERROR_OUT, "ebdone", EventBusSenderComponent.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(scoreNetwork, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            // IP to start the score engine Component
            try {

//              timeoutTimestamp
              DateTime dt = DateTime.now().plusMillis(300000);

              Long timeoutTimestamp = dt.getMillis();

              vertx.eventBus().send("jmsListen", new JsonObject()
                                                    .putString(Utils.PropertyConstants.CUSTOMER, "Cloud9")
                                                    .putString(Utils.PropertyConstants.PARTITION_ID, "partitionid")
                                                    .putNumber("timeoutTimestamp", timeoutTimestamp)
                                                    .putString("dateSuffix", "26-08-2014")
                                                    .putObject("config", new JsonObject()));
            } catch (Exception e) {
              e.printStackTrace();
              fail();
            }
          }
        });
      }
    });

    vertx.eventBus().registerHandler("jmscomplete", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        assertEquals("Status should be error", "error", message.body().getString("status"));
        assertTrue("Message should have an errors array", message.body().getArray("errors") != null);
        testComplete();
      }
    });
  }
}
