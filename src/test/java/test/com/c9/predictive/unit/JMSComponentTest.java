package test.com.c9.predictive.unit;

import com.c9.predictive.util.Utils;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.joda.time.DateTime;
import org.junit.Test;

import javax.jms.*;
import java.util.UUID;

/**
 * Created by yngbldjr on 5/11/14.
 */
public class JMSComponentTest {

  @Test
  public void testTimeout() throws Exception {
    System.out.println(DateTime.now().getMillis());
    System.out.println(DateTime.now().plusMillis(60000000).getMillis());


    System.out.println(System.currentTimeMillis());
    System.out.println(System.currentTimeMillis() + 60000000);



  }

  @Test
  public void consumeFromTest() throws Exception{
    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://recorder1.prod.cloud9analytics.com:61616");

    // Create a Connection
    Connection connection = connectionFactory.createConnection();
    connection.start();

    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

//    Destination destination = session.createQueue(queue);
  }

  @Test
  public void testJMS() throws Exception{
    sendJMSMessageToQueue("C9.predictive.command.queue", "00D300000000DieEAE", "qen-analytic1.engr.cloud9analytics.com", UUID.randomUUID().toString(), 60 * 1000 * 1000L);
  }

  protected String sendJMSMessageToQueue(String queue, String message, String hostname, String correlationId, Long maxWaitTime) throws Exception {
    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

    // Create a Connection
    Connection connection = connectionFactory.createConnection();
    connection.start();

    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

    Destination destination = session.createQueue(queue);

    TextMessage msg = session.createTextMessage();
    msg.setText(message);
    msg.setJMSCorrelationID(correlationId);
    msg.setStringProperty(Utils.PropertyConstants.ANALYTIC_HOSTNAME, hostname);
    msg.setStringProperty("servicename", "fromtest");
    msg.setLongProperty("predictivemaximumwaittime", maxWaitTime);
    msg.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);

    session.createProducer(destination).send(msg);

    session.close();

    // Cleanup;
    session = null;
    destination = null;

    connection.stop();
    connection.close();
    connection = null;

    connectionFactory = null;

    return correlationId;
  }

//  @Test
//  public void testJMSListenerComponent() throws Exception {
//    final Vertigo vertigo = new Vertigo(this);
//
//    vertigo.deployCluster("testJMSListen", new Handler<AsyncResult<Cluster>>() {
//      @Override
//      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
//        NetworkConfig jmsNetwork = vertigo.createNetwork("jms.network");
//
//        //Listen to C9.predictive.command.queue jms queue
//        jmsNetwork.addComponent("kick", JMSListenComponent.class.getName(), new JsonObject().putString("jms.listen.queue", "C9.predictive.wait.queue"), 1);
//        jmsNetwork.addComponent("done", ComponentComplete.class.getName());
//        jmsNetwork.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "jmscomplete"), 1);
//
//        jmsNetwork.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
//        jmsNetwork.createConnection("done", DefaultBaseComponentVerticle.OUTPUTS().OUT, "ebdone", DefaultBaseComponentVerticle.INPUTS().IN);
//
//        clusterManagerAsyncResult.result().deployNetwork(jmsNetwork, new Handler<AsyncResult<ActiveNetwork>>() {
//          @Override
//          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
//            assertTrue(activeNetworkAsyncResult.succeeded());
//            // IP to start the yaml Component
//            try {
//              //sendJMSMessageToQueue("C9.predictive.command.queue", "testpartitionid", "localhost", UUID.randomUUID().toString());
//            } catch (Exception e) {
//              e.printStackTrace();
//              fail();
//            }
//          }
//        });
//      }
//    });
//
//    vertx.eventBus().registerHandler("jmscomplete", new Handler<Message<JsonObject>>() {
//      @Override
//      public void handle(Message<JsonObject> message) {
//        assertNotNull(message.body().getString(Utils.PropertyConstants.PARTITION_ID));
//        testComplete();
//      }
//    });
//  }
}
