package test.com.c9.predictive.unit;

import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import com.c9.predictive.util.Utils;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static org.vertx.testtools.VertxAssert.*;

/**
 * Created by yngbldjr on 7/1/14.
 */
public class FolderCleanupComponent extends TestVerticle {

  @Test
  public void testFolderCleanup() throws Exception {

    final String dataExtractionFormat = "/tmp/%s/";
    final String customerFolder = String.format(dataExtractionFormat, "customer1");

    File baseDir = new File(customerFolder);

    if(!baseDir.exists()){
      baseDir.mkdirs();
    } else {
      delDir(Paths.get(customerFolder));
      baseDir.mkdirs();
    }

    vertx.fileSystem().writeFileSync(customerFolder + "test.txt", new Buffer("test"));
    vertx.fileSystem().writeFileSync(customerFolder + "test1.txt", new Buffer("test1"));

    final JsonObject input = new JsonObject();
    input.putString(Utils.PropertyConstants.CUSTOMER, "customer1");

    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("testFolderLocation", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
        NetworkConfig jmsNetwork = vertigo.createNetwork("folder.location.network");

        jmsNetwork.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "folder_kick"));
        jmsNetwork.addComponent("folderCleanup", com.c9.predictive.components.FolderCleanupComponent.class.getName());
        jmsNetwork.addComponent("done", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "folder_complete"));

        jmsNetwork.createConnection("kick", EventBusListenerComponent.OUTPUTS().OUT, "folderCleanup", com.c9.predictive.components.FolderCleanupComponent.INPUTS().IN);
        jmsNetwork.createConnection("folderCleanup", com.c9.predictive.components.FolderCleanupComponent.OUTPUTS().OUT, "done", com.c9.predictive.components.FolderCleanupComponent.INPUTS().IN);
        jmsNetwork.createConnection("folderCleanup", com.c9.predictive.components.FolderCleanupComponent.OUTPUTS().ERROR_OUT, "done", com.c9.predictive.components.FolderCleanupComponent.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(jmsNetwork, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            vertx.eventBus().send("folder_kick", input);
          }
        });
      }
    });

    vertx.eventBus().registerHandler("folder_complete", new Handler<Message>() {
      @Override
      public void handle(Message message) {
        assertTrue("No Files should exist", !(new File(customerFolder + "test.txt")).exists());
        assertTrue("No Files should exist", !(new File(customerFolder + "test1.txt")).exists());
        assertTrue("The dir should still be there.", (new File(customerFolder)).exists());
        try {
          delDir(Paths.get(customerFolder));
        } catch (IOException e) {
          e.printStackTrace();
          fail();
        }
        testComplete();
      }
    });
  }

  public static void delDir(Path directory) throws IOException {
    Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (file == null)
          return FileVisitResult.TERMINATE;

        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (dir == null)
          return FileVisitResult.TERMINATE;

        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }

    });
  }
}
