package test.com.c9.predictive.unit;

import com.c9.generic.components.BaseComponentVerticle;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonObject;

/**
 * Created by yngbldjr on 7/1/14.
 */
public class ComponentComplete extends BaseComponentVerticle {

  @Override
  public void start(Future<Void> startResult) {
    super.start(startResult);

    input.port(INPUTS().IN).messageHandler(new Handler<JsonObject>() {
      @Override
      public void handle(JsonObject o) {
        logger.info(o);
        output.port(OUTPUTS().OUT).send(new JsonObject().putString("address", "scoreComplete").putObject("msg", o));
      }
    });

    startResult.setResult(null);
  }
}
