package test.com.c9.predictive.unit;

import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.io.batch.InputBatch;
import net.kuujo.vertigo.io.batch.OutputBatch;
import net.kuujo.vertigo.java.ComponentVerticle;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.vertx.testtools.VertxAssert.assertTrue;
import static org.vertx.testtools.VertxAssert.testComplete;

/**
 * Created by yngbldjr on 6/11/14.
 */
public class BatchTesting extends TestVerticle {

  @Test
  public void GroupTest() {
    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("groups", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
        NetworkConfig groupNetwork = vertigo.createNetwork("g");

        groupNetwork.addVerticle("ip", IPComponent.class.getName());
        groupNetwork.addVerticle("group", BatchConsumer.class.getName());

        groupNetwork.createConnection("ip", "out", "group", "in");

        clusterManagerAsyncResult.result().deployNetwork(groupNetwork, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            vertx.eventBus().send("gip", "1");
            vertx.eventBus().send("gip", "2");
          }
        });

      }
    });
  }

  public static class SQLToCSVComponent extends ComponentVerticle {
    @Override
    public void start(Future<Void> startResult) {
      super.start(startResult);

      vertx.eventBus().registerHandler("gip", new Handler<Message<String>>() {
        @Override
        public void handle(final Message<String> ebevent) {

          output.port("out").batch(new Handler<OutputBatch>() {
            @Override
            public void handle(OutputBatch event) {
              String uuid = UUID.randomUUID().toString();
              for(int i = 0;i<5;i++){
                JsonObject batch = new JsonObject();
                batch.putString("batchId", i+"");
                batch.putString("sql", "select 1");
                batch.putString("file", "file1.csv");
                batch.putString("uuid", ebevent.body());
                event.send(batch);
              }
              event.end();
            }
          });
        }
      });
      startResult.setResult(null);
    }

  }

  public static class IPComponent extends ComponentVerticle {

    @Override
    public void start(Future<Void> startResult) {
      super.start(startResult);

      vertx.eventBus().registerHandler("gip", new Handler<Message<String>>() {
        @Override
        public void handle(final Message<String> ebevent) {
          output.port("out").batch(new Handler<OutputBatch>() {
            @Override
            public void handle(OutputBatch event) {
              String uuid = UUID.randomUUID().toString();
              for(int i = 0;i<5;i++){
                JsonObject batch = new JsonObject();
                batch.putString("batchId", i+"");
                batch.putString("sql", "select 1");
                batch.putString("file", "file1.csv");
                batch.putString("uuid", ebevent.body());
                event.send(batch);
              }
              event.end();
            }
          });
        }
      });
      startResult.setResult(null);
    }
  }

  public static class BatchConsumer extends ComponentVerticle {
    @Override
    public void start(Future<Void> startResult) {
      super.start(startResult);
      final AtomicInteger count = new AtomicInteger(0);
      input.port("in").batchHandler(new Handler<InputBatch>() {
        @Override
        public void handle(InputBatch event) {

          event.messageHandler(new Handler<JsonObject>() {
            @Override
            public void handle(JsonObject event) {
              logger.info("Handle : " + event);
            }
          });

          event.startHandler(new Handler<Object>() {
            @Override
            public void handle(Object event) {
              logger.info("Started.");
            }
          });


          event.endHandler(new Handler<Object>() {
            @Override
            public void handle(Object event) {
              logger.info("Ending count : " + count.incrementAndGet());

              if(count.get() == 2){
                testComplete();
              }
            }
          });
        }
      });

      startResult.setResult(null);
    }
  }
}
