package test.com.c9.predictive.unit;

import com.c9.generic.components.BaseComponentVerticle;
import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import com.c9.predictive.components.*;
import com.c9.predictive.components.FolderCleanupComponent;
import com.c9.predictive.components.datascience.PythonScriptComponent;
import com.c9.predictive.util.Utils;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.hook.EventBusHook;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import test.com.c9.predictive.common.JMSTestVerticle;

import java.io.File;
import java.util.UUID;

import static org.vertx.testtools.VertxAssert.*;

/**
 * Created by yngbldjr on 5/11/14.
 */
public class ComponentTests extends JMSTestVerticle {

  public static class ComponentComplete extends BaseComponentVerticle {

    @Override
    public void start(Future<Void> startResult) {
      super.start(startResult);

      input.port(INPUTS().IN).messageHandler(new Handler<JsonObject>() {
        @Override
        public void handle(JsonObject o) {
          logger.info(o);
          output.port(OUTPUTS().OUT).send(new JsonObject().putString("address", "scoreComplete").putObject("msg", o));
        }
      });

      startResult.setResult(null);
    }
  }

  @Test
  public void testFolderCleanup() throws Exception {
    vertx.fileSystem().deleteSync("/tmp/comptest", true);
    vertx.fileSystem().mkdirSync("/tmp/comptest");
    vertx.fileSystem().writeFileSync("/tmp/comptest/test.txt", new Buffer("test"));
    vertx.fileSystem().writeFileSync("/tmp/comptest/test1.txt", new Buffer("test1"));

    String keepFile = "/tmp/comptest/test.txt";
    String dataLocation = "/tmp/comptest/";

    final JsonObject input = new JsonObject();
    input.putString("keepFile", keepFile);
    input.putString("dataLocation", dataLocation);

    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("testFolderLocation", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
        NetworkConfig jmsNetwork = vertigo.createNetwork("folder.location.network");

        jmsNetwork.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "folder_kick")).addHook(new EventBusHook());
        jmsNetwork.addComponent("folderCleanup", FolderCleanupComponent.class.getName()).addHook(new EventBusHook());
        jmsNetwork.addComponent("done", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "folder_complete")).addHook(new EventBusHook());

        jmsNetwork.createConnection("kick", EventBusListenerComponent.OUTPUTS().OUT, "folderCleanup", FolderCleanupComponent.INPUTS().IN);
        jmsNetwork.createConnection("folderCleanup", FolderCleanupComponent.OUTPUTS().OUT, "done", FolderCleanupComponent.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(jmsNetwork, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            vertx.eventBus().send("folder_kick", input);
          }
        });

//        folder_kick
      }
    });

    vertx.eventBus().registerHandler("folder_complete", new Handler<Message>() {
      @Override
      public void handle(Message message) {
        assertTrue("File should still Exisit", (new File("/tmp/comptest/test.txt")).exists());
        assertTrue("File should still Exisit", !(new File("/tmp/comptest/test1.txt")).exists());
        testComplete();
      }
    });
  }

  @Test
  public void testJMSListenerComponent() throws Exception {
    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("testJMSListen", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
        NetworkConfig jmsNetwork = vertigo.createNetwork("jms.network");

        //Listen to C9.predictive.command.queue jms queue
        jmsNetwork.addComponent("kick", JMSListenComponent.class.getName(), new JsonObject().putString("jms.listen.queue", "C9.predictive.command.queue"), 1);
        jmsNetwork.addComponent("done", ComponentComplete.class.getName());
        jmsNetwork.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "jmscomplete"), 1);

        jmsNetwork.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        jmsNetwork.createConnection("done", DefaultBaseComponentVerticle.OUTPUTS().OUT, "ebdone", DefaultBaseComponentVerticle.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(jmsNetwork, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            // IP to start the yaml Component
            try {
              sendJMSMessageToQueue("C9.predictive.command.queue", "testpartitionid", "localhost", UUID.randomUUID().toString());
            } catch (Exception e) {
              e.printStackTrace();
              fail();
            }
          }
        });
      }
    });

    vertx.eventBus().registerHandler("jmscomplete", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        assertNotNull(message.body().getString(Utils.PropertyConstants.PARTITION_ID));
        testComplete();
      }
    });
  }


  @Test
  public void testScoreExecutor() {

    vertx.eventBus().registerHandler("testScore.test.kick", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        container.logger().info("kick : " + message.body() + "\n");
      }
    });

    vertx.eventBus().registerHandler("testScore.test.test_component", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        container.logger().info("test_component : " + message.body() + "\n");
      }
    });

    final Vertigo vertigo = new Vertigo(this);
    vertigo.deployCluster("testScore", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {

        final NetworkConfig scoreExecutor = vertigo.createNetwork("test");

        scoreExecutor.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "scorekick")).addHook(new EventBusHook());
        scoreExecutor.addComponent("test_component", PythonScriptComponent.class.getName(), 1).addHook(new EventBusHook());
        scoreExecutor.addComponent("done", ComponentComplete.class.getName()).addHook(new EventBusHook());
        scoreExecutor.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "scoreComplete"), 1).addHook(new EventBusHook());

        scoreExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "test_component", DefaultBaseComponentVerticle.INPUTS().IN);
        scoreExecutor.createConnection("test_component", DefaultBaseComponentVerticle.OUTPUTS().OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        scoreExecutor.createConnection("test_component", DefaultBaseComponentVerticle.OUTPUTS().ERROR_OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        scoreExecutor.createConnection("done", DefaultBaseComponentVerticle.OUTPUTS().OUT, "ebdone", DefaultBaseComponentVerticle.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(scoreExecutor, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            vertx.eventBus().send("scorekick", new JsonObject().putString("CompanyName", "Cloud9"));
          }
        });
      }
    });

    vertx.eventBus().registerHandler("scoreComplete", new Handler<Message>() {
      @Override
      public void handle(Message message) {
        testComplete();
      }
    });
  }

  @Test
  public void testV2Yaml() {
    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("testScore", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
        NetworkConfig yamlExecutor = vertigo.createNetwork("yaml.network");

        yamlExecutor.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlkick"));

        yamlExecutor.addComponent("score", ParseYAMLComponent.class.getName(), 1);

        yamlExecutor.addComponent("done", ComponentComplete.class.getName());
        yamlExecutor.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlComplete"), 1);

        yamlExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "score", DefaultBaseComponentVerticle.INPUTS().IN);
        yamlExecutor.createConnection("score", DefaultBaseComponentVerticle.OUTPUTS().OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        yamlExecutor.createConnection("score", DefaultBaseComponentVerticle.OUTPUTS().ERROR_OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        yamlExecutor.createConnection("done", DefaultBaseComponentVerticle.OUTPUTS().OUT, "ebdone", DefaultBaseComponentVerticle.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(yamlExecutor, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            // IP to start the yaml Component
            vertx.eventBus().send("yamlkick", new JsonObject().putString("file", "/cloud9/Perforce/Services/nextgen/predictive2/test/Cloud9/config.yaml"));
          }
        });
      }
    });

    vertx.eventBus().registerHandler("yamlComplete", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        assertNotNull(message.body().getObject("config"));
        testComplete();
      }
    });
  }

  @Test
  public void testV3Parse() {
    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("testScore", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {



        NetworkConfig yamlExecutor = vertigo.createNetwork("yaml.network");

        yamlExecutor.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlkick"));

        yamlExecutor.addComponent("readConfig", ParseYAMLComponent.class.getName(), 1);
        yamlExecutor.addComponent("parseQueries", ParseQueriesFromConfigComponent.class.getName(), 1);
        yamlExecutor.addComponent("extract", ExtractDataComponent.class.getName(), new JsonObject().putObject("dataServices", new JsonObject().putString("address", "non.exist")), 1);

        yamlExecutor.addComponent("done", ComponentComplete.class.getName());
        yamlExecutor.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlComplete"), 1);

        yamlExecutor.createConnection("kick", EventBusListenerComponent.OUTPUTS().OUT, "readConfig", ParseYAMLComponent.INPUTS().IN);
        yamlExecutor.createConnection("readConfig", ParseYAMLComponent.OUTPUTS().OUT, "parseQueries", ParseQueriesFromConfigComponent.INPUTS().IN);
        yamlExecutor.createConnection("parseQueries", ParseQueriesFromConfigComponent.OUTPUTS().OUT, "extract", ComponentComplete.INPUTS().IN);
        yamlExecutor.createConnection("extract", ExtractDataComponent.OUTPUTS().OUT, "done", ComponentComplete.INPUTS().IN);
        yamlExecutor.createConnection("done", ComponentComplete.OUTPUTS().OUT, "ebdone", EventBusSenderComponent.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(yamlExecutor, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            // IP to start the yaml Component
            vertx.eventBus().send("yamlkick", new JsonObject().putString(Utils.PropertyConstants.PARTITION_ID, "anyThing").putString("file", "/cloud9/Perforce/Services/nextgen/predictive2/test/Cloud9/config.yaml"));
          }
        });
      }
    });

    vertx.eventBus().registerHandler("yamlComplete", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        assertNotNull(message.body().getObject("config"));
        testComplete();
      }
    });
  }
}
