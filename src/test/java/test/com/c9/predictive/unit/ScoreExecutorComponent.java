package test.com.c9.predictive.unit;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import com.c9.predictive.components.datascience.PythonScriptComponent;
import com.c9.predictive.util.Utils;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.hook.EventBusHook;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import static org.vertx.testtools.VertxAssert.assertTrue;
import static org.vertx.testtools.VertxAssert.testComplete;

/**
 * Created by yngbldjr on 7/1/14.
 */
public class ScoreExecutorComponent extends TestVerticle {

  @Test
  public void testScoreExecutor() {

    vertx.eventBus().registerHandler("testScore.test.kick", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        container.logger().info("kick : " + message.body() + "\n");
      }
    });

    vertx.eventBus().registerHandler("testScore.test.test_component", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        container.logger().info("test_component : " + message.body() + "\n");
      }
    });

    final Vertigo vertigo = new Vertigo(this);
    vertigo.deployCluster("testScore", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {

        final NetworkConfig scoreExecutor = vertigo.createNetwork("test");

        JsonObject scriptComponentConfig = new JsonObject();

        JsonObject scriptEngine = new JsonObject();


        scriptComponentConfig.putObject("scoreEngine", scriptEngine);


        JsonObject scoreConfigObject = createScoreObject(scriptComponentConfig);

        scoreExecutor.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "scorekick")).addHook(new EventBusHook());
        scoreExecutor.addComponent("test.component", PythonScriptComponent.class.getName(), scoreConfigObject, 1).addHook(new EventBusHook());
        scoreExecutor.addComponent("done", ComponentComplete.class.getName()).addHook(new EventBusHook());
        scoreExecutor.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "scoreComplete"), 1).addHook(new EventBusHook());

        scoreExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "test.component", DefaultBaseComponentVerticle.INPUTS().IN);
        scoreExecutor.createConnection("test.component", DefaultBaseComponentVerticle.OUTPUTS().OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        scoreExecutor.createConnection("test.component", DefaultBaseComponentVerticle.OUTPUTS().ERROR_OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        scoreExecutor.createConnection("done", DefaultBaseComponentVerticle.OUTPUTS().OUT, "ebdone", DefaultBaseComponentVerticle.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(scoreExecutor, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            vertx.eventBus().send("scorekick", new JsonObject().putString("CompanyName", "Cloud9"));
          }
        });
      }
    });

    vertx.eventBus().registerHandler("scoreComplete", new Handler<Message>() {
      @Override
      public void handle(Message message) {
        testComplete();
      }
    });
  }

//  "scoreEngine" :
//  {
//      "pythonExecutable" : "/cloud9/anaconda/bin/python",
//      "pythonScript" : "/cloud9/qen-predictive/predictive/probwin_fit_score.py",
//      "pythonDurationScript" : "/cloud9/qen-predictive/predictive/duration_fit_score.py",
//      "predictiveHome" : "/cloud9/qen-predictive/predictive/",
//      // Location of the wrapper script used to export needed env variables for the python process.
//      "pipelineWrapper" : "/cloud9/Perforce/Services/main/PredictiveService/run/pipeline_score.sh",
//      "predictiveBaseDir" : "/cloud9/Perforce/PredictiveConfig/main/",
//      "dataExtractLocationFormat" : "/cloud9/extract/%s_%s/open/",
//      "scoreFileName" : "OpportunityScoresV3",
//      "durationFileName" : "DurationModelScores",
//      "loggingConfig" : "/cloud9/qen-predictive/predictive/resources/logging.config",
//      "verboseLogging" : true
//  },

  private JsonObject createScoreObject(JsonObject networkConfig) {
    JsonObject scoreObject = new JsonObject().mergeIn(networkConfig);

    scoreObject.putString("defaultModel", "opp_score.model");
    scoreObject.putString("configProperty", "score_config");
    scoreObject.putString("modelProperty", "score_model");
    scoreObject.putString("scriptTitle", "Opportunity Score");
    scoreObject.putString("fileLocationOutProperty", Utils.PropertyConstants.SCORE_FILE_LOCATION);
    scoreObject.putString("resultOutProperty", "scoreResults");
    scoreObject.putString("customerLogName", "opp_score_");
    scoreObject.putString("verboseLoggingProperty", "score.verbose.logging");

    return scoreObject;
  }
}
