package test.com.c9.predictive.unit;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.common.SimpleAggregator;
import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.component.ComponentConfig;
import net.kuujo.vertigo.hook.ComponentHook;
import net.kuujo.vertigo.hook.EventBusHook;
import net.kuujo.vertigo.io.connection.ConnectionConfig;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Future;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.vertx.testtools.VertxAssert.assertTrue;
import static org.vertx.testtools.VertxAssert.testComplete;

/**
 * Created by yngbldjr on 1/17/15.
 */
public class TestMultipleToOne extends TestVerticle {

    public static class TestComponent extends DefaultBaseComponentVerticle {

        @Override
        public void start(Future<Void> startResult) {
            super.start(startResult);

            final Long timeToWait = container.config().getLong("timeToWait", 1000);
            final String mod = container.config().getString("mod");

            input.port(INPUTS().IN).messageHandler(new Handler<JsonObject>() {
                @Override
                public void handle(final JsonObject event) {
                    vertx.setTimer(timeToWait, new Handler<Long>() {
                        @Override
                        public void handle(Long eventTimeout) {
                            JsonArray testArray = new JsonArray();
                            testArray.add("test"+mod);
                            output.port(OUTPUTS().OUT).send(event.putArray("testArray", testArray).putString("test" + mod, "complete after " + timeToWait).putString(mod, "From mod" + mod));
                        }
                    });

                }
            });
        }
    }

    public static class TimerComponent extends DefaultBaseComponentVerticle {
        @Override
        public void start(Future<Void> startResult) {
            super.start(startResult);

            final Long timer = container.config().getLong("timeout", 15000L);

            input.port(INPUTS().IN).messageHandler(new Handler<JsonObject>() {
                @Override
                public void handle(final JsonObject jsonevent) {
                    vertx.setTimer(timer, new Handler<Long>() {
                        @Override
                        public void handle(Long event) {
                            System.out.println("Timeout!");
                            output.port(OUTPUTS().OUT).send(jsonevent.putString("msg", "timeout!"));
                        }
                    });
                }
            });
        }
    }

    @Test
    public void testAgg() {

        final AtomicInteger counts = new AtomicInteger(0);
        vertx.eventBus().registerHandler("multiplekickComplete", new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
                int completed = counts.incrementAndGet();
                System.out.println("multiplekickComplete" + completed + message.body());
                if(completed == 4)
                    testComplete();
            }
        });

        final Vertigo vertigo = new Vertigo(this);
        vertigo.deployCluster("testScore", new Handler<AsyncResult<Cluster>>() {
            @Override
            public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {

                final NetworkConfig scoreExecutor = vertigo.createNetwork("test");

                scoreExecutor.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "multiplekick")).addHook(new EventBusHook());
                scoreExecutor.addComponent("timeout", TimerComponent.class.getName(), 1).addHook(new EventBusHook());

                scoreExecutor.addComponent("test.component.1", TestComponent.class.getName(), new JsonObject().putNumber("timeToWait", 1500L).putString("mod", "c1"), 1).addHook(new EventBusHook());
                scoreExecutor.addComponent("test.component.2", TestComponent.class.getName(), new JsonObject().putNumber("timeToWait", 2000L).putString("mod", "c2"), 1).addHook(new EventBusHook());
                scoreExecutor.addComponent("test.component.3", TestComponent.class.getName(), new JsonObject().putNumber("timeToWait", 500L).putString("mod", "c3"), 1).addHook(new EventBusHook());
                scoreExecutor.addComponent("test.component.4", TestComponent.class.getName(), new JsonObject().putNumber("timeToWait", 5000L).putString("mod", "c4"), 1).addHook(new EventBusHook());
                scoreExecutor.addComponent("agg", SimpleAggregator.class.getName(), new JsonObject().putNumber("count", 4L).putArray("mergedArrays", new JsonArray().add("testArray")).putString("key", "key").putNumber("delayCheck", 3000L).putNumber("timeoutAge", 6000L), 1).addHook(new EventBusHook());
                scoreExecutor.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "multiplekickComplete"), 1).addHook(new EventBusHook());

                scoreExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "test.component.1", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "timeout", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "test.component.2", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "test.component.3", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "test.component.4", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("test.component.1", DefaultBaseComponentVerticle.OUTPUTS().OUT, "agg", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("test.component.2", DefaultBaseComponentVerticle.OUTPUTS().OUT, "agg", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("test.component.3", DefaultBaseComponentVerticle.OUTPUTS().OUT, "agg", DefaultBaseComponentVerticle.INPUTS().IN);
                scoreExecutor.createConnection("test.component.4", DefaultBaseComponentVerticle.OUTPUTS().OUT, "agg", DefaultBaseComponentVerticle.INPUTS().IN);

                scoreExecutor.createConnection("timeout", DefaultBaseComponentVerticle.OUTPUTS().OUT, "agg", SimpleAggregator.INPUTS().TIMEOUT);
                scoreExecutor.createConnection("agg", DefaultBaseComponentVerticle.OUTPUTS().OUT, "ebdone", DefaultBaseComponentVerticle.INPUTS().IN);

                Collection<ConnectionConfig> c = scoreExecutor.getConnections();

                System.out.println("[");
                for(ConnectionConfig config : c){
                    System.out.println("{\"source\" : \"" +config.getSource().getComponent() + "\", \"target\" : \"" + config.getTarget().getComponent() + "\"},");
                }
                System.out.println("]");

                clusterManagerAsyncResult.result().deployNetwork(scoreExecutor, new Handler<AsyncResult<ActiveNetwork>>() {
                    @Override
                    public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
                        assertTrue(activeNetworkAsyncResult.succeeded());

                        for(ComponentConfig l : activeNetworkAsyncResult.result().getConfig().getComponents()) {
                            System.out.println(l.getConfig());

                            List<ComponentHook> h = l.getHooks();
                            for(ComponentHook hook : h){
                                System.out.println(hook);

                            }

                        }
                        vertx.eventBus().send("multiplekick", new JsonObject().putString("key", UUID.randomUUID().toString()));
                        vertx.eventBus().send("multiplekick", new JsonObject().putString("key", UUID.randomUUID().toString()));
                        vertx.eventBus().send("multiplekick", new JsonObject().putString("key", UUID.randomUUID().toString()));
                        vertx.setTimer(500, new Handler<Long>() {
                            @Override
                            public void handle(Long event) {
                                vertx.eventBus().send("multiplekick", new JsonObject().putString("key", UUID.randomUUID().toString()));
                            }
                        });
                    }
                });
            }
        });
    }
}
