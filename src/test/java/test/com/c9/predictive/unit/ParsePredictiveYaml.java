package test.com.c9.predictive.unit;

import com.c9.generic.components.DefaultBaseComponentVerticle;
import com.c9.generic.components.eventbus.EventBusListenerComponent;
import com.c9.generic.components.eventbus.EventBusSenderComponent;
import com.c9.predictive.components.ExtractDataComponent;
import com.c9.predictive.components.ParseQueriesFromConfigComponent;
import com.c9.predictive.components.ParseYAMLComponent;
import com.c9.predictive.util.Utils;
import net.kuujo.vertigo.Vertigo;
import net.kuujo.vertigo.cluster.Cluster;
import net.kuujo.vertigo.network.ActiveNetwork;
import net.kuujo.vertigo.network.NetworkConfig;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.testtools.TestVerticle;

import static org.vertx.testtools.VertxAssert.*;

/**
 * Created by yngbldjr on 7/1/14.
 */
public class ParsePredictiveYaml extends TestVerticle {

  @Test
  public void testV2Yaml() {
    final Vertigo vertigo = new Vertigo(this);

    vertigo.deployCluster("testScore", new Handler<AsyncResult<Cluster>>() {
      @Override
      public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {
        NetworkConfig yamlExecutor = vertigo.createNetwork("yaml.network");

        yamlExecutor.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlkick"));

        yamlExecutor.addComponent("score", ParseYAMLComponent.class.getName(), 1);

        yamlExecutor.addComponent("done", ComponentComplete.class.getName());
        yamlExecutor.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlComplete"), 1);

        yamlExecutor.createConnection("kick", DefaultBaseComponentVerticle.OUTPUTS().OUT, "score", DefaultBaseComponentVerticle.INPUTS().IN);
        yamlExecutor.createConnection("score", DefaultBaseComponentVerticle.OUTPUTS().OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        yamlExecutor.createConnection("score", DefaultBaseComponentVerticle.OUTPUTS().ERROR_OUT, "done", DefaultBaseComponentVerticle.INPUTS().IN);
        yamlExecutor.createConnection("done", DefaultBaseComponentVerticle.OUTPUTS().OUT, "ebdone", DefaultBaseComponentVerticle.INPUTS().IN);

        clusterManagerAsyncResult.result().deployNetwork(yamlExecutor, new Handler<AsyncResult<ActiveNetwork>>() {
          @Override
          public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
            assertTrue(activeNetworkAsyncResult.succeeded());
            // IP to start the yaml Component
            vertx.eventBus().send("yamlkick", new JsonObject().putString("file", "/cloud9/Perforce/Services/nextgen/predictive/test/Cloud9/predictive_service.yaml"));
          }
        });
      }
    });

    vertx.eventBus().registerHandler("yamlComplete", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        assertNotNull(message.body().getObject("config"));
        testComplete();
      }
    });
  }

  @Test
  public void testParsePredictiveService() {
    final Vertigo vertigo = new Vertigo(this);

    // Deploying DataServices module - used to make all the SQL calls to the ADS
    final JsonObject dataServicesConfig = new JsonObject();
    dataServicesConfig.putString("version", "com.c9~data-services~1.1.1");
    dataServicesConfig.putString("address", "predictive.jdbc");
    dataServicesConfig.putString("macURL", "jdbc:certive://dev1-mac1.engr.cloud9analytics.com:5432/NW;SSL=false;username='su';password='password'");
    dataServicesConfig.putString("workQueueVersion", "com.c9~workqueue~1.0");
    dataServicesConfig.putBoolean("enableMetrics", false);

    container.deployModule(dataServicesConfig.getString("version"), dataServicesConfig, new Handler<AsyncResult<String>>() {
      @Override
      public void handle(AsyncResult<String> stringAsyncResult) {

        if (stringAsyncResult.succeeded()) {
          vertigo.deployCluster("testParser", new Handler<AsyncResult<Cluster>>() {
            @Override
            public void handle(AsyncResult<Cluster> clusterManagerAsyncResult) {

              NetworkConfig yamlExecutor = vertigo.createNetwork("yaml.network");

              yamlExecutor.addComponent("kick", EventBusListenerComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlkick"));

              yamlExecutor.addComponent("readConfig", ParseYAMLComponent.class.getName(), 1);
              yamlExecutor.addComponent("parseQueries", ParseQueriesFromConfigComponent.class.getName(), 1);
              yamlExecutor.addComponent("extract", ExtractDataComponent.class.getName(), dataServicesConfig, 1);

              yamlExecutor.addComponent("done", ComponentComplete.class.getName());
              yamlExecutor.addComponent("ebdone", EventBusSenderComponent.class.getName(), new JsonObject().putString("eventbus.address", "yamlComplete"), 1);

              yamlExecutor.createConnection("kick", EventBusListenerComponent.OUTPUTS().OUT, "readConfig", ParseYAMLComponent.INPUTS().IN);
              yamlExecutor.createConnection("readConfig", ParseYAMLComponent.OUTPUTS().OUT, "parseQueries", ParseQueriesFromConfigComponent.INPUTS().IN);
              yamlExecutor.createConnection("parseQueries", ParseQueriesFromConfigComponent.OUTPUTS().OUT, "extract", ComponentComplete.INPUTS().IN);
              yamlExecutor.createConnection("extract", ExtractDataComponent.OUTPUTS().OUT, "done", ComponentComplete.INPUTS().IN);
              yamlExecutor.createConnection("done", ComponentComplete.OUTPUTS().OUT, "ebdone", EventBusSenderComponent.INPUTS().IN);

              clusterManagerAsyncResult.result().deployNetwork(yamlExecutor, new Handler<AsyncResult<ActiveNetwork>>() {
                @Override
                public void handle(AsyncResult<ActiveNetwork> activeNetworkAsyncResult) {
                  if (activeNetworkAsyncResult.failed()) {
                    activeNetworkAsyncResult.cause().printStackTrace();
                    fail("Failed to deploy network");
                  }
                  assertTrue(activeNetworkAsyncResult.succeeded());
                  // IP to start the yaml Component
                  vertx.eventBus().send("yamlkick", new JsonObject()
                          .putString(Utils.PropertyConstants.PARTITION_ID, "anyThing")
                          .putString(Utils.PropertyConstants.CUSTOMER, "fake")
                          .putString(Utils.PropertyConstants.DATA_EXTRACTION_LOCATION, "/tmp/%_%/open/%/")
                      .putString("file", "/cloud9/Perforce/Services/nextgen/predictive/test/Cloud9/predictive_service.yaml"));
                }
              });
            }
          });
        }
      }
    });

    vertx.eventBus().registerHandler("yamlComplete", new Handler<Message<JsonObject>>() {
      @Override
      public void handle(Message<JsonObject> message) {
        assertNotNull(message.body().getObject("config"));
        testComplete();
      }
    });
  }
}
