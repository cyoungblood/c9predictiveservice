package test.com.c9.predictive.unit;

import com.c9.predictive.components.config.OppScoreVersion;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Test;
import org.vertx.java.core.json.JsonArray;

import java.io.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by yngbldjr on 1/16/15.
 */
public class UploadScoreUtils {

//    @Test
//    public void testCSVScoresConversion() throws Exception {
//
//        JsonArray featureArray = new JsonArray();
//        featureArray.add(new JsonObject().putString("feature_name", "bucket_0_label").putString("feature_desc", "Bucket 0 Label"));
//
//        OppScoreVersion oppScoreVersion = OppScoreVersion.V1_5;
//
//        oppScoreVersion.helper.initFeaturesMap(featureArray);
//
//
//        assertEquals("Label does not match", OppScoreDefinitionHelperUtils.lookupBucketDesc("0", oppScoreVersion.helper.getFeatureMap()), "Bucket 0 Label");
//
//        assertEquals("Label does not match", OppScoreDefinitionHelperUtils.lookupBucketDesc("1", oppScoreVersion.helper.getFeatureMap()), "bucket_1_label");
//
//        featureArray.add(new JsonObject().putString("feature_name", "bucket_1_label").putString("feature_desc", "Bucket 1 Label"));
//        featureArray.add(new JsonObject().putString("feature_name", "bucket_2_label").putString("feature_desc", "Bucket 2 Label"));
//        featureArray.add(new JsonObject().putString("feature_name", "bucket_3_label").putString("feature_desc", "Bucket 3 Label"));
//
//        oppScoreVersion.helper.initFeaturesMap(featureArray);
//
//        // Located in resources.
//        String path = this.getClass().getResource("/OpportunityScoresV3_5.4.csv").getPath();
//
//        BufferedReader rdr = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
//        String row = rdr.readLine(); //ignore first row - contains headers.
//
//        System.out.print(new String(oppScoreVersion.helper.getHeaderBytes()));
//        while((row = rdr.readLine()) != null) {
//            String[] record = row.split(",");
//            JsonObject recordJson = oppScoreVersion.helper.convertRowToJsonObject(record);
//            System.out.print(new String(oppScoreVersion.helper.convertScoreRowToSFDCBytes(recordJson)));
//        }
//
//    }

  @Test public void testFormat(){
      System.out.println(String.format("asdasd%s", "1234"));

  }

  @Test
  public void testCSVScoresConversion56() throws Exception {
    JsonArray featureArray = new JsonArray();

    OppScoreVersion oppScoreVersion = OppScoreVersion.V1_5;

    oppScoreVersion.getHelper().initFeaturesMap(featureArray);

    // Located in resources.
    String path = this.getClass().getResource("/OpportunityScoresV3_56.csv").getPath();

    CSVParser parser = new CSVParser(new FileReader(path), CSVFormat.DEFAULT.withHeader());

    StringBuffer sb = new StringBuffer();

    sb.append(new String(oppScoreVersion.getHelper().getHeaderBytes()));

    for(CSVRecord record : parser.getRecords()){
        sb.append(new String(oppScoreVersion.getHelper().convertScoreRowToSFDCBytes(record)));
    }

    System.out.println(sb.toString());
  }

    @Test
    public void testCSVScoresConversionNew() throws Exception {

        JsonArray featureArray = new JsonArray();
//        featureArray.add(new JsonObject().putString("feature_name", "bucket_0_label").putString("feature_desc", "Bucket 0 Label"));

        OppScoreVersion oppScoreVersion = OppScoreVersion.V1_5;

        oppScoreVersion.getHelper().initFeaturesMap(featureArray);

//        assertEquals("Label does not match", OppScoreDefinitionHelperUtils.lookupBucketDesc("0", oppScoreVersion.helper.getFeatureMap()), "Bucket 0 Label");

//        assertEquals("Label does not match", OppScoreDefinitionHelperUtils.lookupBucketDesc("1", oppScoreVersion.helper.getFeatureMap()), "bucket_1_label");

//        featureArray.add(new JsonObject().putString("feature_name", "bucket_1_label").putString("feature_desc", "Bucket 1 Label"));
//        featureArray.add(new JsonObject().putString("feature_name", "bucket_2_label").putString("feature_desc", "Bucket 2 Label"));
//        featureArray.add(new JsonObject().putString("feature_name", "bucket_3_label").putString("feature_desc", "Bucket 3 Label"));

        oppScoreVersion.getHelper().initFeaturesMap(featureArray);

        // Located in resources.
        String path = this.getClass().getResource("/OpportunityScoresV3_Test.csv").getPath();

        CSVParser parser = new CSVParser(new FileReader(path), CSVFormat.DEFAULT.withHeader());


        StringBuffer sb = new StringBuffer();

        sb.append(new String(oppScoreVersion.getHelper().getHeaderBytes()));

//
        for(CSVRecord record : parser.getRecords()){
          sb.append(new String(oppScoreVersion.getHelper().convertScoreRowToSFDCBytes(record)));
        }

//        File tmpFile = File.createTempFile("oppScorout", ".csv");

//        FileOutputStream fos = new FileOutputStream(tmpFile);
//
//        fos.write(sb.toString().getBytes());

        System.out.println(sb.toString());


    }

//    //Read from csv
//    String scoreCSV = input.getString(SCORE_CSV);
//
//    CSVParser parser = new CSVParser(new FileReader(scoreCSV), CSVFormat.DEFAULT.withSkipHeaderRecord(true));
//
//    for(CSVRecord record : parser.getRecords()){
//        if(batchCount == 0) {
//            tmpOut = new FileOutputStream(tmpFile);
//            tmpOut.write(oppScoreVersion.helper.getHeaderBytes());
//        }
//        batchCount++;
//        totalCount++;
//
//        tmpOut.write(oppScoreVersion.helper.convertScoreRowToSFDCBytes(record));
//
//        if(batchCount >= batchSize && batchCount % batchSize == 0) {
//            batchInfoList.add(createBatch(tmpOut, tmpFile, connection, job));
//            batchCount = 0;
//        }
//    }
}
