package test.com.c9.predictive.integration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Test;
import org.vertx.java.core.json.JsonObject;
import test.com.c9.predictive.common.VertigoTestVerticle;

import javax.jms.*;
import java.util.UUID;

import static org.vertx.testtools.VertxAssert.*;

public class NoPartitionFoundTest extends VertigoTestVerticle {

  @Test
  public void testErrors() throws Exception {
    final String partitionId = "00D300000006RmSEAU-NotReal";

    final String correlationId = UUID.randomUUID().toString();
    sendJMSMessageToQueue("C9.predictive.command.queue", partitionId, "localhost", correlationId);

    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

    // Create a Connection
    Connection connection = connectionFactory.createConnection();
    connection.start();

    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

    Destination destination = session.createQueue("C9.predictive.wait.queue");

    MessageConsumer consumer = session.createConsumer(destination);

    consumer.setMessageListener(new MessageListener() {
      @Override
      public void onMessage(javax.jms.Message message) {
        if (message instanceof TextMessage)
        {
          JsonObject jsonResponse = new JsonObject();
          TextMessage textMessage = (TextMessage) message;
          String text;

          try
          {
            assertEquals(correlationId, textMessage.getJMSCorrelationID());
            assertEquals("serviceName property should be Predictive.", "Predictive",textMessage.getStringProperty("serviceName"));
            assertTrue("localhost boolean property should be set and true.", textMessage.getBooleanProperty("localhost"));
            text = textMessage.getText();
            jsonResponse.putString("message", text);
            assertNotNull(text);
            testComplete();
          } catch (JMSException e) {
            container.logger().error("Error consuming JMS Message", e);
            fail();
          }
        }
        else {
          container.logger().error("Received but not passed along since it was not a text message : " + message);
        }
      }
    });
  }



}
