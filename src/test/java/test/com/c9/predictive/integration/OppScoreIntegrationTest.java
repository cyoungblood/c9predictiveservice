package test.com.c9.predictive.integration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Test;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.Future;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.json.JsonObject;
import test.com.c9.predictive.common.JMSTestVerticle;

import javax.jms.*;
import java.util.UUID;

import static org.vertx.testtools.VertxAssert.*;

public class OppScoreIntegrationTest extends JMSTestVerticle {

  @Test
  public void testHappyPath() throws Exception{
    final String partitionId = "00D300000006RmSEAU";

    final String correlationId = UUID.randomUUID().toString();
    sendJMSMessageToQueue("C9.predictive.command.queue", partitionId, "localhost", correlationId);

    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

    // Create a Connection
    Connection connection = connectionFactory.createConnection();
    connection.start();

    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

    Destination destination = session.createQueue("C9.predictive.wait.queue");

    MessageConsumer consumer = session.createConsumer(destination);

    consumer.setMessageListener(new MessageListener() {
      @Override
      public void onMessage(javax.jms.Message message) {
        if (message instanceof TextMessage)
        {
          JsonObject jsonResponse = new JsonObject();
          TextMessage textMessage = (TextMessage) message;
          String text;

          try
          {
            assertEquals(correlationId, textMessage.getJMSCorrelationID());
            assertEquals("serviceName property should be Predictive.", "Predictive",textMessage.getStringProperty("serviceName"));
            assertTrue("localhost boolean property should be set and true.", textMessage.getBooleanProperty("localhost"));
            text = textMessage.getText();
            jsonResponse.putString("message", text);
            assertNotNull(text);
            testComplete();
          } catch (JMSException e) {
            container.logger().error("Error consuming JMS Message", e);
            fail();
          }
        }
        else {
          container.logger().error("Received but not passed along since it was not a text message : " + message);
        }
      }
    });

  }


  @Override
  public void start(final Future<Void> startedResult) {
    super.start(startedResult);

    // Make sure we call initialize() - this sets up the assert stuff so assert functionality works correctly
    initialize();

    // Deploy the module - the System property `vertx.modulename` will contain the name of the module so you
    // don't have to hardecode it in your tests

    String confFile = "config/config-local.json";
    if(System.getProperty("conf") != null){
      confFile = System.getProperty("conf");
    }

    Buffer buffer = vertx.fileSystem().readFileSync(confFile);

    final JsonObject config =  new JsonObject(buffer.toString());

    container.logger().info("Starting module with config : " + config.toString());

    container.logger().info("System.getProperty(\"vertx.modulename\") " + System.getProperty("vertx.modulename"));

    container.deployModule(System.getProperty("vertx.modulename"), config, new AsyncResultHandler<String>() {
      @Override
      public void handle(AsyncResult<String> asyncResult) {
        // Deployment is asynchronous and this this handler will be called when it's complete (or failed)
        if (asyncResult.failed()) {

          container.logger().error(asyncResult.cause());
          fail("Failed to deploy module");
        }
        assertTrue(asyncResult.succeeded());
        assertNotNull("deploymentID should not be null", asyncResult.result());

        startedResult.setResult(null);

        startTests();
      }
    });
  }

}
