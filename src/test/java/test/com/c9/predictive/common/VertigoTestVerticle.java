/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package test.com.c9.predictive.common;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.json.JsonObject;

import static org.vertx.testtools.VertxAssert.*;

public abstract class VertigoTestVerticle extends JMSTestVerticle {

  @Override
  public void start() {

    // Make sure we call initialize() - this sets up the assert stuff so assert functionality works correctly
    initialize();

    // Deploy the module - the System property `vertx.modulename` will contain the name of the module so you
    // don't have to hardecode it in your tests

    String confFile = "config/conf-test.json";
    if(System.getProperty("conf") != null){
      confFile = System.getProperty("conf");
    }

    Buffer buffer = vertx.fileSystem().readFileSync(confFile);

    final JsonObject config =  new JsonObject(buffer.toString());

    container.logger().info("Starting module with config : " + config.toString());

    container.logger().info("System.getProperty(\"vertx.modulename\") " + System.getProperty("vertx.modulename"));

    container.deployModule(System.getProperty("vertx.modulename"), config, new AsyncResultHandler<String>() {
      @Override
      public void handle(AsyncResult<String> asyncResult) {
        // Deployment is asynchronous and this this handler will be called when it's complete (or failed)
        if (asyncResult.failed()) {
          container.logger().error(asyncResult.cause());
          fail("Failed to deploy module");
        }
        assertTrue(asyncResult.succeeded());
        assertNotNull("deploymentID should not be null", asyncResult.result());

        startTests();
      }
    });
  }
}
