package test.com.c9.predictive.common;

import com.c9.predictive.util.Utils;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.vertx.java.core.Future;
import org.vertx.testtools.TestVerticle;

import javax.jms.*;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by yngbldjr on 6/9/14.
 */
public abstract class JMSTestVerticle extends TestVerticle {

  private static BrokerService brokerService;

  @Override
  public void start(Future<Void> startedResult) {
    super.start(startedResult);
    try{
      setup();
    }
    catch (Exception e){
      e.printStackTrace();
      startedResult.setFailure(e);
      return;
    }
    startedResult.setResult(null);
  }

  @Override
  public void stop() {
    super.stop();
    try {
      cleanup();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  protected String sendJMSMessageToQueue(String queue, String message, String hostname, String correlationId) throws Exception {
    return sendJMSMessageToQueue(queue, message, hostname, correlationId, 60 * 60 * 1000L);
  }

  protected String sendJMSMessageToQueue(String queue, String message, String hostname, String correlationId, Long maxWaitTime) throws Exception {
    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

    // Create a Connection
    Connection connection = connectionFactory.createConnection();
    connection.start();

    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

    Destination destination = session.createQueue(queue);

    TextMessage msg = session.createTextMessage();
    msg.setText(message);
    msg.setJMSCorrelationID(correlationId);
    msg.setStringProperty(Utils.PropertyConstants.ANALYTIC_HOSTNAME, hostname);
    msg.setLongProperty("predictiveMaximumWaitTime", maxWaitTime);
    msg.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);

    container.logger().info("Sending JMS message to " + queue + " with message : " + message);
    session.createProducer(destination).send(msg);

    session.close();

    // Cleanup;
    session = null;
    destination = null;

    connection.stop();
    connection.close();
    connection = null;

    connectionFactory = null;

    return correlationId;
  }

  public void setup() throws Exception {
    brokerService = new BrokerService();
    brokerService.addConnector("tcp://localhost:61616");
    brokerService.setBrokerName("Broker");
    brokerService.setUseJmx(true);
    brokerService.start();

    brokerService.getAdminView().addQueue("C9.predictive.wait.queue");
    brokerService.getAdminView().addQueue("C9.predictive.command.queue");
  }

  public void cleanup() throws Exception {
    // Stop the activeMQ broker
    brokerService.stop();

    delDir(Paths.get("activemq-data"));
  }

  public static void delDir(Path directory) throws IOException {
    Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (file == null)
          return FileVisitResult.TERMINATE;

        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (dir == null)
          return FileVisitResult.TERMINATE;

        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }

    });
  }
}
