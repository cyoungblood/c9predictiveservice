#!/bin/sh
#

# example of fitting
# update if not using the current directory
export PREDICTIVE_HOME=$1

echo "first argument : $1"
echo "second argument: $2"

cd $PREDICTIVE_HOME

PYTHONPATH=$PREDICTIVE_HOME:$PREDICTIVE_HOME/src/ $2

# EOF
