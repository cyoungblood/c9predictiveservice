#!/bin/sh
#
# Purpose: Used to stop C9 Dashboard Service running process.
#

# set -o xtrace

usage="Usage: stop.sh"

if [ -z "$PREDSVC_LOGFILE" ]; then
	export PREDSVC_LOGFILE=$PREDSVC_HOME/logs/wrapper.log
fi

if [ $# -ne 0 ];
then
        echo 'PURPOSE: To stop a running Predictive Service process.'
        echo 'USAGE  : stop.sh'
        exit 1
fi

pids=`ps xwww | grep "predictive-service" | grep "java" | awk '{print $1}'`

if [ "$pids" != "" ]
then
        echo "Stopping Predictive Services `date`" >> $PREDSVC_LOGFILE
        kill -1 $pids >> $PREDSVC_LOGFILE
        sleep 10
        pids=`ps xwww | grep "predictive-service" | grep "java" | awk '{print $1}'`
        if [ "$pids" != "" ]
        then
          sleep 10
        fi
        pids=`ps xwww | grep "predictive-service" | grep "java" | awk '{print $1}'`
        if [ "$pids" != "" ]
        then
          echo "Stopping Predictive Services `date` with a hard kill" >> $PREDSVC_LOGFILE
          kill -9 $pids >> $PREDSVC_LOGFILE
          echo ".........." >> $PREDSVC_LOGFILE
        fi
        echo "STOPPED Predictive Services `date`" >> $PREDSVC_LOGFILE
fi
exit 1
