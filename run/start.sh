#!/bin/sh
#

# Source pod.bashrc to have access to $pod_name and shell functions
. ${POD_HOME}/c9/data/pod.bashrc

export PATH=$JAVA_HOME/bin:$PATH

# Environment / POD (This is $pod_name without spaces or dashes, all in lowercase
ENV_NAME=$(echo $pod_name | sed -e "s/\ //g" -e "s/-//g" | tr 'A-Z' 'a-z')

# The Hazelcast group is "predictive-" followed by the four first letter of $pod_name in lowercase
PRED_HAZELCAST_GROUP=predictive-${ENV_NAME}

# We get the ip of this local box that is running the service
PRED_SERVICE_IP=$(host $MACHINE_NAME | awk '{print $NF}')

# We get the IPs of all the machines running predictive
CLUSTER_MEMBERS=$(echo $(for svc in $(expand_services predictive_services); do get_service_info ip_address $svc; done) | tr ' ' ',')

# In environments with more than one predictive service, each machine has its own configuration file
case ${ENV_NAME} in
  staging | production)
    CONFIG_FILE=config-${MACHINE_NAME}.json
    ;;

  *)
    CONFIG_FILE=config.json
esac

echo "" > $PREDSVC_LOGFILE
echo "STARTED Predictive Service `date`"            >> $PREDSVC_LOGFILE
echo "Configuration file=${CONFIG_FILE}"            >> $PREDSVC_LOGFILE
echo "JAVA_HOME=$JAVA_HOME"                         >> $PREDSVC_LOGFILE
echo "PATH=$PATH"                                   >> $PREDSVC_LOGFILE
echo "JAVA_OPTIONS=$JAVA_OPTIONS"                   >> $PREDSVC_LOGFILE
echo "JVM_MEM_SETTINGS=$JVM_MEM_SETTINGS"           >> $PREDSVC_LOGFILE
echo "PREDSVC_OPTIONS=$PREDSVC_OPTIONS"             >> $PREDSVC_LOGFILE
echo "PRED_HAZELCAST_GROUP=${PRED_HAZELCAST_GROUP}" >> $PREDSVC_LOGFILE
echo "PRED_SERVICE_IP=${PRED_SERVICE_IP}"           >> $PREDSVC_LOGFILE
echo "CLUSTER_MEMBERS=${CLUSTER_MEMBERS}"           >> $PREDSVC_LOGFILE

java $JVM_MEM_SETTINGS $PREDSVC_OPTIONS -Dhazelcast.members=${CLUSTER_MEMBERS} -Dhazelcast.group=${PRED_HAZELCAST_GROUP} -Dhazelcast.interface=${PRED_SERVICE_IP} -Dorg.vertx.logger-delegate-factory-class-name=org.vertx.java.core.logging.impl.SLF4JLogDelegateFactory -jar $PREDSVC_INSTALLATIONS_ROOT/latestPredSVC/predictive-service-0.0.1-fat.jar -cluster -conf $PREDSVC_INSTALLATIONS_ROOT/latestPredSVC/config/${CONFIG_FILE} >> $PREDSVC_LOGFILE 2>&1 &

#
# EOF
#
