Predictive Service
========

The Predictive Service is an automated service to do the following.

* Listen to new ADS JMS message (queue address TBD)
* Run default/overriden sql to produce data on open opportunities
* Put the data in csv (tab delimited) format on disk
* Execute pyhon script (scorer.py) to produce a final csv to be consumed and materialized by the ADS
* Let the ADS know the rotation/refresh can complete.

## Instalation instructions ##

##### Requirements #####
JDK7 or higher
Activemq Server on the network
##### Building #####
```
./gradlew fatjar
```
##### Running #####
```
run/start.sh
```
##### Configuration #####
```
check the run/start.sh command - it shows how to run with a specific config
```

### Related Projects ###
* [Data Extractor](https://bitbucket.org/c9inc/data-extractor) <- Code for open opportunities is based off this project
* [predictive](https://bitbucket.org/c9inc/predictiveserviceconfig) <- The python scripts that produce the score data

#### Sequence Diagram of the predictive service lifecycle ####

![Sequence Diagram](PredictiveServiceSequenceDiagram.png)